<?php
namespace daicuo;

class Cache 
{
    // 错误信息
    protected static $error = 'error';
    
    /**
     * 获取错误信息
     * @return mixed
     */
    public static function getError()
    {
        return self::$error;
    }
    
    /**
    * APP初始化时动态配置缓存类型
    * @param array $data 数据
    * @return array 配置数组
    */
    public static function appInt()
    {
        // 实例化文件缓存
        $cacheFile = new \think\cache\driver\File([
            'type'   => 'File',
            'path'   => CACHE_PATH,
            'prefix' => '',
            'expire' => 0,
        ]);
        
        // 文件缓存获取缓存方式动态配置
        if( !$siteCache = $cacheFile->get('site_cache') ){
            $info = \daicuo\Op::get(['op_name' => 'site_cache', 'op_module' => 'cache'], false);
            if(is_null($info)){
                return NULL;
            }
            //定义缓存配置
            $siteCache = $info['op_value'];
            //文件缓存设置缓存方式动态配置
            $cacheFile->set('site_cache', $siteCache);
        }
        
        //动态配置缓存方式（缓存配置前不能使用缓存函数，否则失效）
        if( self::check($siteCache['type']) ){
            config('cache', $siteCache);
        }
        
        return $siteCache;
    }
    
    /**
    * 删除文件缓存的site_cache
    * @return bool
    */
    public static function rm()
    {
        // 实例化文件缓存
        $cacheFile = new \think\cache\driver\File([
            'type'   => 'File',
            'path'   => CACHE_PATH,
            'prefix' => '',
            'expire' => 0,
        ]);
        //返回删除结果
        return $cacheFile->rm('site_cache');
    }

    /**
    * 自动处理新增与修改
    * @param array $data 数据
    * @return array 数据集
    */
    public static function write($post)
    {
		//读取旧数据
        $info = \daicuo\Op::get(['op_name' => 'site_cache', 'op_module' => 'cache'], false);
        //无记录则新增
        if(is_null($info)){
            $op = array();
            $op['op_name']     = 'site_cache';
            $op['op_value']    = $post;
            $op['op_module']   = 'cache';
            $op['op_controll'] = '';
            $op['op_action']   = '';
            $op['op_order']    = 0;
            $op['op_autoload'] = 'no';
            $op['op_status']   = 'normal';
            return \daicuo\Op::save($op);
        }
		//新与旧合并
        $info = $info->toArray();
        $info['op_value'] = array_merge($info['op_value'], $post);
		return \daicuo\Op::update_id($info['op_id'], $info);
    }
    
    /**
    * 验证缓存方式是否支持
    * @param string $cacheType 缓存方式
    * @return bool
    */
    public static function check($cacheType)
    {
        if($cacheType == 'Sqlite3'){
			if(!class_exists('sqlite3')){
                return false;
			}
		}elseif($cacheType == 'Memcache'){
			if(!class_exists('memcache')){
                return false;
			}
		}elseif($cacheType == 'Memcached'){
			if(!class_exists('memcached')){
                return false;
			}
		}elseif($cacheType == 'Redis'){
			if(!class_exists('redis')){
                return false;
			}
		}elseif($cacheType == 'Wincache'){
			if(!class_exists('wincache_ucache_info')){
                return false;
			}
		}elseif($cacheType == 'Xcache'){
			if(!class_exists('xcache_info')){
                return false;
			}
		}
        return true;
    }
}