<?php
namespace daicuo;

class Nav 
{
    // 错误信息
    protected static $error = 'error';
    
    /**
     * 获取错误信息
     * @return mixed
     */
    public static function getError()
    {
        return self::$error;
    }

    /**
     * 批量增加导航
     * @param array $list 写入数据（二维数组） 
     * @return null|obj 添加成功返回自增ID数据集
     */
    public static function save_all($list)
    {
        foreach($list as $key=>$value){
            //手动验证数据
            if(false === DcCheck($value, 'common/Nav')){
                unset($list[$key]);
                continue;
            }
            //取消自动验证
            config('common.validate_name', false);
            $list[$key] = self::data_post($value);
        }
        if(!$list){
            return null;
        }
        return \daicuo\Op::save_all($list);
    }
   
    /**
     * 按Id删除一个导航
     * @param string $value 字段值
     * @return int 返回操作记录数
     */
    public static function delete_id($value)
    {
        $where = array();
        $where['op_id'] = ['eq', $value];
        return self::delete($where);
    }
    
    /**
     * 按Id修改一个导航
     * @param string $value 字段值
     * @param array $data 写入数据（一维数组） 
     * @return obj|null 不为空时返回obj
     */
    public static function update_id($value, $data)
    {
        if ( ! $value ) {
            return null;
        }
        if($value < 1){
            return null;
        }
        $where = array();
        $where['op_id'] = ['eq', $value];
        return self::update($where, $data);
    }
    
    /**
     * 通过ID获取导航
     * @param int $value 字段值 
     * @param bool $cache 是否开启缓存功能 由后台统一配置
     * @return obj|null 不为空时返回修改后的数据
     */
    public static function get_id($value, $cache=true)
    {
        if ( !$value ) {
            return null;
        }
        $where = array();
        $where['op_id'] = ['eq', $value];
        return self::get($where, $cache);
    }
    
    /**
     * 新增一个新导航
     * @param array $data 写入数据（一维数组） 
     * @return int 添加成功返回自增ID
     */
    public static function save($data)
    {
        //数据验证及格式化数据
        if(!$data = self::data_post($data)){
            return null;
		}
        //OP验证
        config('common.validate_name', false);
        //数据库
        $result = \daicuo\Op::save($data);
        //处理结果
        if(!$result){
            self::$error = \daicuo\Op::getError();
        }else{
            DcCacheTag('common/Nav/Item', 'clear');
        }
        //返回结果
        return $result;
    }
    
    /**
     * 按条件删除导航
     * @param array $where 删除条件
     * @return mixed $mixed obj|null
     */
    public static function delete($where)
    {
        $result = \daicuo\Op::delete($where);
        //清理全局缓存
        DcCacheTag('common/Nav/Item', 'clear');
        //返回结果
        return $result;
    }
    
    /**
     * 修改一个导航
     * @param array $where 修改条件
     * @param array $data 写入数据（一维数组） 
     * @return null|obj 成功时返回obj
     */
    public static function update($where, $data)
    {
        //数据验证及格式化数据
        if(!$data = self::data_post($data)){
            return null;
		}
        //OP验证
        config('common.validate_name', false);
        //数据库
        $result = \daicuo\Op::update($where, $data);
        //处理结果
        if(!$result){
            self::$error = \daicuo\Op::getError();
        }else{
            DcCacheTag('common/Nav/Item', 'clear');
        }
        //返回结果
        return $result;
    }
    
    /**
     * 按条件查询一个导航
     * @param array $where 查询条件（一维数组）
     * @param bool $cache 是否开启缓存功能由后台统一配置
     * @return mixed null|array 不为空时返回获取器修改后的array
     */
    public static function get($where, $cache=true)
    {
        //数据库
        $result = \daicuo\Op::get($where, $cache);
        //获取器转化
        $result = \daicuo\Op::data_value($result);
        //获取器增加链接字段
        if($result){
            $result['nav_link'] = self::navLink($result);
        }
        //返回结果
        return $result;
    }
    
    /**
     * 按条件查询多条导航
     * @param array $args 查询条件（一维数组）
     * @return obj|array 不为空时返回处理好的array
     */
    public static function all($args=[])
    {
        //格式验证
        if(!is_array($args)){
            return null;
        }
        //合并初始参数
        $args = DcArrayArgs($args,[
            'cache'     => true,
            'fetchSql'  => false,
            'tree'      => false,
            'level'     => false,
            'field'     => 'op_id,op_name,op_value,op_module,op_controll,op_action,op_order,op_status',
            'sort'      => 'op_order',
            'order'     => 'asc',
            'where'     => [],
            'limit'     => 0,
            'page'      => 0,
            'paginate'  => '',
        ]);
        //模块限制条件
        $args['where'] = array_merge($args['where'],['op_name'=>['eq','site_nav']]);
        //查询数据库
        $result = \daicuo\Op::all($args);
        //获取器转化
        $result = \daicuo\Op::data_value_array($result);
        //是否分页查询
        if($args['paginate']){
            //获取器增加链接字段
            foreach($result['data'] as $key=>$value){
                $result['data'][$key]['nav_link'] = self::navLink($value);
            }
            //是否转化为树状
            if($args['tree']){
                $result['data'] = list_to_tree($result['data'], 'op_id', 'nav_parent');
            }
            //是否还原为层级
            if($args['level']){
                $result['data'] = tree_to_level($result['data'], 'nav_text');
            }
            return $result;
        }
        //获取器增加链接字段
        foreach($result as $key=>$value){
            $result[$key]['nav_link'] = self::navLink($value);
        }
        //是否转化为树状
        if($args['tree']){
            $result = list_to_tree($result, 'op_id', 'nav_parent');
        }
        //是否还原为层级
        if($args['level']){
            $result = tree_to_level($result, 'nav_text');
        }
        //返回结果
        return $result;
    }
    
    /**
     * 转换post数据
     * @param array $data 表单数据
     * @return obj|null 不为空时返回obj
     */
    public static function data_post($post)
    {
        //表单验证
        $validate = [];
        $validate['data'] = $post;
        $validate['error'] = '';
        $validate['result'] = true;
        //定义钩子参数
        \think\Hook::listen('form_validate', $validate);
        if($validate['result'] == false){
            self::$error = $validate['error'];
            return null;
        }
        unset($validate);
        //数据整理
		$post['op_name']   = 'site_nav';
		$post['op_value']  = self::data_value_set($post);
        //数据过滤
        $post = DcArrayIsset($post, ['op_name','op_value','op_module','op_controll','op_action','op_order','op_status','op_autoload']);
        //合并初始参数
        $post = DcArrayArgs($post,[
            'op_status'    => 'normal',
            'op_order'     => 0,
            'op_autoload'  => 'no',
        ]);
        return $post;
    }
    
    /**
     * 修改器:转换post数据,处理成一条符合TP路由规则格式的数据（键名为规则）
     * @param array $post 表单数据
     * @return array 二维数组
     */
    private static function data_value_set($post)
    {
        $op_value = array();
        foreach($post as $key=>$value){
            if(substr($key,0,3)=='nav'){
                $op_value[$key] = $value;
            }
        }
        return $op_value;
    }
    
    /**
     * 导航列表简单数组格式
     * @param array $args 查询条件（一维数组）
     * @return array;
     */
    public static function option($args)
    {
        $args = DcArrayArgs($args,[
            'cache' => true,
            'tree'  => true,
            'level' => true,
        ]);
        $list = self::all($args);
        if(is_null($list)){
            return null;
        }
        $navList = array();
        $navList[0] = ' ';
        foreach($list as $key=>$value){
            $navList[$value['op_id']] = $value['nav_text'];
        }
        unset($list);
        return $navList;
    }
    
    /**
     * 转换导航链接
	 * @param array $data 转换后的导航数据
     * @return string 处理后的导航链接
     */
	private static function navLink($data)
    {
		if($data['nav_type'] == 'addon'){
			return DcUrlAdmin(
                $data['nav_module'].'/'.$data['nav_controll'].'/'.$data['nav_action'], 
                $data['nav_params'],
                $data['nav_suffix']
            );
		}
		return $data['nav_url'];
	}
    
}