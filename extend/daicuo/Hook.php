<?php
namespace daicuo;

class Hook 
{
    // 错误信息
    protected static $error = 'error';
    
    /**
     * 获取错误信息
     * @return mixed
     */
    public static function getError()
    {
        return self::$error;
    }
    
    /**
    * 模块初始化时注册所有动态钩子
    * @return array
    */
    public static function moduleInit()
    {
        if( !$hooks = DcCache('hook_all') ){
            $args = array();
            $args['cache'] = false;
            $args['field'] = 'op_id,op_value,op_status';
            $args['sort']  = 'op_order';
            $args['order'] = 'asc';
            $args['where']['op_status'] = ['eq','normal'];
            $hooks = self::all($args);
            if(!is_null($hooks)){
                DcCache('hook_all', $hooks, 0);
            }
        }
        foreach($hooks as $key=>$value){
            //验证钩子路径
            if( class_exists( trim($value['hook_path'])) ){
                //注册钩子
                if($value['hook_overlay']=='yes'){
                    \think\Hook::add($value['hook_name'], [$value['hook_path'], '_overlay'=>true]);
                }else{
                    \think\Hook::add($value['hook_name'], $value['hook_path']);
                }
            }
        }
        return $hooks;
    }

    /**
     * 批量增加钩子
     * @param array $data 写入数据（二维数组） 
     * @return null|obj 添加成功返回自增ID数据集
     */
    public static function save_all($list)
    {
        foreach($list as $key=>$value){
            //手动验证数据
            if(false === DcCheck($value, 'common/Hook')){
                unset($list[$key]);
                continue;
                //return 0;
            }
            //取消自动验证
            config('common.validate_name', false);
            $list[$key] = self::data_post($value);
        }
        if(!$list){
            return null;
        }
        return \daicuo\Op::save_all($list);
    }
   
    /**
     * 按Id删除一个钩子
     * @param mixed $value 必需;字段值（string|array）;默认：空
     * @return mixed $mixed obj|null
     */
    public static function delete_id($value)
    {
        $where = array();
        $where['op_id'] = ['eq', $value];
        return self::delete($where);
    }
    
    /**
     * 按Id修改一个钩子
     * @param string $value 字段值
     * @param array $data 写入数据（一维数组） 
     * @return array|null 不为空时返回array
     */
    public static function update_id($value, $data)
    {
        if ( !$data ) {
            return null;
        }
        if($value < 1){
            return null;
        }
        $where = array();
        $where['op_id'] = ['eq', $value];
        return self::update($where, $data);
    }
    
    /**
     * 通过ID获取一个钩子
     * @param int $value 字段值 
     * @param bool $cache 是否开启缓存功能 由后台统一配置
     * @return array|null 不为空时返回修改后的数据
     */
    public static function get_id($value, $cache=true)
    {
        if ( ! $value ) {
            return null;
        }
        $where = array();
        $where['op_id'] = ['eq', $value];
        return self::get($where, $cache);
    }
    
    /**
     * 增加一个钩子
     * @param array $data 必需;表单数据/一维数组;默认：空
     * @return int $int 添加成功返回自增ID,失败时返回0
     */
    public static function save($data=[])
    {
        //数据验证及格式化数据
        if(!$data = self::data_post($data)){
            return 0;
		}
        //OP验证
        config('common.validate_name', 'common/Op');
        //数据库操作
        $result = \daicuo\Op::save($data);
        //处理结果
        if(!$result){
            self::$error = \daicuo\Op::getError();
        }else{
            DcCache('hook_all', null);
        }
        //返回结果
        return $result;
    }
    
    /**
     * 按条件删除一个钩子
     * @param array $where 删除条件
     * @return int 返回操作记录数
     */
    public static function delete($where=[])
    {
        //OP验证
        config('common.validate_name', false);
        //数据库操作
        $result = \daicuo\Op::delete($where);
        //清理全局缓存
        DcCache('hook_all', null);
        return $result;
    }
    
    /**
     * 修改一个钩子
     * @param array $where 修改条件
     * @param array $data 写入数据（一维数组） 
     * @return int 添加成功返回自增ID
     */
    public static function update($where=[], $data=[])
    {
        //数据验证及格式化数据
        if(!$data = self::data_post($data)){
            return null;
		}
        //OP验证
        config('common.validate_name', false);
        //数据库操作
        $result = \daicuo\Op::update($where, $data);
        //处理结果
        if(!$result){
            self::$error = \daicuo\Op::getError();
        }else{
            DcCache('hook_all', null);
        }
        //返回结果
        return $result;
    }
    
    /**
     * 按条件查询一个钩子
     * @param array $where 查询条件（一维数组）
     * @param bool $cache 是否开启缓存功能由后台统一配置
     * @return mixed null|array,不为空时返回array
     */
    public static function get($where=[], $cache=true)
    {
        //数据库
        $result = \daicuo\Op::get($where, $cache);
        //获取器转化
        $result = \daicuo\Op::data_value($result);
        //返回结果
        return $result;
    }
    
    /**
     * 按条件查询多个钩子
     * @param array $args 查询条件（一维数组）
     * @return obj|array 不为空时返回处理好的array
     */
    public static function all($args=[])
    {
        //格式验证
        if(!is_array($args)){
            return null;
        }
        //合并初始参数
        $args = DcArrayArgs($args,[
            'cache'     => true,
            'fetchSql'  => false,
            'tree'      => false,
            'field'     => 'op_id,op_name,op_value,op_module,op_controll,op_action,op_order,op_status',
            'sort'      => 'op_order',
            'order'     => 'desc',
            'where'     => [],
            'limit'     => 0,
            'page'      => 0,
            'paginate'  => '',
        ]);
        //模块限制条件
        $args['where'] = array_merge($args['where'],['op_name'=>['eq','site_hook']]);
        //查询数据库
        $result = \daicuo\Op::all($args);
        //获取器转化
        $result = \daicuo\Op::data_value_array($result);
        //返回结果
        return $result;
    }
    
    /**
     * 转换post数据写入OP配置表
     * @param array $post 表单数据
     * @return array|null 二维数组
     */
    public static function data_post($post)
    {
        //表单验证
        $validate = [];
        $validate['data'] = $post;
        $validate['error'] = '';
        $validate['result'] = true;
        //定义钩子参数
        \think\Hook::listen('form_validate', $validate);
        if($validate['result'] == false){
            self::$error = $validate['error'];
            return null;
        }
        unset($validate);
        //数据整理
		$post['op_name']   = 'site_hook';
		$post['op_value']  = self::data_value_set($post);
        //数据过滤
        $post = DcArrayIsset($post, ['op_name','op_value','op_module','op_controll','op_action','op_order','op_status','op_autoload']);
        //合并初始参数
        $post = DcArrayArgs($post,[
            'op_status'    => 'normal',
            'op_order'     => 0,
            'op_autoload'  => 'no',
        ]);
        return $post;
    }
    
    /**
     * 修改器:转换post数据,处理成一条符合TP路由规则格式的数据（键名为规则）
     * @param array $post 表单数据
     * @return array 二维数组
     */
    private static function data_value_set($post){
        if($post['op_value']){
            return $post['op_value'];
        }
        $hook = array();
        $hook['hook_name']    = trim($post['hook_name']);
        $hook['hook_path']    = trim($post['hook_path']);
        $hook['hook_info']    = trim($post['hook_info']);
        $hook['hook_overlay'] = DcEmpty($post['hook_overlay'], 'no');
		return $hook;
    }
    
}