<?php
return [
    //'controller_auto_search' => true,
    
	'url_common_param' => true,
    
    'url_html_suffix'  => '',
    
    'admin_menu'       => [
        //插件
        'addon' => [],
        //顶部
        'top'   => [
            [
                'ico'     => 'fa fa-fw fa-gear',
                'title'   => 'admin_home',
                'controll'=> 'index',
                'action'  => 'index',
                'url'     => DcUrl('admin/index/index','',''),
                'order'   => 1,
            ],
            [
                'ico'     => 'fa fa-fw fa-home',
                'title'   => 'front_home',
                'controll'=> 'index',
                'action'  => 'index',
                'target'  => '_blank',
                'url'     => '../../',
                'order'   => 11,
            ],
            [
                'ico'     => 'fa fa-fw fa-sign-out',
                'title'   => 'admin_logout',
                'controll'=> 'index',
                'action'  => 'index',
                'url'     => DcUrl('admin/index/logout','',''),
                'order'   => 21,
            ],
        ],
        //配置
        'config' => [
            [
                'ico'     => 'fa fa-fw fa-gear',
                'title'   => 'op_index',
                'controll'=> 'op',
                'action'  => 'index',
                'url'     => DcUrl('admin/op/index','',''),
                'order'   => 1,
            ],
            [
                'ico'     => 'fa fa-fw fa-folder',
                'title'   => 'cache_index',
                'controll'=> 'cache',
                'action'  => 'index',
                'url'     => DcUrl('admin/cache/index','',''),
                'order'   => 11,
            ],
            [
                'ico'     => 'fa fa-fw fa-file-movie-o',
                'title'   => 'video_index',
                'controll'=> 'video',
                'action'  => 'index',
                'url'     => DcUrl('admin/video/index','',''),
                'order'   => 21,
            ],
            [
                'ico'     => 'fa fa-fw fa-upload',
                'title'   => 'upload_config',
                'controll'=> 'upload',
                'action'  => 'index',
                'url'     => DcUrl('admin/upload/index','',''),
                'order'   => 31,
            ],
        ],
        //系统
        'system' => [
            [
                'ico'     => 'fa fa-fw fa-wrench',
                'title'   => 'tool_index',
                'controll'=> 'tool',
                'action'  => 'index',
                'url'     => DcUrl('admin/tool/index','',''),
                'order'   => 1,
            ],
            [
                'ico'     => 'fa fa-fw fa-navicon',
                'title'   => lang('nav_index'),
                'controll'=> 'nav',
                'action'  => 'index',
                'url'     => DcUrl('admin/nav/index','',''),
                'order'   => 11,
            ],
            [
                'ico'     => 'fa fa-fw fa-leaf',
                'title'   => 'category_index',
                'controll'=> 'category',
                'action'  => 'index',
                'url'     => DcUrl('admin/category/index','',''),
                'order'   => 21,
            ],
            [
                'ico'     => 'fa fa-fw fa-tag',
                'title'   => 'tag_index',
                'controll'=> 'tag',
                'action'  => 'index',
                'url'     => DcUrl('admin/tag/index','',''),
                'order'   => 31,
            ],
            [
                'ico'     => 'fa fa-fw fa-user',
                'title'   => 'user_index',
                'controll'=> 'user',
                'action'  => 'index',
                'url'     => DcUrl('admin/user/index','',''),
                'order'   => 41,
            ],
            [
                'ico'     => 'fa fa-fw fa-filter',
                'title'   => 'auth_index',
                'controll'=> 'auth',
                'action'  => 'index',
                'url'     => DcUrl('admin/auth/index','',''),
                'order'   => 51,
            ],
            [
                'ico'     => 'fa fa-fw fa-cogs',
                'title'   => 'route_index',
                'controll'=> 'route',
                'action'  => 'index',
                'url'     => DcUrl('admin/route/index','',''),
                'order'   => 61,
            ],
            [
                'ico'     => 'fa fa-fw fa-anchor',
                'title'   => 'hook_index',
                'controll'=> 'hook',
                'action'  => 'index',
                'url'     => DcUrl('admin/hook/index','',''),
                'order'   => 71,
            ],
            [
                'ico'     => 'fa fa-fw fa-clone',
                'title'   => 'index_index',
                'controll'=> 'index',
                'action'  => 'index',
                'url'     => DcUrl('admin/index/index','',''),
                'order'   => 81,
            ],
        ],
        //应用
        'apply' => [
            [
                'ico'     => 'fa fa-fw fa-cloud',
                'title'   => 'apply_store',
                'controll'=> 'store',
                'action'  => 'index',
                'url'     => DcUrl('admin/store/index','',''),
                'order'   => 1,
            ],
            [
                'ico'     => 'fa fa-fw fa-archive',
                'title'   => 'apply_index',
                'controll'=> 'apply',
                'action'  => 'index',
                'url'     => DcUrl('admin/apply/index','',''),
                'order'   => 11,
            ],
            [
                'ico'     => 'fa fa-fw fa-gear',
                'title'   => 'apply_create',
                'controll'=> 'apply',
                'action'  => 'create',
                'url'     => DcUrl('admin/apply/create','',''),
                'order'   => 21,
            ],
        ],
    ]
];