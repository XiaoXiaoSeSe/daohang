<?php
namespace app\admin\controller;

use app\common\controller\Admin;

class Nav extends Admin
{

    /*
    protected $beforeActionList = [
        'postData'  =>  ['only'=>'save,update'],
        'second'    =>  ['except'=>'create'],
    ];
    */
    
    //添加数据保存至数据库
	public function save()
    {
        config('common.validate_name', 'common/Nav');
        $op_id = \daicuo\Nav::save(input('post.'));
		if($op_id < 1){
			$this->error(\daicuo\Nav::getError());
		}
		$this->success(lang('success'));
	}
    
    //删除(数据库)
	public function delete()
    {
		$ids = input('id/a');
		if(!$ids){
			$this->error(lang('mustIn'));
		}
        foreach($ids as $id){
            \daicuo\Nav::delete_id($id);
        }
        $this->success(lang('success'));
	}
    
    //修改（表单）
	public function edit()
    {
		$id = input('id/d',0);
		if(!$id){
			$this->error(lang('mustIn'));
		}
		//查询数据
        $data = \daicuo\Nav::get_id($id, false);
        if( is_null($data) ){
            $this->error(lang('empty'));
        }
		$this->assign('data', $data);
		return $this->fetch();
	}
    
    //修改（数据库）
	public function update()
    {
		$data = input('post.');
        if(!$data['op_id']){
			$this->error(lang('mustIn'));
		}
        config('common.validate_name', 'common/Nav');
        $info = \daicuo\Nav::update_id($data['op_id'], $data);
        if(is_null($info)){
            $this->error(\daicuo\Nav::getError());
        }
        $this->success(lang('success'));
	}

	public function index()
    {
        if($this->request->isAjax()){
            $args = array();
            $args['cache']    = false;
            $args['tree']     = true;
            $args['level']    = true;
            $args['fetchSql'] = false;
            $args['field']    = 'op_id,op_name,op_value,op_module,op_controll,op_action,op_order,op_status';
            $args['sort']     = input('get.sortName/s','op_order');
            $args['order']    = input('get.sortOrder/s','asc');
            $args['where']    = DcWhereFilter($this->query, ['op_module','op_controll','op_action','op_status'], 'eq');
            //$args['limit']  = 10;
            //$args['page']   = 1;
            if($this->query['searchText']){
                $args['where']['op_value'] = ['like','%'.DcHtml($this->query['searchText']).'%'];
            }
            $list = \daicuo\Nav::all($args);
            if(is_null($list)){
                return json([]);
            }
            return json($list);
        }
		$this->assign('query', $this->query);
		return $this->fetch();
	}
	
	//排序（拖拽ajax）
	public function sort()
    {
		if( !\daicuo\Op::sort(input('get.id')) ){
            $this->error(lang('fail'));
        }
        DcCacheTag('common/Nav/Item', 'clear');
        $this->success(lang('success'));
	}
    
    //预览
	public function preview()
    {
		$id = input('id/d',0);
		if(!$id){
			$this->error(lang('mustIn'));
		}
		//查询数据
        $data = \daicuo\Nav::get_id($id, false);
        if(!$data['nav_link']){
            $this->error(lang('empty'));
        }
        $this->redirect($data['nav_link'],302);
	}
	
}