<?php
namespace app\admin\controller;

use app\common\controller\Admin;

class Auth extends Admin
{
	public function save()
    {
        config('common.validate_name', 'common/Op');
        
		if( !$op_id = \daicuo\Op::save(input('post.')) ){
			$this->error(\daicuo\Op::getError());
		}
        
        DcCache('auth_all', null);
        
		$this->success(lang('success'));
	}
    
	public function delete()
    {
        $ids = input('id/a');
        
		if(!$ids){
			$this->error(lang('mustIn'));
		}
        
        foreach($ids as $id){
            \daicuo\Op::delete_id($id);
        }
        
        DcCache('auth_all', null);
        
        $this->success(lang('success'));
	}
    
	public function edit()
    {
        $id = input('id/d',0);
        
		if(!$id){
			$this->error(lang('mustIn'));
		}
        
        $data = \daicuo\Op::get_id($id, false);
        
        if( is_null($data) ){
            $this->error(lang('empty'));
        }
        
		$this->assign('data', $data);
        
		return $this->fetch();
	}
	
	//修改一条规则到数据库
	public function update()
    {
        $data = input('post.');
        
        if(!$data['op_id']){
			$this->error(lang('mustIn'));
		}
        
        config('common.validate_name', 'common/Op');
        
        $info = \daicuo\Op::update_id($data['op_id'], $data);
        
        if(is_null($info)){
            $this->error(\daicuo\Op::getError());
        }
        
        DcCache('auth_all', null);
        
        $this->success(lang('success'));
	}
    
	public function index()
    {
        if($this->request->isAjax()){
            $args = array();
            $args['cache'] = false;
            $args['field'] = 'op_id,op_name,op_value,op_module,op_controll,op_action,op_order,op_status';
            $args['sort']  = input('get.sortName/s','op_id');
            $args['order'] = input('get.sortOrder/s','desc');
            $args['where']['op_action'] = ['eq', 'auth'];
            if( $this->query['op_module'] ){
                $args['where']['op_module'] = ['eq', DcHtml($this->query['op_module'])];
            }
            if( $this->query['searchText'] ){
                $args['where']['op_name|op_value'] = ['like','%'.DcHtml($this->query['searchText'].'%')];
            }
            $list = \daicuo\Op::all($args);
            if($list){
                return json($list);
            }
            return json([]);
		}
        
		$this->assign('query', $this->query);
        
		return $this->fetch();
	}
	
}