<?php
namespace app\admin\controller;

use app\common\controller\Admin;

class Hook extends Admin
{
	//添加数据保存至数据库
    public function save()
    {
        config('common.validate_name', 'common/Hook');
        $op_id = \daicuo\Hook::save(input('post.'));
		if($op_id < 1){
			$this->error(\daicuo\Hook::getError());
		}
		$this->success(lang('success'));
    }
    
    //删除（数据库）
	public function delete()
    {
	    $ids = input('id/a');
		if(!$ids){
			$this->error(lang('mustIn'));
		}
        foreach($ids as $id){
            \daicuo\Hook::delete_id($id);
        }
        $this->success(lang('success'));
	}
    
    //修改（表单）
	public function edit()
    {
		$id = input('id/d',0);
		if(!$id){
			$this->error(lang('mustIn'));
		}
		//查询数据
        $data = \daicuo\Hook::get_id($id, false);
        if( is_null($data) ){
            $this->error(lang('empty'));
        }
		$this->assign('data', $data);
		return $this->fetch();
	}
	
	//修改（数据库）
	public function update()
    {
		$data = input('post.');
        if(!$data['op_id']){
			$this->error(lang('mustIn'));
		}
        config('common.validate_name', 'common/Hook');
        $info = \daicuo\Hook::update_id($data['op_id'], $data);
        if(is_null($info)){
            $this->error(\daicuo\Hook::getError());
        }
        $this->success(lang('success'));
	}
    
    //管理
    public function index()
    {
        if($this->request->isAjax()){
            $args = array();
            $args['cache'] = false;
            $args['field'] = 'op_id,op_name,op_value,op_module,op_controll,op_action,op_order,op_status';
            $args['sort']  = input('get.sortName/s','op_order');
            $args['order'] = input('get.sortOrder/s','asc');
            $args['where'] = DcWhereQuery(['op_module','op_controll','op_action'], 'eq', $this->query);
            if( $this->query['searchText'] ){
                $args['where']['op_value'] = ['like','%'.DcHtml($this->query['searchText'].'%')];
            }
            $infos = \daicuo\Hook::all($args);
            if($infos){
                return json($infos);
            }
            return json([]);
		}
        $this->assign('query', $this->query);
        return $this->fetch();
    }
    
    //排序（拖拽ajax）
    public function sort()
    {
        if( !\daicuo\Op::sort(input('get.id')) ){
            $this->error(lang('fail'));
        }
        DcCache('hook_all', NULL);
        $this->success(lang('success'));
	}
}