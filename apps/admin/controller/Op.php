<?php
namespace app\admin\controller;

use app\common\controller\Admin;

class Op extends Admin
{
	public function update()
    {
        $result = \daicuo\Op::write(
            array_merge( ['app_debug'=>'off','app_domain'=>'off','site_status'=>'off','site_captcha'=>'off'], input('post.') ),
            input('module/s','common'),
            input('controll/s','config'),
            input('action/s','system'),
            input('order/d',0),
            input('autoload/d','yes')
        );
		if( !$result ){
		    $this->error(lang('fail'));
        }
        $this->success(lang('success'));
	}
}