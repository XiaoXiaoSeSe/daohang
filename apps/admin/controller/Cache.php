<?php
namespace app\admin\controller;

use app\common\controller\Admin;

class Cache extends Admin
{
	//保存缓存配置
	public function update()
    {
        $config = array();
        $config['type'] = input('post.cache_type/s','File');
        $config['prefix'] = input('post.cache_prefix/s');
        $config['path'] = input('post.cache_path/s');
        $config['db'] = input('post.cache_db/s');
        $config['host'] = input('post.cache_host/s');
        $config['port'] = input('post.cache_port/s');
        $config['expire'] = input('post.cache_expire/d',0);
        $config['expire_detail'] = input('post.cache_expire_detail/d','');
        $config['expire_item'] = input('post.cache_expire_item/d','');
        //验证缓存服务是否支持
        if( !\daicuo\Cache::check($config['type']) ){
            $this->error(lang('unSupport').$config['type']);
        }
        //将配置写入数据库
        $result = \daicuo\Cache::write($config);
        if($result){
            \daicuo\Cache::rm();//清楚临时文件
            $this->success(lang('success'));
        }else{
            $this->error(lang('fail'));
        }
	}
    
}