<?php
namespace app\admin\controller;

use app\common\controller\Admin;

class Video extends Admin
{
    public function update()
    {
        $result = \daicuo\Op::write([
            'video_in'       => DcSwitch(input('post.video_in/s')),
            'video_size'     => input('post.video_size/s'),
            'video_ai'       => input('post.video_ai/s'),
            'video_frontUrl' => input('post.video_frontUrl/s'),
            'video_frontTime'=> input('post.video_frontTime/s'),
            'video_endUrl'   => input('post.video_endUrl/s'),
            'video_endTime'  => input('post.video_endTime/s'),
            'video_pause'    => input('post.video_pause/s'),
            'video_buffer'   => input('post.video_buffer/s'),
            'video_advUnit'  => input('post.video_advUnit/s'),
        ],'common','video','system','0','yes');
        if( !$result ){
            $this->error(lang('fail'));
        }
        $this->success(lang('success'));
    }
    
    public function index()
    {
        $items = [
            'video_in'   => [
                'type'   => 'switch',
                'value'  => config('common.video_in'),
            ],
            'video_size' => [
                'type'   => 'custom',
                'value'  => config('common.video_size'),
                'option' => [
                    '16by9'=>lang('video_size_16by9'),
                    '21by9'=>lang('video_size_21by9'),
                    '4by3'=>lang('video_size_4by3'),
                    '1by1'=>lang('video_size_1by1')
                ],
                'class_left'  => 'col-md-2',
                'class_right' => 'col-auto',
            ],
            'video_ai'  => [
                'type'  => 'text',
                'value' => config('common.video_ai'),
            ],
            'video_frontUrl'  => [
                'type'  => 'text',
                'value' => config('common.video_frontUrl'),
            ],
            'video_frontTime' => [
                'type'  => 'text',
                'value' => config('common.video_frontTime'),
            ],
            'video_endUrl'   => [
                'type'  => 'text',
                'value' => config('common.video_endUrl'),
            ],
            'video_endTime'  => [
                'type'  => 'text',
                'value' => config('common.video_endTime'),
            ],
            'video_pause'    => [
                'type'  => 'text',
                'value' => config('common.video_pause'),
            ],
            'video_buffer'   => [
                'type'  => 'text',
                'value' => config('common.video_buffer'),
            ],
            'video_advUnit'  => [
                'type'  => 'hidden',
                'value' => config('common.video_advUnit'),
            ],
        ];
        $this->assign('items', $items);
        $this->assign('query', $this->query);
        return $this->fetch();
    }
}