<?php
// 返回本地应用列表
function DcAdminApply(){
	$dirs = array();
	foreach(glob(APP_PATH.'*',GLOB_ONLYDIR) as $key=>$value){
		if(!in_array(basename($value),array('admin','api','common','extra','lang'))){
			array_push($dirs, basename($value));
		}
	}
	return $dirs;
}
//返回排序后的后台菜单
function DcAdminMenuOrder($item=[], $sortName='order', $sortOrder='asc'){
    return list_sort_by($item, $sortName, $sortOrder);
}
//返回后台菜单的某一列
function DcAdminMenuColumn($array_key='system', $column_key='controll'){
    return array_column(config('admin_menu.'.$array_key), $column_key);
}
//返回后台菜单的ICO
function DcAdminMenuIco($value=''){
    return DcEmpty(DcHtml($value), 'fa fa-fw fa-angle-double-right');
}
//返回后台菜单的标题
function DcAdminMenuTitle($value=''){
    return DcSubstr(DcHtml(lang($value)),0,6,false);
}
//返回后台菜单的展示
function DcAdminMenuShow($controll='addon', $action='index', $menuKey='config', $module=''){
    //手动高亮
    if(input('active/s','') == $menuKey){
        return 'show';
    }
    //自动高亮 module+controll+action
    if($controll=='addon' && $action='index'){
        $module   = input('module/s','');
        $controll = input('controll/s','');
        $action   = input('action/s','');
    }
    $list = [];
    foreach(config('admin_menu.'.$menuKey) as $key=>$value){
        array_push($list, $value['module'].$value['controll'].$value['action']);
    }
    if( in_array($module.$controll.$action, $list) ){
        return 'show';
    }
    return '';
}
//返回后台菜单的颜色
function DcAdminMenuColor($controll='addon', $action='index', $menuModule='', $menuControll='', $menuAction='', $module=''){
    if($controll=='addon' && $action='index'){
        $module   = input('module/s','');
        $controll = input('controll/s','');
        $action   = input('action/s','');
    }
    return DcDefault($module.$controll.$action, $menuModule.$menuControll.$menuAction, 'text-purple', 'text-muted');
}
//返回后台插件的展示状态
function DcAdminAddonShow($menuModule='index'){
    if( input('active/s','') ){
        return '';
    }
    return DcDefault(input('module/s',''), $menuModule, 'show', '');
}
//返回后台插件菜单的颜色
function DcAdminAddonColor($menuModule='index', $menuControll='admin', $menuAction='index'){
    $module   = input('module/s','');
    $controll = input('controll/s','');
    $action   = input('action/s','');
    return DcDefault($module.$controll.$action, $menuModule.$menuControll.$menuAction, 'text-purple', 'text-muted');
}