{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("user_index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("user_index")}
</h6>
<!-- -->
<form action="{:DcUrl('admin/user/delete','','')}" method="post" data-toggle="form">
<div class="toolbar mb-2" id="toolbar">
  <a class="btn btn-sm btn-light border" href="javascript:;" data-toggle="reload">
    <i class="fa fa-refresh fa-fw"></i> {:lang('refresh')}
  </a>
  <a class="btn btn-sm btn-outline-purple" href="{:DcUrl('admin/user/create',$query,'')}" data-toggle="create" data-modal-lg="true">
    <i class="fa fa-plus fa-fw"></i> {:lang('create')}
  </a>
  <button class="btn btn-sm btn-outline-danger" type="submit" data-toggle="delete">
    <i class="fa fa-trash"></i> {:lang('delete')}
  </button>
</div>
{:DcBuildTable([
    'data-escape'             => 'true',
    'data-toggle'             => 'bootstrap-table',
    'data-url'                => DcUrl('admin/user/index', $query, ''),
    'data-url-sort'           => '',
    'data-url-preview'        => '',
    'data-url-edit'           => DcUrl('admin/user/edit', ['id'=>''], ''),
    'data-url-delete'         => DcUrl('admin/user/delete', ['id'=>''], ''),
    'data-buttons-prefix'     => 'btn',
    'data-buttons-class'      => 'purple',
    'data-buttons-align'      => 'none float-md-right',
    'data-icon-size'          => 'sm',
    
    'data-toolbar'            => '.toolbar',
    'data-toolbar-align'      => 'none float-md-left',
    'data-buttons-align'      => 'none',
    'data-search-align'       => 'none float-md-right',
    'data-search'             => 'true',
    'data-show-search-button' => 'true',
    'data-show-refresh'       => 'false',
    'data-show-toggle'        => 'false',
    'data-show-fullscreen'    => 'false',
    'data-smart-display'      => 'false',
    
    'data-unique-id'          => 'user_id',
    'data-id-field'           => 'user_id',
    'data-select-item-name'   => 'id[]',
    'data-query-params-type'  => 'params',
    'data-query-params'       => 'daicuo.table.query',
    'data-sort-name'          => 'user_id',
    'data-sort-order'         => 'desc',
    'data-sort-class'         => 'table-active',
    'data-sort-stable'        => 'true',
    
    'data-side-pagination'    => 'server',
    'data-total-field'        => 'total',
    'data-data-field'         => 'data',
    'data-pagination'         => 'true',
    
    'data-page-number'        => $page,
    'data-page-size'          => '20',
    'data-page-list'          => '[20, 50, 100]',
    
    'columns'=>[
        [
            'data-checkbox'=>'true',
        ],
        [
            'data-field'=>'user_id',
            'data-title'=>'id',
            'data-sortable'=>'true',
            'data-sort-name'=>'user_id',
            'data-sort-order'=>'desc',
            //'data-width'=>'30',
            //'data-width-unit'=>'px',
            'data-class'=>'',
            'data-align'=>'center',
            'data-valign'=>'middle',
            'data-halign'=>'center',
            'data-falign'=>'center',
            'data-visible'=>'true',
            //'data-formatter'=>'',
            //'data-footer-formatter'=>'',
        ],
        [
            'data-field'=>'user_name',
            //'data-width'=>'30',
            //'data-width-unit'=>'%',
            'data-title'=>lang('user_name'),
        ],
        [
            'data-field'=>'user_email',
            'data-title'=>lang('user_email'),
            'data-align'=>'center',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'user_mobile',
            'data-title'=>lang('user_mobile'),
            'data-align'=>'center',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'user_token',
            'data-title'=>lang('user_token'),
            'data-align'=>'center',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'user_status_text',
            'data-title'=>lang('user_status'),
            'data-align'=>'center',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'user_create_time',
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-title'=>lang('user_create_time'),
        ],
        [
            'data-field'=>'user_update_time',
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-title'=>lang('user_update_time'),
        ],
        [
            'data-field'=>'user_create_ip',
            'data-title'=>lang('user_create_ip'),
            'data-align'=>'center',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'user_update_ip',
            'data-title'=>lang('user_update_ip'),
            'data-align'=>'center',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'operate',
            'data-title'=>lang('operate'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'100',
            'data-width-unit'=>'px',
            'data-events'=>'',
            'data-formatter'=>'daicuo.table.operate',
        ]
    ]
])}
</form>
{/block}