{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("category_index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
{:lang("category_index")}
</h6>
<!-- -->
<form action="{:DcUrl('admin/category/delete','','')}" method="post" data-toggle="form">
<div class="toolbar mb-2" id="toolbar">
  <a class="btn btn-sm btn-light border" href="javascript:;" data-toggle="reload">
    <i class="fa fa-refresh fa-fw"></i> {:lang('refresh')}
  </a>
  <a class="btn btn-sm btn-outline-purple" href="{:DcUrl('admin/category/create',$query,'')}" data-toggle="create" data-modal-lg="true">
    <i class="fa fa-plus fa-fw"></i> {:lang('create')}
  </a>
  <button class="btn btn-sm btn-outline-danger" type="submit" data-toggle="delete">
    <i class="fa fa-trash"></i> {:lang('delete')}
  </button>
</div>
{:DcBuildTable([
    'data-escape'             => 'false',
    'data-toggle'             => 'bootstrap-table',
    'data-url'                => DcUrl('admin/category/index', $query, ''),
    'data-url-sort'           => '',
    'data-url-preview'        => '',
    'data-url-edit'           => DcUrl('admin/category/edit', ['id'=>''], ''),
    'data-url-delete'         => DcUrl('admin/category/delete', ['id'=>''], ''),
    'data-buttons-prefix'     => 'btn',
    'data-buttons-class'      => 'purple',
    'data-buttons-align'      => 'none float-md-right',
    'data-icon-size'          => 'sm',
    
    'data-toolbar'            => '.toolbar',
    'data-toolbar-align'      => 'none float-md-left',
    'data-buttons-align'      => 'none',
    'data-search-align'       => 'none float-md-right',
    'data-search'             => 'true',
    'data-show-search-button' => 'true',
    'data-show-refresh'       => 'false',
    'data-show-toggle'        => 'false',
    'data-show-fullscreen'    => 'false',
    'data-smart-display'      => 'false',
    
    'data-unique-id'          => 'term_id',
    'data-id-field'           => 'term_id',
    'data-select-item-name'   => 'id[]',
    'data-query-params-type'  => 'params',
    'data-query-params'       => 'daicuo.table.query',
    'data-sort-name'          => 'tree',
    'data-sort-order'         => 'desc',
    'data-sort-class'         => 'table-active',
    'data-sort-stable'        => 'true',
    
    'data-side-pagination'    => 'server',
    'data-total-field'        => 'total',
    'data-data-field'         => 'data',
    'data-pagination'         => 'true',
    
    'data-page-number'        => $page,
    'data-page-size'          => '20',
    'data-page-list'          => '[20, 50, 100]',
    
    'columns'=>[
        [
            'data-checkbox'   => 'true',
        ],
        [
            'data-field'      => 'term_id',
            'data-title'      => 'id',
            'data-width'      => '5',
            'data-width-unit' => '%',
            'data-sortable'   => 'true',
            'data-sort-name'  => 'term_id',
            'data-sort-order' => 'asc',
            'data-class'      => '',
            'data-align'      => 'center',//left|center|right
            'data-valign'     => 'middle',//top|middle|bottom
            'data-halign'     => 'center',//表格页头
            'data-falign'     => 'center',//表格页脚
            'data-visible'    => 'true',
        ],
        [
            'data-field'=>'term_order',
            'data-title'=>lang('weight'),
            'data-sortable'=>'true',
            'data-sort-name'=>'term_order',
            'data-sort-order'=>'asc',
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'60',
            'data-width-unit'=>'px',
        ],
        [
            'data-field'=>'term_name',
            'data-title'=>lang('name'),
            'data-align'=>'left',
            'data-halign'=>'center',
            'data-escape'=>'true',
        ],
        [
            'data-field'=>'term_slug',
            'data-title'=>lang('slug'),
            'data-align'=>'left',
            'data-halign'=>'center',
            'data-escape'=>'true',
        ],
        [
            'data-field'=>'term_parent',
            'data-title'=>lang('parent'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'6',
            'data-width-unit'=>'%',
            'data-sortable'=>'true',
            'data-sort-name'=>'term_parent',
            'data-sort-order'=>'desc',
        ],
        [
            'data-field'=>'term_count',
            'data-title'=>lang('count'),
            'data-sortable'=>'true',
            'data-sort-name'=>'term_count',
            'data-sort-order'=>'desc',
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'6',
            'data-width-unit'=>'%',
        ],
        [
            'data-field'=>'term_status_text',
            'data-title'=>lang('status'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'6',
            'data-width-unit'=>'%',
        ],
        [
            'data-field'=>'term_module',
            'data-title'=>lang('module'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'6',
            'data-width-unit'=>'%',
            'data-formatter'=>'daicuo.admin.op.bakModule',
        ],
        [
            'data-field'=>'operate',
            'data-title'=>lang('operate'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'120',
            'data-width-unit'=>'px',
            'data-formatter'=>'daicuo.table.operate',
        ]
    ]
])}
</form>
{/block}