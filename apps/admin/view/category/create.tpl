<div class="modal-content">
    <div class="modal-body"> 
    {:DcBuildForm([
        'name'     => 'category_create',
        'class'    => 'bg-white px-2 py-2 form-create',
        'action'   => DcUrl('admin/category/save', '', ''),
        'method'   => 'post',
        'submit'   => lang('submit'),
        'reset'    => lang('reset'),
        'close'    => lang('close'),
        'ajax'     => true,
        'disabled' => false,
        'callback' => '',
        'data'     => '',
        'items'    => DcFormItems([
            'term_type'      => [
                'type'       => 'hidden',
                'value'      => 'category',
            ],
            'term_name'       => [
                'type'        => 'text',
                'value'       => '',
                'required'    => true,
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_slug'       => [
                'type'        => 'text',
                'value'       => '',
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_parent'     => [
                'type'        => 'select',
                'value'       => '',
                'option'      => DcTermOption(['module'=>input('get.op_module')]),
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_module'     => [
                'type'        => 'text',
                'value'       => '',
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_info'       => [
                'type'        => 'text',
                'value'       => '',
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_order'      => [
                'type'        => 'number',
                'value'       => '',
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_tpl'        => [
                'type'        => 'text',
                'value'       => 'index',
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
        ]),
    ])}
    </div>
</div>
