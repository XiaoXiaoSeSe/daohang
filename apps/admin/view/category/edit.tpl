<div class="modal-content">
    <div class="modal-body">
    {:DcBuildForm([
        'name'     => 'category_edit',
        'class'    => 'bg-white px-2 py-2 form-edit',
        'action'   => DcUrl('admin/category/update', '', ''),
        'method'   => 'post',
        'submit'   => lang('submit'),
        'reset'    => lang('reset'),
        'close'    => lang('close'),
        'disabled' => false,
        'ajax'     => true,
        'callback' => '',
        'data'     => $data,
        'items'    => DcFormItems([
            'term_id'       => [
                'type'       => 'hidden',
                'value'      => $data['term_id'],
            ],
            'term_type'      => [
                'type'       => 'hidden',
                'value'      => 'category',
            ],
            'term_name'       => [
                'type'        => 'text',
                'value'       => $data['term_name'],
                'required'    => true,
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_slug'       => [
                'type'        => 'text',
                'value'       => $data['term_slug'],
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_parent'     => [
                'type'        => 'select',
                'value'       => $data['term_parent'],
                'option'      => DcTermOption(['module'=>input('get.op_module')]),
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_module'     => [
                'type'        => 'text',
                'value'       => $data['term_module'],
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_info'       => [
                'type'        => 'text',
                'value'       => $data['term_info'],
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_order'      => [
                'type'        => 'number',
                'value'       => $data['term_order'],
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
            'term_tpl'        => [
                'type'        => 'text',
                'value'       => DcEmpty($data['term_tpl'],'index'),
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ],
        ]),
    ])}
    </div>
</div>