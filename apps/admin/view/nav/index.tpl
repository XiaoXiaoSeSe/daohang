{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("nav_index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("nav_index")}
</h6>
<!-- -->
<form action="{:DcUrl('admin/nav/delete','','')}" method="post" data-toggle="form">
<input type="hidden" name="_method" value="delete">
<div id="toolbar" class="toolbar mb-2">
  <a class="btn btn-sm btn-light border" href="javascript:;" data-toggle="reload">
    <i class="fa fa-refresh fa-fw"></i>
    {:lang('refresh')}
  </a>
  <a class="btn btn-sm btn-outline-purple" href="{:DcUrl('admin/nav/create',$query,'')}" data-toggle="create" data-modal-lg="true" data-callback="daicuo.admin.nav.bakEdit">
    <i class="fa fa-plus fa-fw"></i>
    {:lang('create')}
  </a>
  <button class="btn btn-sm btn-outline-danger" type="submit" data-toggle="delete">
    <i class="fa fa-trash"></i>
    {:lang('delete')}
  </button>
</div>
{:DcBuildTable([
    'data-escape'             => 'true',
    'data-toggle'             => 'bootstrap-table',
    'data-url'                => DcUrl('admin/nav/index', $query, ''),
    'data-url-sort'           => DcUrl('admin/nav/sort', ['id'=>''], ''),
    'data-url-preview'        => DcUrl('admin/nav/preview', ['id'=>''], ''),
    'data-url-edit'           => DcUrl('admin/nav/edit', ['id'=>''], ''),
    'data-url-delete'         => DcUrl('admin/nav/delete', ['id'=>''], ''),
    'data-buttons-prefix'     => 'btn',
    'data-buttons-class'      => 'purple',
    'data-buttons-align'      => 'none float-md-right',
    'data-icon-size'          => 'sm',
    
    'data-toolbar'            => '.toolbar',
    'data-toolbar-align'      => 'none float-md-left',
    'data-search-align'       => 'none float-md-right',
    'data-search'             => 'true',
    'data-show-search-button' => 'true',
    'data-show-refresh'       => 'false',
    'data-show-toggle'        => 'false',
    'data-show-fullscreen'    => 'false',
    'data-smart-display'      => 'false',
    
    'data-unique-id'          => 'op_id',
    'data-id-field'           => 'op_id',
    'data-select-item-name'   => 'id[]',
    'data-query-params-type'  => 'params',
    'data-query-params'       => 'daicuo.table.query',
    'data-sort-name'          => 'op_order',
    'data-sort-order'         => 'asc',
    'data-sort-class'         => 'table-active',
    'data-sort-stable'        => 'true',
    
    'data-side-pagination'    => 'server',
    //'data-total-field'      => 'total',
    //'data-data-field'       => 'data',
    
    //'data-page-number'      => $page,
    //'data-page-size'        => '30',
    //'data-page-list'        => '[10, 25, 50, 100]',
    
    'data-pagination'         => 'false',
    //'data-pagination-h-align'        => 'left',
    //'data-pagination-detail-h-align' => 'right',
    //'data-pagination-v-align'        => 'top',
    //'data-show-extended-pagination'  => 'true',
    
    'columns'=>[
        [
            'data-checkbox'=>'true',
        ],
        [
            'data-field'=>'op_id',
            'data-title'=>'id',
            'data-align'=>'center',
            'data-valign'=>'middle',
            'data-halign'=>'center',
            'data-falign'=>'center',
            'data-visible'=>'true',
            'data-width'=>'60',
            'data-width-unit'=>'px',
            'data-sortable'=>'true',
            'data-sort-name'=>'op_id',
            'data-sort-order'=>'desc',
            'data-class'=>'',
            'data-formatter'=>'',
            'data-footer-formatter'=>'',
        ],
        [
            'data-field'=>'op_order',
            'data-title'=>lang('weight'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
            'data-sortable'=>'true',
            'data-sort-name'=>'op_order',
            'data-sort-order'=>'asc',
            'data-formatter'=>'daicuo.table.sort',
        ],
        [
            'data-field'=>'nav_ico',
            'data-title'=>lang('ico'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'50',
            'data-width-unit'=>'px',
            'data-formatter'=>'daicuo.table.ico',
        ],
        [
            'data-field'=>'nav_text',
            'data-title'=>lang('nav_text'),
            'data-align'=>'left',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'nav_type',
            'data-title'=>lang('nav_type'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'100',
            'data-width-unit'=>'px',
        ],
        [
            'data-field'=>'nav_target',
            'data-title'=>lang('target'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'100',
            'data-width-unit'=>'px',
        ],
        [
            'data-field'=>'nav_active',
            'data-title'=>lang('active'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
        ],
        [
            'data-field'=>'op_status_text',
            'data-title'=>lang('status'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
        ],
        [
            'data-field'=>'op_module',
            'data-title'=>lang('module'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
            'data-formatter'=>'daicuo.admin.op.bakModule',
        ],
        [
            'data-field'=>'op_controll',
            'data-title'=>lang('controll'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
            'data-formatter'=>'daicuo.admin.op.bakControll',
        ],
        [
            'data-field'=>'op_action',
            'data-title'=>lang('action'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
            'data-formatter'=>'daicuo.admin.op.bakAction',
        ],
        [
            'data-field'=>'operate',
            'data-title'=>lang('operate'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'150',
            'data-width-unit'=>'px',
            'data-escape'=>false,
            'data-events'=>'daicuo.admin.nav.events',
            'data-formatter'=>'daicuo.table.operate',
        ]
    ]
])}
</form>
{/block}