{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("upload_config")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("upload_config")}
</h6>
{:DcBuildForm([
    'name'      => 'upload_index',
    'class'     => 'py-2',
    'action'    => DcUrl('admin/upload/update', '', ''),
    'method'    => 'post',
    'submit'    => lang('submit'),
    'reset'     => lang('reset'),
    'close'     => false,
    'disabled'  => false,
    'ajax'      => true,
    'callback'  => '',
    'items'     => DcFormItems($items),
])}
{/block}