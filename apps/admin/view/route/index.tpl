{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("route_index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<form action="{:DcUrl('admin/route/delete','','')}" method="post" data-toggle="form">
<input type="hidden" name="_method" value="delete">
<h6 class="border-bottom pb-2 text-purple">
  {:lang("route_index")}
</h6>
<div id="toolbar" class="toolbar mb-2">
  <a class="btn btn-sm btn-light border" href="javascript:;" data-toggle="reload">
    <i class="fa fa-refresh"></i>
    {:lang('refresh')}
  </a>
  <a class="btn btn-sm btn-light border" href="{:DcUrl('admin/route/create',$query,'')}" data-toggle="create">
    <i class="fa fa-plus"></i>
    {:lang('create')}
  </a>
  <button class="btn btn-sm btn-outline-danger" type="submit" data-toggle="delete">
    <i class="fa fa-trash"></i>
    {:lang('delete')}
  </button>
</div>
{:DcBuildTable([
    'data-escape'             => 'true',
    'data-toggle'             => 'bootstrap-table',
    'data-url'                => DcUrl('admin/route/index', $query, ''),
    'data-url-sort'           => DcUrl('admin/route/sort', ['id'=>''], ''),
    'data-url-preview'        => DcUrl('admin/route/preview', ['id'=>''], ''),
    'data-url-edit'           => DcUrl('admin/route/edit', ['id'=>''], ''),
    'data-url-delete'         => DcUrl('admin/route/delete', ['id'=>''], ''),
    'data-buttons-prefix'     => 'btn',
    'data-buttons-class'      => 'purple',
    'data-buttons-align'      => 'none float-md-right',
    'data-icon-size'          => 'sm',
    
    'data-toolbar'            => '.toolbar',
    'data-toolbar-align'      => 'none float-md-left',
    'data-buttons-align'      => 'none',
    'data-search-align'       => 'none float-md-right',
    'data-search'             => 'true',
    'data-show-search-button' => 'false',
    'data-show-refresh'       => 'false',
    'data-show-toggle'        => 'false',
    'data-show-fullscreen'    => 'false',
    'data-smart-display'      => 'false',
    
    'data-unique-id'          => 'op_id',
    'data-id-field'           => 'op_id',
    'data-select-item-name'   => 'id[]',
    'data-query-params-type'  => 'params',
    'data-query-params'       => 'daicuo.table.query',
    'data-sort-name'          => 'op_order',
    'data-sort-order'         => 'asc',
    'data-sort-class'         => 'table-active',
    'data-sort-stable'        => 'true',
    
    'data-side-pagination'    => 'server',
    //'data-total-field'      => 'total',
    //'data-data-field'       => 'data',
    
    //'data-page-number'      => $page,
    //'data-page-size'        => '30',
    //'data-page-list'        => '[10, 25, 50, 100]',
    
    'data-pagination'         => 'false',
    //'data-pagination-h-align'        => 'left',
    //'data-pagination-detail-h-align' => 'right',
    //'data-pagination-v-align'        => 'top',
    //'data-show-extended-pagination'  => 'true',
    'columns'=>[
        [
            'data-checkbox'=>'true',
        ],
        [
            'data-field'=>'op_id',
            'data-title'=>'id',
            'data-width'=>'60',
            'data-width-unit'=>'px',
            'data-sortable'=>'true',
            'data-sort-name'=>'op_id',
            'data-sort-order'=>'asc',
            'data-class'=>'',
            'data-align'=>'center',
            'data-valign'=>'middle',
            'data-halign'=>'center',
            'data-falign'=>'center',
            'data-visible'=>'true',
            'data-formatter'=>'',
            'data-footer-formatter'=>'',
        ],
        [
            'data-field'=>'op_order',
            'data-title'=>lang('weight'),
            'data-width'=>'80',
            'data-width-unit'=>'px',
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-sortable'=>'true',
            'data-sort-name'=>'op_order',
            'data-sort-order'=>'asc',
            'data-formatter'=>'daicuo.table.sort',
        ],
        [
          'data-field'=>'rule',
          'data-title'=>lang('route_rule'),
        ],
        [
          'data-field'=>'address',
          'data-title'=>lang('route_address'),
        ],
        [
          'data-field'=>'method',
          'data-title'=>lang('route_method'),
          'data-align'=>'center',
          'data-halign'=>'center',
          'data-width'=>'80',
          'data-width-unit'=>'px',
        ],
        [
            'data-field'=>'op_status_text',
            'data-title'=>lang('status'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
        ],
        [
            'data-field'=>'op_module',
            'data-title'=>lang('module'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
            'data-formatter'=>'daicuo.admin.op.bakModule',
        ],
        [
            'data-field'=>'op_controll',
            'data-title'=>lang('controll'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
            'data-formatter'=>'daicuo.admin.op.bakControll',
        ],
        [
            'data-field'=>'op_action',
            'data-title'=>lang('action'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'80',
            'data-width-unit'=>'px',
            'data-formatter'=>'daicuo.admin.op.bakAction',
        ],
        [
          'data-field'=>'operate',
          'data-title'=>lang('operate'),
          'data-width'=>'150',
          'data-width-unit'=>'px',
          'data-align'=>'center',
          'data-halign'=>'center',
          'data-escape'=>false,
          'data-events'=>'daicuo.admin.route.events',
          'data-formatter'=>'daicuo.table.operate',
        ]
    ]
])}
</form>
{/block}