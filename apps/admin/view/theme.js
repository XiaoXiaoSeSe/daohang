$(function() {
    //扩展daicuo.admin
    $.extend(daicuo.admin, {
        // 初始化
        init: function(){
            daicuo.admin.sideBar();
            daicuo.admin.versionCheck();
            daicuo.admin.nav.onChange();//导航筛选事件
        },
        // 侧栏
        sideBar: function() {
            $(document).on('click', '[data-toggle="main-left"]', function() {
                $('.main-left').toggleClass('open');
                $('.main-left').toggleClass('d-block');
                $('.main-right').toggleClass('col-12');
            });
        },
        // 版本检测
        versionCheck: function($module) { //获取服务器最新版本jsonp格式
            var $dom = new Array();
            $('[data-toggle="version"]').each(function(key, value) {
                $dom[key] = $(this);
                $version = $dom[key].attr('data-version');
                $module = $dom[key].attr('data-module');
                if (!$version || !$module) {
                    $this.removeClass();
                    return false;
                }
                //后端请求防止恶意JS
                $.ajax({
                    type: 'get',
                    cache: false,
                    url: daicuo.config.file + '/version/index/?version=' + $version + '&module=' + $module,
                    dataType: 'json',
                    timeout: 3000,
                    success: function($json) {
                        if ($json.code == 1) {
                            $dom[key].html('<a class="text-danger" href="' + $json.update + '" target="_blank">' + $json.msg + '</a>');
                        }
                    }
                });
                $dom[key].removeClass();
            });
        },
        // 配置模块
        op:{
            //data-formatter 模型筛选
            bakModule: function(value, row, index, field){
                var $url = daicuo.config.file + '/' + daicuo.config.controll + '/index/?op_module='+value;
                return '<a class="text-purple" href="'+$url+'">'+value+'</a>';
            },
            //data-formatter 控制器筛选
            bakControll: function(value, row, index, field){
                var $url = daicuo.config.file + '/' + daicuo.config.controll + '/index/?op_controll='+value;
                return '<a class="text-purple" href="'+$url+'">'+value+'</a>';
            },
            //data-formatter 操作筛选
            bakAction: function(value, row, index, field){
                var $url = daicuo.config.file + '/' + daicuo.config.controll + '/index/?op_action='+value;
                return '<a class="text-purple" href="'+$url+'">'+value+'</a>';
            }
        },
        // 路由模块
        route : {
            //编辑按钮回调
            bakEdit:function($data, $status, $xhr) {
                //$('.form-edit input[name="rule"]').attr('readonly',true);
                daicuo.bootstrap.dialogForm($data);
                daicuo.json.beauty();
            },
            //data-events 点击事件
            events: {
                'click [data-toggle="edit"]': function (event, value, row, index) {
                    $(event.currentTarget).attr('data-callback','daicuo.admin.route.bakEdit');
                }
            }
        },
        // 导航模块
        nav : {
            //监听筛选事件
            onChange: function(){
                $(document).on("change", '#nav_type', function(){
                    daicuo.admin.nav.showAddon($(this).val());
                });
            },
            //是否显示插件内部导航
            showAddon: function($value){
                if($value == 'addon'){
                    $('.dc-modal #nav_url').parents('.form-group').addClass('d-none');
                    $('.dc-modal #nav_module').parents('.form-group').removeClass('d-none');
                    $('.dc-modal #nav_controll').parents('.form-group').removeClass('d-none');
                    $('.dc-modal #nav_action').parents('.form-group').removeClass('d-none');
                    $('.dc-modal #nav_params').parents('.form-group').removeClass('d-none');
                    $('.dc-modal #nav_suffix').parents('.form-group').removeClass('d-none');
                }else{
                    $('.dc-modal #nav_url').parents('.form-group').removeClass('d-none');
                    $('.dc-modal #nav_module').parents('.form-group').toggleClass('d-none',true);
                    $('.dc-modal #nav_controll').parents('.form-group').toggleClass('d-none',true);
                    $('.dc-modal #nav_action').parents('.form-group').toggleClass('d-none',true);
                    $('.dc-modal #nav_params').parents('.form-group').toggleClass('d-none',true);
                    $('.dc-modal #nav_suffix').parents('.form-group').toggleClass('d-none',true);
                }
            },
            //编辑按钮回调
            bakEdit: function($data, $status, $xhr) {
                daicuo.bootstrap.dialogForm($data);//展示表单数据
                daicuo.admin.nav.showAddon($('.dc-modal #nav_type').val());//刷新是否显示插件内部链接
            },
            //data-events 点击事件
            events: {
                'click [data-toggle="edit"]': function (event, value, row, index) {
                    $(event.currentTarget).attr('data-callback','daicuo.admin.nav.bakEdit');
                }
            }
        },
        // 应用模块
        store : {
            //筛选选项
            query: function(params){
              return {
                 pageNumber: params.pageNumber, 
                 pageSize: params.pageSize,
                 sortName: params.sortName,
                 sortOrder: params.sortOrder,
                 searchText: params.searchText,
                 termId: $("#term_id").val(),
                 price: $("#price").val()
              }; 
            }
        }
    }); //extend
    window.daicuo.admin.init();
    window.daicuo.ajax.init();
    window.daicuo.form.init();
    window.daicuo.upload.init();
    window.daicuo.json.init();
    window.daicuo.table.init();
    window.daicuo.tags.init();
}); //jquery