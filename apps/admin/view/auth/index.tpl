{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("auth_index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<form action="{:DcUrl('admin/auth/delete','','')}" method="post" data-toggle="form">
<input type="hidden" name="_method" value="delete">
<h6 class="border-bottom pb-2 text-purple">
  {:lang("auth_index")}
</h6>
<div id="toolbar" class="toolbar mb-2">
  <a class="btn btn-sm btn-light border" href="javascript:;" data-toggle="reload">
    <i class="fa fa-refresh"></i>
    {:lang('refresh')}
  </a>
  <a class="btn btn-sm btn-light border" href="{:DcUrl('admin/auth/create',$query,'')}" data-toggle="create">
    <i class="fa fa-plus"></i>
    {:lang('create')}
  </a>
  <button class="btn btn-sm btn-outline-danger" type="submit" data-toggle="delete">
    <i class="fa fa-trash"></i>
    {:lang('delete')}
  </button>
</div>
{:DcBuildTable([
    'data-escape'             => false,
    'data-toggle'             => 'bootstrap-table',
    'data-url'                => DcUrl('admin/auth/index', $query, ''),
    'data-url-sort'           => '',
    'data-url-preview'        => '',
    'data-url-edit'           => DcUrl('admin/auth/edit', ['id'=>''], ''),
    'data-url-delete'         => DcUrl('admin/auth/delete', ['id'=>''], ''),
    'data-buttons-prefix'     => 'btn',
    'data-buttons-class'      => 'purple',
    'data-buttons-align'      => 'none float-md-right',
    'data-icon-size'          => 'sm',
    
    'data-toolbar'            => '.toolbar',
    'data-toolbar-align'      => 'none float-md-left',
    'data-buttons-align'      => 'none',
    'data-search-align'       => 'none float-md-right',
    'data-search'             => 'true',
    'data-show-search-button' => 'false',
    'data-show-refresh'       => 'false',
    'data-show-toggle'        => 'false',
    'data-show-fullscreen'    => 'false',
    'data-smart-display'      => 'false',
    
    'data-unique-id'          => 'op_id',
    'data-id-field'           => 'op_id',
    'data-select-item-name'   => 'id[]',
    'data-query-params-type'  => 'params',
    'data-query-params'       => 'daicuo.table.query',
    'data-sort-name'          => 'op_id',
    'data-sort-order'         => 'desc',
    'data-sort-class'         => 'table-active',
    'data-sort-stable'        => 'true',
    
    'data-side-pagination'    => 'server',
    //'data-total-field'      => 'total',
    //'data-data-field'       => 'data',
    //'data-pagination'       => 'true',
    
    //'data-page-number'      => $page,
    //'data-page-size'        => '30',
    //'data-page-list'        => '[10, 25, 50, 100]',
    'columns'=>[
        [
            'data-checkbox'   => 'true',
        ],
        [
            'data-field'      => 'op_id',
            'data-title'      => 'id',
            'data-width'      => '60',
            'data-width-unit' => 'px',
            'data-sortable'   => 'true',
            'data-sort-name'  => 'op_id',
            'data-sort-order' => 'asc',
            'data-class'      => '',
            'data-align'      => 'center',
            'data-valign'     => 'middle',
            'data-halign'     => 'center',
            'data-falign'     => 'center',
            'data-visible'    => 'true',
            'data-escape'     => false,
            'data-events'     => '',
            'data-formatter'  => '',
            'data-footer-formatter'=>'',
        ],
        [
          'data-field'        => 'op_name',
          'data-title'        => lang('auth_roles'),
          'data-escape'       => true,
        ],
        [
          'data-field'        => 'op_value',
          'data-title'        => lang('auth_caps'),
          'data-escape'       => true,
        ],
        [
            'data-field'      => 'op_status_text',
            'data-title'      => lang('status'),
            'data-align'      => 'center',
            'data-halign'     => 'center',
            'data-width'      => '80',
            'data-width-unit' => 'px',
        ],
        [
            'data-field'      => 'op_module',
            'data-title'      => lang('module'),
            'data-align'      => 'center',
            'data-halign'     => 'center',
            'data-width'      => '80',
            'data-width-unit' => 'px',
            'data-formatter'  => 'daicuo.admin.op.bakModule',
        ],
        [
            'data-field'      => 'op_controll',
            'data-title'      => lang('controll'),
            'data-align'      => 'center',
            'data-halign'     => 'center',
            'data-width'      => '80',
            'data-width-unit' => 'px',
            'data-formatter'  => 'daicuo.admin.op.bakControll',
        ],
        [
            'data-field'      => 'op_action',
            'data-title'      => lang('action'),
            'data-align'      => 'center',
            'data-halign'     => 'center',
            'data-width'      => '80',
            'data-width-unit' => 'px',
            'data-formatter'  => 'daicuo.admin.op.bakAction',
        ],
        [
            'data-field'      => 'operate',
            'data-title'      => lang('operate'),
            'data-align'      => 'center',
            'data-halign'     => 'center',
            'data-escape'     => false,
            'data-width'      => '120',
            'data-width-unit' => 'px',
            'data-formatter'  => 'daicuo.table.operate',
        ]
    ]
])}
</form>
{/block}