{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("video_index")}－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("video_index")}
</h6>
{:DcBuildForm([
    'name'      => 'video_index',
    'class'     => 'py-2',
    'action'    => DcUrl('admin/video/update', '', ''),
    'method'    => 'post',
    'submit'    => lang('submit'),
    'reset'     => lang('reset'),
    'close'     => false,
    'disabled'  => false,
    'ajax'      => true,
    'callback'  => '',
    'items'     => DcFormItems($items),
])}
{/block}