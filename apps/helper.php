<?php
/**************************************************采集内核***************************************************/
/**
 * 采集远程数据
 * @version 1.1.0 首次引入
 * @param string $useragent 必需;模拟用户HEAD头,可选值有（auto|windows|linux|ios|iphone）;默认：auto
 * @param int $timeout 必需;超时时间;默认：10
 * @param string $url 必需;待抓取的远程地址;默认：空
 * @param array $post_data 可选;post请求时发送的数据，留空则为get请求;默认：空
 * @param string $referer 可选;模拟来湃URL地址;默认：空
 * @param array $headers 可选;自定义请求头;默认：空
 * @param string $cookie 可选;模拟cookie信息;默认：空
 * @param string $proxy 可选;代理请求信息;默认：空
 * @return string $string 返回读取远程网页的内容
 */
function DcCurl($useragent='auto', $timeout=10, $url, $post_data='', $referer='', $headers=[], $cookie='', $proxy=''){
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_HEADER, 0);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt ($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);//301 302
    curl_setopt ($ch, CURLOPT_ENCODING, "");//乱码是因为返回的数据被压缩过了，在curl中加上一项参数即可
    //useragent
    if($useragent == 'windows'){
        curl_setopt ($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows;U;WindowsNT6.1;en-us)AppleWebKit/534.50(KHTML,likeGecko)Version/5.1Safari/534.50');
    }elseif($useragent == 'linux'){
        curl_setopt ($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:20.0) Gecko/20100101 Firefox/20.0');
    }elseif($useragent == 'ios'){
        curl_setopt ($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh;U;IntelMacOSX10_6_8;en-us)AppleWebKit/534.50(KHTML,likeGecko)Version/5.1Safari/534.50');
    }elseif($useragent == 'iphone'){
        curl_setopt ($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_2 like Mac OS X; zh-CN) AppleWebKit/537.51.1 (KHTML, like Gecko) Mobile/14F89 UCBrowser/10.9.17.807 Mobile');
    }elseif($useragent == 'android'){
        curl_setopt ($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; U; Android 7.1.1; zh-cn; OPPO R11st Build/NMF26X) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.134 Mobile Safari/537.36 OppoBrowser/4.6.5.3');
    }    
    //是否post
    if(is_array($post_data)){
        curl_setopt($ch, CURLOPT_POST, 1);// post数据
        if($headers[0] == 'Content-Type: application/json'){
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));    // post的变量
        }else{
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);    // post的变量
        }
    }
    //是否伪造来路
    if($referer){
        curl_setopt ($ch, CURLOPT_REFERER, $referer);
    }
    //是否headers
    if(is_array($headers)){
        //$headers = array('X-FORWARDED-FOR:28.58.88.10','CLIENT-IP:225.28.58.32');//构造IP
        curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers);
    }    
    //是否cookie
    if($cookie){
        curl_setopt ($ch, CURLOPT_COOKIE, $cookie);
    }
    //IP代理
    if($proxy){
        curl_setopt ($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt ($ch, CURLOPT_PROXYPORT, "80");
        //curl_setopt ($ch, CURLOPT_PROXYAUTH, CURLAUTH_BASIC); //代理认证模式
        //curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP); //使用http代理模式
        //curl_setopt ($ch, CURLOPT_PROXYUSERPWD,'testuser:pass');
    }    
    //https自动处理
    $http = parse_url($url);
    if($http['scheme'] == 'https'){
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    }
    $content = curl_exec($ch);
    curl_close($ch);
    //
    if($content){
        return $content;
    }
    return false;
}
/**
 * 根据正则规则快捷提取内容
 * @version 1.0.0 首次引入
 * @param string $rule 必需;正则规则
 * @param string $html 必需;待提取的字符串
 * @return string $string 匹配后的字符串
 */
function DcPregMatch($rule,$html){
    $arr = explode('$$$',$rule);
    if(count($arr) == 2){
      preg_match('/'.$arr[1].'/', $html, $data);
        return $data[$arr[0]].'';
    }else{
      preg_match('/'.$rule.'/', $html, $data);
        return $data[1].'';
    }
}

/**************************************************字符串、Array、Xml、Json、Serialize***************************************************/
/**
 * 字符串DES加密
 * @version 1.6.0 首次引入
 * @param string $str 必需;待加密的字符串
 * @param string $secert 必需;加密密钥
 * @return string $string 过滤后的字符串
*/
function DcDesEncode($str, $secert='daicuo'){
    return openssl_encrypt($str, 'DES-ECB', trim($secert), 0);
}
/**
 * 字符串DESDES解密
 * @version 1.6.0 首次引入
 * @param string $str 必需;待解密的字符串
 * @param string $secert 必需;加密密钥
 * @return string $string 解密后的字符串
 */
function DcDesDecode($str, $secert='daicuo'){
    return openssl_decrypt($str, 'DES-ECB', trim($secert), 0);
}
/**
 * 字符串转拼音
 * @version 1.6.0 首次引入
 * @param string $string 必需;待转化的字符;默认：空
 * @return string $string 转换后的拼音
 */
function DcPinYin($string){
    return \daicuo\Pinyin::get(trim($string));
}
/**
 * 字符串安全输出去除xss漏洞
 * @version 1.6.0 首次引入
 * @param string $string 必需;待过滤的字符串
 * @return string $string 过滤后的字符串
 */
function DcRemoveXss($string){
    return remove_xss($string);
}
/**
 * 字符串安全输出过滤目录名称不让跳转到上级目录
 * @version 1.6.0 优化过滤..与.
 * @version 1.5.0 首次引入
 * @param string $string 必需;待过滤的字符串
 * @return string $string 过滤后的字符串
 */
function DcDirPath($string){
    //if(!preg_match(“/^\w+$/”,$string)) exit(‘err!!!');//[A-Za-z0-9_]
    return str_replace(['..','.'], '', trim($string));
}
/**
 * 字符串安全输出去除Html标签
 * @version 1.5.0 首次引入
 * @param string $string 必需;待过滤的字符串
 * @param string $allow 可选;需保留的标签;默认：<p>
 * @return string $string 过滤后的字符串
 */
function DcStrip($string, $allow='<p>'){
    $string = preg_replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '', $string);
    $string = strip_tags(htmlspecialchars_decode(trim($string)), $allow);
    return stripslashes($string);
}
/**
 * 字符串安全输出转义HTML实体
 * @version 1.1.0 首次引入
 * @param string $string 必需;待过滤的字符串
 * @return string $string 过滤后的字符串
 */
function DcHtml($string){
    return htmlspecialchars(trim($string), ENT_QUOTES);
}
/**
 * %分隔的错误输出
 * @version 1.4.0 首次引入
 * @param string $value 必需;待分割的字符串
 * @return string $string 截取后的字符串
 */
function DcError($value){
    $value_array = explode('%', $value);
    if(count($value_array) > 1){
        return $value_array[1];
    }
    return $value;
}
/**
 * BOOL快捷输出
 * @version 1.3.0 首次引入
 * @param string $value 必需;待验证的字符串
 * @param bool $default 可选;验证规则;默认：true
 * @return string $string true|false
 */
function DcBool($value, $default=true){
    $array = ['1', 'true', 'on', 'yes'];
    if(in_array(strtolower($value), $array)){
        return $default;
    }
    return false;
}
/**
 * OnOff快捷输出
 * @version 1.3.0 首次引入
 * @param string $value 必需;待验证的字符串
 * @return string $string on|off
 */
function DcSwitch($value){
    if( DcBool($value) ){
        return 'on';
    }
    return 'off';
}
/**
 * 检测变量是否定义并默认输出
 * @version 1.6.0 首次引入
 * @param string $value 必需;待验证的字符串
 * @param mixed $default 必需;默认值;默认：空
 * @return string $string 验证后的字符串
 */
function DcIsset($value, $default=''){
    return isset($value) ? $value : $default;
}
/**
 * 字符串作比较是否相同后输出不同值
 * @version 1.1.0 首次引入
 * @param string $value 必需;待验证的字符串
 * @param string $default 必需;待比较的字符串;默认：空
 * @param string $stringTrue 必需;比较结果为真时输出的字符;默认：active
 * @param string $empty 必需;比较结果为假时输出的字符;默认：空
 * @return string $string 验证后的字符串
 */
function DcDefault($value, $default, $stringTrue='active', $stringFalse=''){
  if($value == $default){
    return $stringTrue;
  }
  return $stringFalse;
}
/**
 * 字符串空值快捷输出
 * @version 1.1.0 首次引入
 * @param string $value 必需;待验证的字符串
 * @param mixed $default 必需;默认值;默认：空
 * @return string $string 验证后的字符串
 */
function DcEmpty($value, $default=''){
    return !empty($value) ? $value : $default;
}
/**
 * 字符串截取
 * @version 1.0.0 首次引入
 * @param string $str 必需;待截取的字符串
 * @param int $start 必需;起始位置;默认：0
 * @param int $length 必需;截取长度;默认：空
 * @param bool $suffix 可选;超出长度是否以...显示;默认：true
 * @param string $charset 可选;字符编码;默认：utf-8
 * @return string $string 截取后的字符串
 */
function DcSubstr($str, $start=0, $length, $suffix=true, $charset="UTF-8"){
    $str = trim($str);
    if( function_exists('mb_strimwidth') ){
        if($suffix){
            return mb_strimwidth($str, $start, $length*2, '...', $charset);
        }
        return mb_strimwidth($str, $start, $length*2, '', $charset);
    }
    return @substr($str, $start, $length);
}
/**
 * XML转数组
 * @version 1.5.0 首次引入
 * @param string $xml 必需;待验证的字符串
 * @param bool $isnormal 可选;是否转义;默认：false
 * @return array $array 转换后的数组
 */
function DcXmlUnSerialize(&$xml, $isnormal = false) {
    $xml = new \net\Xml($isnormal);
    $data = $xml->parse($xml);
    return $data;
}
/**
 * 数组转XML
 * @version 1.5.0 首次引入
 * @param array $xml 必需;待转换的数组
 * @param bool $htmlon 可选;是否支持HTML标签;默认：false
 * @param bool $isnormal 可选;是否转义;默认：false
 * @param int $level 可选;待验证的字符串;默认：1
 * @return string $string 转换后的XML代码
 */
function DcXmlSerialize($arr, $htmlon = false, $isnormal = false, $level = 1) {
    $s = $level == 1 ? "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n<root>\r\n" : '';
    $space = str_repeat("\t", $level);
    foreach($arr as $k => $v) {
        if(!is_array($v)) {
            $s .= $space."<item id=\"$k\">".($htmlon ? '<![CDATA[' : '').$v.($htmlon ? ']]>' : '')."</item>\r\n";
        } else {
            $s .= $space."<item id=\"$k\">\r\n".DcXmlSerialize($v, $htmlon, $isnormal, $level + 1).$space."</item>\r\n";
        }
    }
    $s = preg_replace("/([\x01-\x08\x0b-\x0c\x0e-\x1f])+/", ' ', $s);
    return $level == 1 ? $s."</root>" : $s;
}
/**
 * Json数据序列化为字符串
 * @version 1.4.0 首次引入
 * @params array $json 必需;json数据源
 * @return string $string 序列化后的字符串
 */
function DcJsonToSerialize($json){
    if($json_array = json_decode($json, true)){
        return serialize($json_array);
    }
    return $json;
}
/**
 * 将序列化字符串转化为json
 * @version 1.4.0 首次引入
 * @params array $string 必需;序列化后的数据
 * @return string $json json格式的字符串
 */
function DcSerializeToJson($string){
    $array = unserialize($string);
    if(is_array($array)){
        return json_encode($array);
    }
    return $string;
}
/**
 * 字符串转数组
 * @version 1.1.0 首次引入
 * @param string $value 必需;待验证的字符串;默认:空
 * @return array $array 索引数组
 */
function DcStrToArray($string=''){
    if(is_string($string)){
        return [$string];
    }
    return $string;
}
/**
 * 过滤数组中空值的字段
 * @version 1.6.0 首次引入
 * @param array $array 必需;待过滤的数组;默认：空
 * @return array $array 过滤后的数组
 */
function DcArrayEmpty($array){
    return array_filter($array, function($value){
        if($value || $value=='0'){
            return true;
        }
        return false;
    });
}
/**
 * 过滤数组中不需要的字段
 * @version 1.6.0 首次引入
 * @param array $array 必需;待过滤的数组
 * @param array $fileds 必需;需保留的字段
 * @param string $prefix 可选;KEY前缀
 * @param string $suffix 可选;KEY后缀
 * @return mixed $mixed array|value 过滤后的数组
 */
function DcArrayFilter($array=[], $fileds=[], $prefix='', $suffix=''){
    $filter = [];
    foreach($array as $key=>$value){
        if(in_array($key, $fileds)){
            $filter[$prefix.$key.$suffix] = $value;
        }
    }
    return $filter;
}
/**
 * 过滤数组中没有定义的字段
 * @version 1.6.0 首次引入
 * @param array $array 必需;待过滤的数组;默认：空
 * @param array $fileds 必需;需保留的字段;默认：空
 * @return mixed $mixed array|value 过滤后的数组
 */
function DcArrayIsset($array=[], $fileds=[]){
    foreach($array as $key=>$value){
        if(!in_array($key, $fileds)){
            unset($array[$key]);
        }
    }
    foreach($fileds as $key=>$value){
        if(!isset($array[$key])){
            unset($array[$key]);
        }
    }
    return $array;
}
/**
 * 给一个数组参数或者url字符串（args）绑定默认值
 * @version 1.5.26 首次引入
 * @param array $args 必需;数组参数列表
 * @param array $defaults 可选;数组默认值;默认：空
 * @return array $array 合并后的数组
 */
function DcArrayArgs($args, $defaults = ''){
	if ( is_array( $args ) ){
		$r =& $args;
    }else{
		parse_str( $args, $r );
    }
	if ( is_array( $defaults ) ){
		return array_merge( $defaults, $r );
    }
	return $r;
}
/**
 * 将数组序列化为字符串
 * @version 1.2.0 首次引入
 * @params array $array 必需;序列化后的数据
 * @return string $json json格式的字符串
 */
function DcArraySerialize($array){
    if(is_array($array)){
        return serialize($array);
    }
    return $array;
}
/**
 * 二维数组根据字段进行排序
 * @version 1.2.0 首次引入
 * @params array $array 必需;需要排序的数组
 * @params string $field 必须;排序的字段名;默认：空
 * @params string $sort 可选;排序顺序标志，SORT_DESC＝降序，SORT_ASC＝升序;默认：SORT_DESC
 * @return array $array 排序后的数组
 */
function DcArraySequence($array, $field, $sort = 'SORT_DESC'){
    $arrSort = array();
    foreach ($array as $uniqid => $row) {
        foreach ($row as $key => $value) {
            $arrSort[$key][$uniqid] = $value;
        }
    }
    array_multisort($arrSort[$field], constant($sort), $array);
    return $array;
}
/**
 * 在数据列表中搜索
 * @version 1.2.0 首次引入
 * @param array $list 数据列表
 * @param mixed $condition 查询条件,支持array('name'=>$value)或者name=$value
 * @param string $key 要返回的字段值
 * @return mixed $mixed array|value
 */
function DcArraySearch($array, $condition, $key=''){
    $array_search = list_search($array, $condition);
    if($key){
        return $array_search[0][$key];
    }
    return $array_search;
}
/**
 * 在指定的键之前插入元素
 * @version 1.6.0 首次引入
 * @param array $array 必需;原数组;默认：空
 * @param array $data 必需;要插入的值;默认：空
 * @param string $key 可选;键名值;默认：空
 * @return array $array 合并后的值
 */
function DcArrayPush($array, $data=null, $key=false){
    $data    = (array)$data;
    $offset  = ($key===false)?false:array_search($key, array_keys($array));
    $offset  = ($offset)?$offset:false;
    if($offset){
        return array_merge(
            array_slice($array, 0, $offset), 
            $data, 
            array_slice($array, $offset)
        );
    }else{     // 没指定 $key 或者找不到，就直接加到末尾
        return array_merge($array, $data);
    }
}
/**
 * 将数据集转化为数组
 * @version 1.6.0 首次引入
 * @param mixed $data 必需;需验请的数据集;默认：空
 * @return mixed $mixed array|null
 */
function DcArrayResult($data=[]){
    if(is_null($data)){
        return null;
    }
    if(is_object($data)){
        return $data->toArray();
    }
    return $data;
}
/**
 * 判断是否为（普通/多维）数组
 * @version 1.4.0 首次引入
 * @param array $array 必需;待验证的数组;默认：空
 * @param bool $mode 可选;多维数组模式;默认：false
 * @return bool true|false 真或假
 */
function DcIsArray($array, $mode=false){
    //验证普通数组模式
    if($mode == false){
        return is_array($array);
    }
    //验证多维数组模式
    if(count($array, 1) == 1){
        return false;
    }
    //计算一维与多维是否相同，统计相同则是一维，不相同则是多维
    if (count($array) == count($array, 1)) {
        return false;
    } else {
        return true;
    }
}

/**************************************************ThinkPhp语言***************************************************/
/**
 * 加载语言定义(不区分大小写)
 * @version 1.0.0 首次引入
 * @param string $file 必需;语言文件
 * @param string $range 可选;语言作用域;默认：空
 * @return mixed $mixed 语言配置信息
 */
function DcLoadLang($file, $range = ''){
    return \think\Lang::load($file, $range);
}

/**************************************************ThinkPhp验证器***************************************************/
/**
* 验证器独立验证
* @version 1.2.0 首次引入
* @param array $data 必需;待验证数据
* @param string $name 必需;验证器名称;默认：空
* @param string $scene 可选;验证场景,多个用逗号分隔;默认：空
* @param string $layer 可选;业务层名称;默认：validate
* @return bool $bool 验证失败时可使用cofig('daicuo.error')获取出错信息
*/
function DcCheck($data = [], $name, $scene='', $layer='validate'){
    $validate = validate($name, $layer);
    if(!$validate->scene($scene)->check($data)){
        config('daicuo.error', $validate->getError());
        return false;
    }
    return true;
}

/**************************************************ThinkPhp缓存***************************************************/
/**
 * 缓存标识处理函数、支持清空缓存(value=false)
 * @version 1.0.0 首次引入
 * @param string $key 必需;缓存KEY
 * @param string|array $value 必需;缓存数据（空|clear|false|data值）;默认：空
 * @param intval $time 可选;缓存时间;默认：0
 * @return mixed $mixed 缓存处理结果
 */
function DcCache($key, $value='', $time=0){
    if(!$key){
        return false;
    }
    if(is_null($value)){
        return \think\Cache::rm($key);//DcCache('key',null);
    }
    if($value === false || $value == 'clear'){
        return \think\Cache::clear();
    }
    if($value){
        return \think\Cache::set($key, $value, $time);
    }
    return \think\Cache::get($key);
}
/**
 * 缓存标签处理函数、支持清空缓存(value=false)
 * @version 1.0.0 首次引入
 * @param string $tag 必需;缓存标签名
 * @param string $key 必需;缓存标识KEY;默认：空
 * @param string|array $value 必需;缓存数据（空|clear|false|data值）;默认：空
 * @param intval $time 可选;缓存时间;默认：0
 * @return mixed $mixed 缓存结果
 */
function DcCacheTag($tag='', $key='', $value='', $time=0){
    if(!$tag){
        return false;
    }
    if(!$key || $key == 'clear' || $value === false || $value == 'clear'){
        return \think\Cache::clear($tag);//DcCacheTag('tag','clear','clear');
    }
    if($key && $value){
        return \think\Cache::tag($tag)->set($key, $value, $time);//DcCacheTag('tag','key','value',60);
    }
    return \think\Cache::get($key);
}
/**
 * 数据库结果处理后触发删除缓存
 * @version 1.0.0 首次引入
 * @param int|obj $result 必需;结果记录数或者数据集
 * @param string $cacheSign 可选;缓存标记（标识名或者标签名）;默认：空
 * @param string $delType 可选;缓存删除方式(key|tag);默认：key
 * @return mixed $mixed obj|array数据库查询对象
 */
function DcCacheResult($result, $cacheSign, $delType='key'){
    if($result && $cacheSign){
        if($delType == 'tag'){
            DcCacheTag($cacheSign, NULL, false);
        }else{
            DcCache($cacheSign, NULL);
        }
    }
    return $result;
}
/**
 * 生成缓存KEY名
 * @version 1.2.0 首次引入
 * @param string|array $value 必需;缓存名格式化
 * @return string $string 缓存KEY
 */
function DcCacheKey($value){
    if(is_array($value)){
        return md5(serialize($value));
    }
    return md5($value);
}

/**************************************************ThinkPhp路径***************************************************/
/**
 * 获取系统根目录
 * @version 1.0.0 首次引入
 * @return string $string 框架根目录路径
 */
function DcRoot(){
    $base = \think\Request::instance()->root();
    return ltrim(dirname($base), DS).'/';
}
/**
 * 提取网址的域名
 * @version 1.0.0 首次引入
 * @return string $string 框架根目录路径
 */
function DcDomain($url=''){
    $url = parse_url($url);
    return $url['host'];
}
/**
 * 生成站内链接
 * @version 1.1.0 首次引入
 * @param string $url 必需;调用地址
 * @param string|array $vars 可选;调用参数，支持字符串和数组;默认：空
 * @param bool $suffix 可选;是否添加类名后缀;默认：true
 * @return string $string 站内链接
 */
function DcUrl($url = '', $vars = '', $suffix = true){
    if(config('common.app_domain') == 'on'){
        return strip_tags(url($url, $vars, $suffix, true));
    }else{
        return strip_tags(url($url, $vars, $suffix, false));
    }
}
/**
 * 后台生成前台路径
 * @version 1.1.0 首次引入
 * @param string $url 必需;调用地址
 * @param string|array $vars 可选;调用参数 支持字符串和数组;默认：空
 * @param bool $suffix 可选;是否添加类名后缀;默认：true
 * @return string $string 后台专用生成前台链接
 */
function DcUrlAdmin($url = '', $vars = '', $suffix = true){
	$baseFile = request()->baseFile();
	return str_replace($baseFile, '', DcUrl($url, $vars, $suffix));
}
/**
 * 后台插件管理路径
 * @version 1.2.0 首次引入
 * @param array $vars 必需;地址栏参数
 * @param bool $suffix 可选;是否添加类名后缀;默认：true
 * @return string $string 后台插件访问地址
 */
function DcUrlAddon($vars = '', $suffix = true){
    return DcUrl('addon/index', $vars, '');
    //return '../addon/index?'.http_build_query($vars);
}
/**
 * 附件读取路径
 * @version 1.3.0 首次引入
 * @param string $file 必需;文件保存的路径
 * @param int $key 可选;key值、第几张;默认：0
 * @return string $string 附件访问地址
 */
function DcUrlAttachment($file, $key=0){
    //必要参数
    if(!$file){
        return '';
    }
    //多图分割
    $file = explode(';',$file);
    //当前第几个
    $file = $file[$key];
    //判断本地附件还是远程附件
    $array = parse_url($file);
    //远程附件处理
	if(in_array($array['scheme'], array('http','https','ftp'))){
        //第三方防盗链附盗链开关
		if( config('common.upload_referer') ){
			return config('common.upload_referer').urlencode($file);
		}
        //直接返回绝对地址
		return $file;
	}
    //本地附件URL接口开关
    if(config('common.upload_host')){
        return config('common.upload_host').urlencode($file);
    }
    //本地附件CDN加速开关
    if(config('common.upload_cdn')){
        return trim(config('common.upload_cdn')).DcRoot().trim(config('common.upload_path')).'/'.$file;
    }
    //相对路径直接返回真实路径
    return DcRoot().trim(config('common.upload_path')).'/'.$file;
}

/****************************************************ThinkPhp配置***************************************************/
/**
* 获取系统配置.支持多级层次
* @version 1.2.0 首次引入
* @param string $name 必需;配置名称;默认：空
* @return mixed $mixed 获取到的配置值
*/
function DcConfig($name=''){
    if(!$name){
        return false;
    }
    $data = config();
    foreach (explode('.', $name) as $key => $val) {
        if (isset($data[$val])) {
            $data = $data[$val];
        } else {
            $data = null;
            break;
        }
    }
    return $data;
}
/**
 * 加载配置文件（PHP格式）
 * @version 1.0.0 首次引入
 * @param string $file 必需;配置文件名;默认：空
 * @param string $name 可选;配置名（如设置即表示二级配置）;默认：空
 * @param string $range 可选;作用域;默认：空
 * @return mixed $mixed 配置信息
 */
function DcConfigLoad($file, $name = '', $range = ''){
    return \think\Config::load($file, $name, $range);
}
/**
* 合并配置信息
* @version 1.4.0 首次引入
* @param string $config_name 必需;配置名称;默认空
* @param string $config_value 必需;新配置值;默认空
* @return mixed $mixed 合并后的配置值
*/
function DcConfigMerge($config_name='', $config_value=''){
    if( !is_array($config_value) ){
        return false;
    }
    //旧配置
    $config_name = trim($config_name);
    $config_value_old = config($config_name);
    if( is_string($config_value_old) ){
        $config_value_old = [$config_value_old];
    }elseif( is_null($config_value_old) ){
        $config_value_old = [];
    }
    if( !is_array($config_value_old) ){
        return false;
    }
    return config($config_name, array_merge($config_value_old, $config_value));
}
/**
 * 批量更新与新增动态配置、常用于需自动加载的配置
 * @version 1.6.0 首次引入
 * @param array $formData 必需;表单数据(key=>value)成对形式;默认：空
 * @param string $module 必需;模块名称;默认：common
 * @param string $controll 可选;控制器名;默认：NULL
 * @param string $action 可选;操作名;默认：NULL
 * @param int $order 可选;排序值;默认：0
 * @param string $autoload 可选;自动加载;默认：yes
 * @param string $status 可选;状态;默认：normal
 * @return array $array 数据集
 */
function DcConfigWrite($formData=[], $module='common', $controll=NULL, $action=NULL, $order=0, $autoload='yes', $status='normal'){
    return \daicuo\Op::write($formData, $module, $controll, $action, $order, $autoload, $status);
}
/**
 * 添加动态配置至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;普通表单数组形式;默认：空
 * @return mixed $mixed 自增ID
 */
function DcConfigSave($data=[]){
    //数据过滤
    $data = DcArrayIsset($data, ['op_name','op_value','op_module','op_controll','op_action','op_order','op_status','op_autoload']);
    //合并初始参数
    $data = DcArrayArgs($data,[
        'op_module'    => 'common',
        'op_controll'  => 'common',
        'op_action'    => 'common',
        'op_order'     => 0,
        'op_autoload'  => 'yes',
        'op_status'    => 'normal',
    ]);
    return \daicuo\Op::save($array);
}
/**
 * 批量添加动态配置至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;二维数组;默认：空
 * @return obj $obj 多条数据集
 */
function DcConfigSaveAll($data=[]){
    foreach($data as $key=>$value){
        if(!$value['op_name']){
            unset($data[$key]);
            continue;
        }
    }
    return \daicuo\Op::save_all($data);
}
/**
 * 按条件删除一条动态配置
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcConfigDelete($args=[]){

    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }

    return \daicuo\Op::delete($where);
}
/**
 * 按ID条件删除一条动态配置
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;Id值/多个用逗号分隔或者数组(string|array)；默认：空
 * @return mixed $mixed 删除结果(obj|null)
 */
function DcConfigDeleteId($value=''){
    return \daicuo\Op::delete_id($value);
}
/**
 * 按条件删除多条动态配置
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcConfigDeleteAll($args=[]){
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }
    return \daicuo\Op::delete_all($where);
}
/**
 * 按条件删除一条数组配置指定的KEY
 * @version 1.6.0 首次引入
 * @param array $where 查询条件（一维数组）
 * @param string $opValueKey 数组KEY值
 * @return mixed $mixed 操作结果(array|false)
 */
function DcConfigDeleteKey($where=[], $opValueKey=''){
    if($where && $opValueKey){
        return \daicuo\Op::delete_value_key($where, $opValueKey);
    }
    return false;
}
/**
 * 按条件修改配置列表
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @param array $data 必需;待修改的数据（普通表单数组形式）;默认：空
 * @return mixed $mixed 查询结果（array|null）
 */
function DcConfigUpdate($args=[], $data=[]){
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }
    return \daicuo\Op::update($where, $data);
}
/**
 * 按Id修改一个配置
 * @param int $opId 必需;ID值;默认：空
 * @param array $data 必需;表单数据（一维数组）;默认：空 
 * @return mixed $mixed obj|null;不为空时返回obj
 */
function DcConfigUpdateId($opId, $data){
    return \daicuo\Op::update_id($opId, $data);
}
/**
 * 按条件更新一条数组配置指定的KEY
 * @version 1.6.0 首次引入
 * @param array $where 查询条件（一维数组）
 * @param string $opValueKey 数组KEY值
 * @param string $opValueData 数组Value
 * @return mixed $mixed 操作结果(array|false)
 */
function DcConfigUpdateKey($where=[], $opValueKey='', $opValueData=''){
    return \daicuo\Op::update_value_key($where, $opValueKey, $opValueData);
}
/**
 * 按条件更新一条数组配置的多个数组值
 * @version 1.6.0 首次引入
 * @param array $where 必需;查询条件（一维数组）;默认：空
 * @param array $opValueData 必需;key=>Value类型的数据;默认：空
 * @return mixed $mixed 更新结果（obj|false）
 */
function DcConfigUpdateArray($where=[], $opValueData=''){
    return \daicuo\Op::update_value_array($where, $opValueData);
}
/**
 * 按条件获取一个配置列表
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcConfigFind($args=[]){
    $cache = DcBool($args['cache'],true);
    
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }

    return \daicuo\Op::get($where, $cache);
}
/**
 * 按条件获取多个配置列表
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type string $field 可选;查询字段;默认：*
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed 查询结果（obj|null）
 */
function DcConfigSelect($args=[]){
    $defaults = array();
    if(!$args['where']){
        $defaults['where'] = DcWhereFilter($args, ['module','controll','action','status'], 'eq', 'op_');
    }
    if(!$args['paginate']){
        $defaults['paginate'] = DcPageFilter($args);
    }
    return \daicuo\Op::all( DcArrayArgs($args, $defaults) );
}

/**************************************************ThinkPhp钩子***************************************************/
/**
* 动态添加行为扩展到某个标签
* @version 1.2.0 首次引入
* @param string $tag 必需;标签名称
* @param mixed $behavior 必需;行为名称
* @param bool $first 可选;是否放到开头执行;默认：false
* @return void $void 不返回值
*/
function DcHookAdd($tag, $behavior, $first = false){
    \think\Hook::add($tag, $behavior, $first);
}
/**
 * 监听标签的行为
 * @version 1.2.0 首次引入
 * @param string $tag 必需;标签名称
 * @param mixed $params 可选;传入参数;默认：null
 * @param mixed $extra 可选;额外参数;默认：null
 * @param bool $once 可选;只获取一个有效返回值;默认：false
 * @return void $void 不返回值
 */
function DcHookListen($tag, &$params = null, $extra = null, $once = false){
    \think\Hook::listen($tag, $params, $extra, $once);
}
/**
 * 执行某个标签行为
 * @version 1.2.0 首次引入
 * @param mixed $class 必需;要执行的行为
 * @param string $tag 可选;方法名（标签名）;默认：空
 * @param mixed $params 可选;传入的参数;默认：null
 * @param mixed $extra 可选;额外参数;默认：null
 * @return void $void 不返回值
 */
function DcHookExec($class, $tag = '', &$params = null, $extra = null){
    \think\Hook::exec($class, $tag, $params, $extra);
}
/**
 * 添加一个动态钩子至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;普通表单数组形式;默认：空
 * @return mixed $mixed 自增ID
 */
function DcHookSave($data=[]){
    return \daicuo\Hook::save($data);
}
/**
 * 批量添加动态钩子至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;二维数组;默认：空
 * @return obj $obj 多条数据集
 */
function DcHookSaveAll($data=[]){
    return \daicuo\Hook::save_all($data);
}
/**
 * 按条件删除一条动态钩子
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcHookDelete($args=[]){

    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id'], 'eq', 'op_');
    }

    return \daicuo\Hook::delete($where);
}
/**
 * 按ID条件删除一条动态钩子
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;Id值/多个用逗号分隔或者数组(string|array)；默认：空
 * @return mixed $mixed 删除结果(obj|null)
 */
function DcHookDeleteId($value=''){
    return \daicuo\Hook::delete_id($value);
}
/**
 * 按条件修改一条动态钩子
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称(site_hook);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @param array $data 必需;待修改的数据（普通表单数组形式）;默认：空
 * @return mixed $mixed 查询结果（array|null）
 */
function DcHookUpdate($args=[], $data=[]){
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }
    return \daicuo\Hook::update($where, $data);
}
/**
 * 按Id修改一个动态钩子
 * @param int $opId 必需;ID值;默认：空
 * @param array $data 必需;表单数据（一维数组）;默认：空 
 * @return mixed $mixed obj|null;不为空时返回obj
 */
function DcHookUpdateId($opId, $data){
    return \daicuo\Hook::update_id($opId, $data);
}
/**
 * 按条件获取一个动态钩子
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称(site_hook);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcHookFind($args=[]){
    $cache = DcBool($args['cache'], true);
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }
    return \daicuo\Hook::get($where, $cache);
}
/**
 * 按条件获取多个动态钩子
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type string $field 可选;查询字段;默认：*
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed 查询结果（obj|null）
 */
function DcHookSelect($args=[]){
    $defaults = array();
    if(!$args['where']){
        $defaults['where'] = DcWhereFilter($args, ['module','controll','action','status'], 'eq', 'op_');
    }
    if(!$args['paginate']){
        $defaults['paginate'] = DcPageFilter($args);
    }
    return \daicuo\Hook::all( DcArrayArgs($args, $defaults) );
}

/**************************************************ThinkPhp路由***************************************************/
/**
 * 添加一个路由至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;普通表单数组形式;默认：空
 * @return mixed $mixed 自增ID
 */
function DcRouteSave($data=[]){
    return \daicuo\Route::save($data);
}
/**
 * 批量添加路由至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;二维数组;默认：空
 * @return obj $obj 多条数据集
 */
function DcRouteSaveAll($data=[]){
    return \daicuo\Route::save_all($data);
}
/**
 * 按条件删除一条路由
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcRouteDelete($args=[]){

    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id'], 'eq', 'op_');
    }

    return \daicuo\Route::delete($where);
}
/**
 * 按ID条件删除一条动态配置
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;Id值/多个用逗号分隔或者数组(string|array)；默认：空
 * @return mixed $mixed 删除结果(obj|null)
 */
function DcRouteDeleteId($value=''){
    return \daicuo\Route::delete_id($value);
}
/**
 * 按条件修改一条路由
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称(site_rotue);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @param array $data 必需;待修改的数据（普通表单数组形式）;默认：空
 * @return mixed $mixed 查询结果（array|null）
 */
function DcRouteUpdate($args=[], $data=[]){
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }
    return \daicuo\Route::update($where, $data);
}
/**
 * 按Id修改一条路由
 * @param int $opId 必需;ID值;默认：空
 * @param array $data 必需;表单数据（一维数组）;默认：空 
 * @return mixed $mixed obj|null;不为空时返回obj
 */
function DcRouteUpdateId($opId, $data){
    return \daicuo\Route::update_id($opId, $data);
}
/**
 * 按条件获取一条路由
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称(site_rotue);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcRouteFind($args=[]){
    $cache = DcBool($args['cache'], true);
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }
    return \daicuo\Route::get($where, $cache);
}
/**
 * 按条件获取多个路由
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type string $field 可选;查询字段;默认：*
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed 查询结果（obj|null）
 */
function DcRouteSelect($args=[]){
    $defaults = array();
    if(!$args['where']){
        $defaults['where'] = DcWhereFilter($args, ['module','controll','action','status'], 'eq', 'op_');
    }
    if(!$args['paginate']){
        $defaults['paginate'] = DcPageFilter($args);
    }
    return \daicuo\Route::all( DcArrayArgs($args, $defaults) );
}

/**************************************************ThinkPhp模板***************************************************/
/**
 * 获取模板主题存放目录路径
 * @version 1.1.0 首次引入
 * @param string $module 必需;模块名称
 * @param bool $isMobile 必需;是否移动端
 * @return string $string 模板主题路径
 */
function DcViewPath($module, $isMobile){
    return 'apps/'.$module.'/theme/'.DcTheme($module, $isMobile).'/';
}
/**
 * 获取模板主题目录名称
 * @version 1.1.0 首次引入
 * @param string $module 必需;模块名称;默认：index
 * @param bool $isMobile 必需;是否移动端;默认：false
 * @return string $string 模板主题名称
 */
function DcTheme($module='index', $isMobile=false){
    if($isMobile){
        return DcEmpty(config($module.'.theme_wap'),config('common.wap_theme'));
    }
    return DcEmpty(config($module.'.theme'),config('common.site_theme'));
}
/**
 * 获取插件应用的主题列表
 * @version 1.6.0 首次引入
 * @param string $module 必需;模块名称;默认：index
 * @return array $array 主题列表
 */
function DcThemeOption($module='index'){
    $themes = array();
    foreach( glob_basename('./apps/'.$module.'/theme/') as $value){
        $themes[$value] = $value;
    }
    return $themes;
}
/**
 * 生成模板调用配置标签
 * @version 1.4.0 首次引入
 * @param string $module 模块
 * @param string $field 字段
 * @return string $string 原生输出;
 */
function DcTplLabelOp($module='', $field=''){
    return DcHtml('{:config("'.$module.'.'.$field.'")}');
}

/**************************************************视频模块***************************************************/
/**
 * 调用播放器
 * @version 1.4.0 首次引入
 * @param array $options 播放器参数，数组格式
 * @return string $string html代码
 */
function DcPlayer($options=[]){
    $video = new \daicuo\Video();
    return $video->player($options);
}

/**************************************************编辑器模块***************************************************/
/**
 * 调用编辑器解析函数
 * @version 1.6.0 首次引入
 * @param string $content 内容
 * @return mixed $mixed 解析后的内容
 */
function DcEditor($content=''){
    if(config('common.editor_name') != 'textarea'){
        if( $function = config(config('common.editor_name').'.editor_function') ){
            return call_user_func_array($function, [$content]);
        }
    }
    return $content;
}
/**
 * 根据框架配置获取当前编辑器路径
 * @version 1.6.0 首次引入
 * @return string $string 后台设置的编辑器路径
 */
function DcEditorPath(){
    if(config('common.editor_name') != 'textarea'){
        if( $editorPath = config(config('common.editor_name').'.editor_path') ){
            return $editorPath;
        }
    }
    return config('form_view.editor');
}
/**
 * 获取已安装的编辑器列表
 * @version 1.6.0 首次引入
 * @return array $array 普通数组列表（键名为模块名）
 */
function DcEditorOption(){
    $option = [];
    foreach(config('common.editor_list') as $editor){
        $option[$editor] = lang($editor);
    }
    return $option;
}

/**************************************************表单模块***************************************************/
/**
 * 快速生成表单元素
 * @version 1.0.0 首次引入
 * @param array $form 必需;表单元素属性列表 {
 *     @type string $name 必需;form标签的name属性，可通过此参数搭配钩子处理系统预设的表单;默认：text
 *     @type string $class 可选;form标签的class属性;默认：row form-group
 *     @type string $action 可选;form标签的action属性;默认：空
 *     @type string $method 可选;form标签的method属性;默认：post
 *     @type bool $disabled 可选;form标签的disabled属性;默认：false
 *     @type bool $ajax 可选;是否采用AJAX模式提交表单;默认：true
 *     @type string $callback 可选;AJAX模式时提交后回调函数;默认：空
 *     @type string $submit 可选;提交按钮文字,留空不显示;默认：提交
 *     @type string $reset 可选;重置按钮文字,留空不显示;默认：重置
 *     @type string $close 可选;关闭按文字,留空不显示,主要用于ajax浮动窗口;默认：字段标题
 *     @type string $items 可选;表单元素列表,参考DcFormItems参数;默认：空
 * }
 * @return array $array 框架表单元素属性专用格式
 */
function DcBuildForm($args=[]){
    return widget('common/Form/build', ['args'=>$args]);
}
/**
 * 快速生成适用于框架的表单元素格式列表
 * @version 1.6.0 首次引入
 * @param string $field 必需;表单字段名；默认：空
 * @param array $items 必需;多维数组,键名为字段,键值参考DcFormItem参数列表;默认：空
 * @return array $array 框架表单元素属性专用格式
 */
function DcFormItems($items=[]){
    $list = [];
    foreach($items as $field=>$form){
        $list[] = DcFormItem($field, $form);
    }
    return $list;
}
/**
 * 快速生成一条适用于框架的表单元素格式
 * @version 1.6.0 首次引入
 * @param string $field 必需;表单字段名；默认：空
 * @param array $form 必需;表单元素属性列表 {
 *     @type string $type 必需;input类型(html|hidden|type|url|email|number|password|image|file|datetime|textarea|editor|json|custom|select|switch|radio|checkbox);默认：text
 *     @type string $html 可选;自定义html标签;默认：空
 *     @type string $hidden 可选;hidden属性;默认：空
 *     @type string $name 可选;name属性;默认：表单字段名
 *     @type string $id 可选;id属性;默认：表单字段名
 *     @type string $value 可选;value属性;默认：空
 *     @type string $title 可选;title属性;默认：字段标题
 *     @type string $placeholder 可选;placeholder属性;默认：字段描述
 *     @type string $tips 可选;表单元素提示;默认：空
 *     @type string $autofocus 可选;自动获取焦点属性;默认：空
 *     @type string $readonly 可选;readonly属性;默认：false
 *     @type string $disabled 可选;disabled属性;默认：false
 *     @type string $required 可选;required属性;默认：false
 *     @type string $option 可选;option属性，有效范围(select|suctom|switch|radio|checkbox);默认：空
 *     @type string $rows 可选;rows属性，有效范围(textarea|json|editor);默认：10
 *     @type string $class 可选;表单外层class属性;默认：row form-group
 *     @type string $class_left 可选;表单元素左侧class属性;默认：col-12 col-md-2
 *     @type string $class_right 可选;表单元素右侧class属性;默认：col-12 col-md-6
 *     @type string $class_right_controll 可选;表单元素右侧input标签class属性;默认：'form-control form-control-sm'
 *     @type string $class_right_label 可选;表单元素右侧label标签class属性，有效范围(switch|radio|checkbox);默认：'form-check-la'
 *     @type string $class_tips 可选;表单提示信息class属性;默认：'form-text text-muted small'
 * }
 * @return array $array 框架表单元素属性专用格式
 */
function DcFormItem($field='', $form=[]){
    //默认参数
    $default = [
        'type'                => 'text',
        'name'                => $field,
        'id'                  => $field,
        'value'               => '',
        'title'               => lang(str_replace('[]','',$field)),
        'placeholder'         => lang(str_replace('[]','',$field).'_placeholder'),
        'tips'                => '',
        'autofocus'           => '', 
        'readonly'            => false,
        'disabled'            => false,
        'required'            => false,
        'class'               => 'row form-group',
        'class_left'          => 'col-12 col-md-2',
        'class_right'         => 'col-12 col-md-6',
        'class_right_control' => '',
        'class_tips'          => 'col-12 col-md-4 form-text text-muted small',
    ];
    //表单类型特有属性
    if( in_array($form['type'],['text','url','email','number','password','image','file','datetime']) ){
        $default['autocomplete']     = 'off';
        $default['maxlength']        = '250';
    }elseif( in_array($form['type'],['textarea','editor','json']) ){
        $default['rows']             = 10;
    }elseif( in_array($form['type'],['custom','select']) ){
        $default['option']            = '';
    }elseif( in_array($form['type'],['switch','radio','checkbox']) ){
        $default['class_right_label'] = '';
        $default['option']            = '';
    }elseif( in_array($form['type'],['tags']) ){
        $default['class_tags']        = 'form-text pt-2';
        $default['class_tags_list']   = 'text-danger mr-2';
        $default['option']            = [];
    }
    //返回结果
    return DcArrayArgs($form, $default);
}

/**************************************************表格模块***************************************************/
/**
 * 生成表格数据
 * @version 1.1.0 首次引入
 * @param array $config 表格参数列表
 * @return string $string 返回渲染后的表单HTML代码
 */
function DcBuildTable($args=[]){
    return widget('common/Table/build', ['args'=>$args]);
}

/**************************************************分页模块***************************************************/
/**
 * 解析页码为HTML
 * @version 1.1.0 首次引入
 * @param number $index 当前页
 * @param number $rows 每页数量 
 * @param number $total 总记录数
 * @param string $path URL路径
 * @param array $query URL额为参数
 * @param string $fragment URL锚点 
 * @param string $varpage 分页变量
 * @return string $string HTML代码
 */
function DcPage($index=1, $rows=10, $total=1, $path='', $query=[], $fragment='', $varpage='pageNumber'){
    $page = new \page\Bootstrap('', $rows, $index, $total, false, [
        'var_page' => $varpage,
        'path'     => str_replace('%5BPAGE%5D','[PAGE]',$path),//url('index/index/index','','')
        'query'    => $query,
        'fragment' => $fragment,
    ]);
    return $page->render();
}
/**
 * 单页分页代码HTML
 * @version 1.1.0 首次引入
 * @param number $index 当前页
 * @param number $total 总页数 
 * @return string $string HTML代码
 */
function DcPageSimple($page=1, $total=1, $path=''){
    //统一变量
    $path = str_replace('%5BPAGE%5D','[PAGE]', $path);
    //上一页按钮
    if ($page <= 1) {
        $preButton = '<li class="page-item disabled"><a class="page-link" href="javascript:;">&laquo;</a></li>';
    }else{
        $url = str_replace('[PAGE]', $page-1, $path); 
        $preButton = '<li class="page-item"><a class="page-link" href="' . htmlentities($url) . '">&laquo;</a></li>';
    }
    //下一页按钮
    if ($page == $total) {
        $nextButton = '<li class="page-item disabled"><a class="page-link" href="javascript:;">&raquo;</a></li>';
    }else{
        $url = str_replace('[PAGE]', $page+1, $path);  
        $nextButton = '<li class="page-item"><a class="page-link" href="' . htmlentities($url) . '">&raquo;</a></li>';
    }
    return sprintf('<ul class="pagination">%s %s</ul>', $preButton, $nextButton);
}
/**
 * 根据字段过滤分页参数、只保留默认两个
 * @version 1.6.0 首次引入
 * @param array $args 必需;参数列表
 * @return array $array 只返回字段中的条件语句
 */
function DcPageFilter($args=[]){
    if($args['limit'] && $args['page']){
        return [
            'list_rows' => $args['limit'],
            'page' => $args['page'],
        ];
    }
    return [];
}

/**************************************************导航模块***************************************************/
/**
 * 添加一个导航至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;普通表单数组形式;默认：空
 * @return mixed $mixed 自增ID
 */
function DcNavSave($data=[]){
    return \daicuo\Nav::save($data);
}
/**
 * 批量添加导航至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;二维数组;默认：空
 * @return obj $obj 多条数据集
 */
function DcNavSaveAll($data=[]){
    return \daicuo\Nav::save_all($data);
}
/**
 * 按条件删除导航
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcNavDelete($args=[]){

    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id'], 'eq', 'op_');
    }

    return \daicuo\Nav::delete($where);
}
/**
 * 按ID条件删除动态配置
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;Id值/多个用逗号分隔或者数组(string|array)；默认：空
 * @return mixed $mixed 删除结果(obj|null)
 */
function DcNavDeleteId($value=''){
    return \daicuo\Nav::delete_id($value);
}
/**
 * 按条件修改导航
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称(site_rotue);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @param array $data 必需;待修改的数据（普通表单数组形式）;默认：空
 * @return mixed $mixed 查询结果（array|null）
 */
function DcNavUpdate($args=[], $data=[]){
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }
    return \daicuo\Nav::update($where, $data);
}
/**
 * 按Id修改一个导航
 * @param int $opId 必需;ID值;默认：空
 * @param array $data 必需;表单数据（一维数组）;默认：空 
 * @return mixed $mixed obj|null;不为空时返回obj
 */
function DcNavUpdateId($opId, $data){
    return \daicuo\Nav::update_id($opId, $data);
}
/**
 * 按条件获取一个导航
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称(site_rotue);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcNavFind($args=[]){
    $cache = DcBool($args['cache'], true);
    if(!$where = $args['where']){
        $where = DcWhereFilter($args, ['module','controll','action','status','id','name'], 'eq', 'op_');
    }
    return \daicuo\Nav::get($where, $cache);
}
/**
 * 获取网站导航列表
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type bool $tree 可选;是否转化为树型;默认：false
 *     @type bool $level 可选;是否将树型还原为层级结构;默认：false
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type string $field 可选;查询字段;默认：*
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed obj|null 查询结果
 */
function DcNavSelect($args=[]){
    $defaults = array();
    if(!$args['where']){
        $defaults['where'] = DcWhereFilter($args, ['module','controll','action','status'], 'eq', 'op_');
    }
    if(!$args['paginate']){
        $defaults['paginate'] = DcPageFilter($args);
    }
    return \daicuo\Nav::all( DcArrayArgs($args, $defaults) );
}
/**
 * 获取导航列表简单数组格式用于
 * @version 1.3.0 首次引入
 * @param array $args 可选;array;查询条件，其属性值如下 {
 *     @type array $where 可选;where查询条件;默认：空
 *     @type bool $cache 可选;是否缓存;默认：false
 * }
 * @return mixed $mixed null|array
 */
function DcNavOption($args=[]){
    $defaults = array();
    if(!$args['where']){
        $defaults['where'] = DcWhereQuery(['op_module','op_controll','op_action'],'eq');
    }
    $defaults['cache'] = false;
    $defaults['where']['op_status'] = ['eq','normal'];
    return \daicuo\Nav::option( DcArrayArgs($args, $defaults) );
}

/**************************************************分类模块***************************************************/
/**
 * 添加一个分类至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;普通表单数组形式;默认：空
 * @return mixed $mixed 自增ID
 */
function DcTermSave($data=[]){
    $data = DcArrayArgs($data,[
        'term_status'     => 'normal',
        'term_order'      => 0,
        'term_type'       => 'category',
    ]);
    return \daicuo\Term::save($data,'term_meta');
}
/**
 * 批量添加分类至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;二维数组;默认：空
 * @return obj $obj 多条数据集
 */
function DcTermSaveAll($data=[]){
    return \daicuo\Term::save_all($data);
}
/**
 * 按条件删除一条分类
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type mixed $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $type 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $module 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $id 可选;类型ID(int|array);默认：空
 *     @type mixed $name 可选;分类名称(stirng|array);默认：空
 *     @type mixed $slug 可选;分类别名(stirng|array);默认：空
 *     @type mixed $info 可选;分类描述(stirng|array);默认：空
 *     @type mixed $parent 可选;父级ID(int|array);默认：空
 *     @type mixed $count 可选;数量统计(int|array);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcTermDelete($args=[]){
    //动态参数
    $where = DcWhereFilter($args, ['id','name','slug','module','status','type','info','parent','count'], 'eq', 'term_');
    //参数合并
    if($args['where']){
        $where = DcArrayArgs($args['where'], $where);
    }
    return \daicuo\Term::delete($where, 'term_meta');
}
/**
 * 按ID快速删除一条分类
 * @version 1.6.0 首次引入
 * @param string $module 必需;插件应用名;默认：空
 * @return array $array 影响条数值
 */
function DcTermDeleteModule($module=''){
    return \daicuo\Term::delete_module($module);
}
/**
 * 按ID快速删除一条分类
 * @version 1.6.0 首次引入
 * @param int $value 必需;Id值;默认：空
 * @return bool $bool true|false
 */
function DcTermDeleteId($id=''){
    return \daicuo\Term::delete_id($id);
}
/**
 * 按ID快速删除多条分类
 * @version 1.6.0 首次引入
 * @param mixed $ids 必需;Id值/多个用逗号分隔或者数组(string|array);默认：空
 * @return array $array 多条删除结果
 */
function DcTermDeleteIds($ids=''){
    return \daicuo\Term::delete_ids($ids);
}
/**
 * 按条件修改一个分类
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type mixed $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $type 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $module 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $id 可选;类型ID(int|array);默认：空
 *     @type mixed $name 可选;分类名称(stirng|array);默认：空
 *     @type mixed $slug 可选;分类别名(stirng|array);默认：空
 *     @type mixed $info 可选;分类描述(stirng|array);默认：空
 *     @type mixed $parent 可选;父级ID(int|array);默认：空
 *     @type mixed $count 可选;数量统计(int|array);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @param array $data 必需;待修改的数据（普通表单数组形式）;默认：空
 * @return mixed $mixed 查询结果（array|null）
 */
function DcTermUpdate($args=[], $data=[]){
    //动态参数
    $where = DcWhereFilter($args, ['id','name','slug','module','status','type','info','parent','count'], 'eq', 'term_');
    //参数合并
    if($args['where']){
        $where = DcArrayArgs($args['where'], $where);
    }
    return \daicuo\Term::update($where, $data, 'term_meta');
}
/**
 * 按Id快速修改一个分类
 * @param int $id 必需;ID值;默认：空
 * @param array $data 必需;表单数据（一维数组）;默认：空 
 * @return mixed $mixed obj|null
 */
function DcTermUpdateId($id, $data){
    return \daicuo\Term::update_id($id, $data);
}
/**
 * 按条件获取一个分类
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $field 可选;查询字段;默认：*
 *     @type mixed $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $type 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $module 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $id 可选;类型ID(int|array);默认：空
 *     @type mixed $name 可选;分类名称(stirng|array);默认：空
 *     @type mixed $slug 可选;分类别名(stirng|array);默认：空
 *     @type mixed $info 可选;分类描述(stirng|array);默认：空
 *     @type mixed $parent 可选;父级ID(int|array);默认：空
 *     @type mixed $count 可选;数量统计(int|array);默认：空
 *     @type mixed $meta_key 可选;扩展字段限制条件(string|array);默认：空
 *     @type mixed $meta_value 可选;扩展字段值限制条件(string|array);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcTermFind($args=[]){
    //where动态字段参数
    $where = DcWhereFilter($args, ['id','name','slug','module','status','type','info','parent','count','meta_key','meta_value'], 'eq', 'term_');
    //where动态数组参数
    if($args['where']){
        $args['where'] = DcArrayArgs($args['where'], $where);
    }else{
        $args['where'] = $where;
    }
    //返回结果
    return \daicuo\Term::get( DcArrayArgs($args) );
}
/**
 * 按ID快速获取一条队列信息
 * @version 1.6.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @return mixed $mixed 查询结果(obj|null)
 */
function DcTermFindId($id='', $cache=true){
    return \daicuo\Term::get_id($id, $cache);
}
/**
 * 获取多条分类数据
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $field 可选;查询字段;默认：*
 *     @type string $result 可选;返回结果类型(array|tree|obj);默认：array
 *     @type string $sort 可选;排序字段名(term_id|term_parent|term_order|trem_count|term_meta_key|term_meta_value|meta_value_num);默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $search 可选;搜索关键词（名称与别名）;默认：空
 *     @type mixed $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $type 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $module 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $id 可选;类型ID(int|array);默认：空
 *     @type mixed $name 可选;分类名称(stirng|array);默认：空
 *     @type mixed $slug 可选;分类别名(stirng|array);默认：空
 *     @type mixed $info 可选;分类描述(stirng|array);默认：空
 *     @type mixed $parent 可选;父级ID(int|array);默认：空
 *     @type mixed $count 可选;数量统计(int|array);默认：空
 *     @type mixed $meta_key 可选;扩展字段限制条件(string|array);默认：空
 *     @type mixed $meta_value 可选;扩展字段值限制条件(string|array);默认：空
 *     @type array $meta_query 可选;自定义筛选字段(二维数组[key,value,compare]);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed obj|array|null 查询结果
 */
function DcTermSelect($args=[]){
    //基础定义
    $defaults = array();
    $defaults['cache']    = true;
    $defaults['result']   = [];
    $defaults['group']    = '';
    $defaults['join']     = [];
    $defaults['with']     = 'term_meta';
    $defaults['view']     = [];
    $defaults['where']    = [];
    $defaults['paginate'] = [];
    //分页处理
    if(!$args['paginate']){
        if($defaults['paginate'] = DcPageFilter($args)){
            unset($args['limit']);
            unset($args['page']);
        }
    }
    //多条件采用JOIN查询
    if($args['meta_query']){
        $defaults['field'] = '*';
        $defaults['alias'] = 'term';
        $defaults['group'] = 'term.term_id';
        //join固定参数
        //array_push($defaults['join'],['term_meta','term_meta.term_id=term.term_id']);
        //where固定参数
        array_push($defaults['where'],['term.term_type'=>['eq',DcEmpty($args['type'],'category')]]);
        //where动态参数
        foreach(DcWhereFilter($args, ['id','name','slug','module','status','type','parent','count'], 'eq', 'term.term_') as $key=>$where){
            array_push($defaults['where'],[$key=>$where]);
        }
        //where动态拼装自定义字段多条件与JOIN
        foreach($args['meta_query'] as $key=>$where){
            //join参数拼装
            array_push($defaults['join'],['term_meta t'.$key, 't'.$key.'.term_id = term.term_id']);
            //where参数拼装
            $whereSon = [];
            if( isset($where['key']) ){
                $whereSon['t'.$key.'.term_meta_key']  = DcWhereValue($where['key'],'eq');
            }
            if( isset($where['value']) ){
                $whereSon['t'.$key.'.term_meta_value'] = DcWhereValue($where['value'],'eq');
            }
            if($whereSon){
                array_push($defaults['where'], $whereSon);
            }
        }
        //where搜索参数
        if($args['search']){
            array_push($defaults['where'],['term.term_name|term.term_slug'=>['like','%'.DcHtml($args['search']).'%']]);
            unset($args['search']);
        }
        //where参数合并
        if(!$args['where']){
            $args['where'] = $defaults['where'];
        }else{
            $args['where'] = DcArrayArgs($args['where'], $defaults['where']);
        }
        unset($defaults['where']);
        unset($args['meta_query']);
        //排序处理
        if($args['sort'] == 'meta_value_num'){
            $args['sort'] = '';
            $args['orderRaw'] = 't0.term_meta_value+0 '.$args['order'];
        }
        //返回结果
        return \daicuo\Term::result(\daicuo\Term::all(DcArrayArgs($args, $defaults)), DcEmpty($args['result'],'array'));
    }
    //是否视图查询
    if($args['meta_key'] || $args['meta_value']){
        //重置默认条件
        $defaults['group'] = 'term.term_id';
        $defaults['field'] = '';//强制为空下面定义的字段才有效
        $defaults['view']  = [
            ['term', '*'],
            ['term_meta', 'term_meta_id', 'term_meta.term_id=term.term_id']
        ];
        //自定义字段排序处理
        if($args['sort'] == 'meta_value_num'){
            $args['sort'] = '';
            $args['orderRaw'] = 'term_meta_value+0 '.$args['order'];
        }
    }
    //where动态参数
    $defaults['where'] = DcWhereFilter($args, ['id','name','slug','module','status','type','info','parent','count','meta_key','meta_value'], 'eq', 'term_');
    //where搜索参数
    if($args['search']){
        $defaults['where'] = DcArrayArgs($defaults['where'], ['term_name|term_slug'=>['like','%'.DcHtml($args['search']).'%']]);
        unset($args['search']);
    }
    //where参数合并
    if(!$args['where']){
        $args['where'] = $defaults['where'];
    }else{
        $args['where'] = DcArrayArgs($args['where'], $defaults['where']);
    }
    unset($defaults['where']);
    //返回结果
    return \daicuo\Term::result(\daicuo\Term::all(DcArrayArgs($args, $defaults)), DcEmpty($args['result'], 'array'));
}
/**
 * 获取分类列表checkbox关系
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：false
 *     @type string $result 可选;返回结果类型(array|tree|obj);默认：array
 *     @type string $sort 可选;排序字段名(term_id|term_parent|term_order|trem_count|term_meta_key|term_meta_value|meta_value_num);默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $search 可选;搜索关键词（名称与别名）;默认：空
 *     @type mixed $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $type 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $module 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $id 可选;类型ID(int|array);默认：空
 *     @type mixed $name 可选;分类名称(stirng|array);默认：空
 *     @type mixed $slug 可选;分类别名(stirng|array);默认：空
 *     @type mixed $info 可选;分类描述(stirng|array);默认：空
 *     @type mixed $parent 可选;父级ID(int|array);默认：空
 *     @type mixed $count 可选;数量统计(int|array);默认：空
 * }
 * @return mixed $mixed obj|null 
 */
function DcTermCheck($args=[]){
    $args = DcArrayArgs($args,[
        'type'       => 'category',
        'fieldKey'   => 'term_id',
        'fieldValue' => 'term_name',
    ]);
    return \daicuo\Term::Option($args);
}
/**
 * 获取分类列表select关系
 * @version 1.6.0 优化调用参数
 * @version 1.5.20 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：false
 *     @type string $result 可选;返回结果类型(array|tree|obj);默认：array
 *     @type string $sort 可选;排序字段名(term_id|term_parent|term_order|trem_count|term_meta_key|term_meta_value|meta_value_num);默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $search 可选;搜索关键词（名称与别名）;默认：空
 *     @type mixed $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $type 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $module 可选;队列类型(stirng|array),固定范围(category|tag);默认：category
 *     @type mixed $id 可选;类型ID(int|array);默认：空
 *     @type mixed $name 可选;分类名称(stirng|array);默认：空
 *     @type mixed $slug 可选;分类别名(stirng|array);默认：空
 *     @type mixed $info 可选;分类描述(stirng|array);默认：空
 *     @type mixed $parent 可选;父级ID(int|array);默认：空
 *     @type mixed $count 可选;数量统计(int|array);默认：空
 * }
 * @return mixed $mixed null|array
 */
function DcTermOption($args=[]){
    $args = DcArrayArgs($args,[
        'type'       => 'category',
        'fieldKey'   => 'term_id',
        'fieldValue' => 'term_name',
        'isSelect'   => true,
    ]);
    return \daicuo\Term::Option($args);
}
/**
 * 通过termId获取所有子类ID
 * @version 1.6.0 首次引入
 * @param int $termId 必需;队列ID;默认：空
 * @param string $termType 必需;队列类型(category|tag);默认：category
 * @param string $result 必需;返回类型(array|string);默认：array
 * @return mixed $mixed array|string
 */
function DcTermSubIds($termId, $termType='category', $result='array'){
    if(!$termId){
        return null;
    }
    $subIds = [$termId];
    foreach(\daicuo\Term::childrens($termId, $termType) as $key=>$value){
        array_push($subIds, $value['term_id']);
    }
    if($result == 'array'){
        return $subIds;
    }
    return implode(',', $subIds);
}
/**
 * 通过队列名获取队列ID
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;队例字段值；默认：空
 * @param string $type 必需;队例类型(category|tag)；默认：category
 * @param bool $cache 可选;是否缓存；默认：空
 * @return int $int 父级ID值
 */
function DcTermNameToId($value='', $type='category', $cache=true){
    $term = \daicuo\Term::get_by('term_name', $value, $cache, $type);
    return intval($term['term_id']);
}
/**
 * 多个队列名转化为多个队例ID
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;多个队列名称,逗号分隔或数组(string|array)；默认：空
 * @param string $type 必需;队例类型(category|tag)；默认：category
 * @param bool $cache 可选;是否缓存；默认：空
 * @return int $int 父级ID值
 */
function DcTermNameToIds($value='', $type='category', $cache=true){
    if(!$value){
        return [0];
    }
    if(is_array($value)){
        $value = implode(',',$value);
    }
    $terms = DcTermSelect([
        'cache' => $cache,
        'type'  => $type,
        'name'  => ['in', $value],
        //'sort'  => 'meta_value_num',
        //'meta_key' => ['in','term_tpl,term_hook']
    ]);
    return array_column($terms, 'term_id');
}
/**
 * 通过队列别名获取队列ID
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;队例字段值；默认：空
 * @param string $type 必需;队例类型(category|tag)；默认：category
 * @param bool $cache 可选;是否缓存；默认：空
 * @return int $int 父级ID值
 */
function DcTermSlugToId($value='', $type='category', $cache=true){
    $term = \daicuo\Term::get_by('term_slug', $value, $cache, $type);
    return intval($term['term_id']);
}
/**
 * 多个队列别名转化为多个队例ID
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;多个队列名称,逗号分隔或数组(string|array)；默认：空
 * @param string $type 必需;队例类型(category|tag)；默认：category
 * @param bool $cache 可选;是否缓存；默认：空
 * @return int $int 父级ID值
 */
function DcTermSlugToIds($value='', $type='category', $cache=true){
    if(!$value){
        return [0];
    }
    if(is_array($value)){
        $value = implode(',',$value);
    }
    $terms = DcTermSelect([
        'cache' => $cache,
        'type'  => $type,
        'slug'  => ['in', $value],
    ]);
    return array_column($terms, 'term_id');
}

/**************************************************用户模块***************************************************/
/**
 * 添加一个用户至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;普通表单数组形式;默认：空
 * @return mixed $mixed 自增ID
 */
function DcUserSave($data=[]){
    $data = DcArrayArgs($data,[
        'user_status'       => 'normal',
        'user_hits'         => 0,
        'user_views'        => 0,
        'user_capabilities' => 'guest',
    ]);
    return \daicuo\User::save($data,'user_meta');
}
/**
 * 批量添加用户至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;二维数组;默认：空
 * @return obj $obj 多条数据集
 */
function DcUserSaveAll($data=[]){
    return \daicuo\User::save_all($data);
}
/**
 * 按条件删除一条用户数据
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type string $slug 可选;配置名称;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcUserDelete($args=[]){
    //动态参数
    $where = DcWhereFilter($args, ['id','name','slug','module','status'], 'eq', 'user_');
    //参数合并
    if($args['where']){
        $where = DcArrayArgs($args['where'], $where);
    }
    return \daicuo\User::delete($where, 'user_meta');
}
/**
 * 按ID快速删除一条用户
 * @version 1.6.0 首次引入
 * @param int $value 必需;Id值;默认：空
 * @return bool $bool true|false
 */
function DcUserDeleteId($id=''){
    return \daicuo\User::delete_user_by('user_id',$id);
}
/**
 * 按ID快速删除多条用户
 * @version 1.6.0 首次引入
 * @param mixed $ids 必需;Id值/多个用逗号分隔或者数组(string|array);默认：空
 * @return array $array 多条删除结果
 */
function DcUserDeleteIds($ids=''){
    return \daicuo\User::delete_ids($ids);
}
/**
 * 按字段名称与值快速删除一条用户
 * @version 1.6.0 首次引入
 * @param string $field 必需;字段名(user_id|user_name|user_email|user_mobile|user_token);默认：user_id
 * @param mixed $value 必需;字段值;默认：空
 * @return bool $bool true|false
 */
function DcUserDeleteField($field='user_id', $value=''){
    return \daicuo\User::delete_user_by($field, $value);
}
/**
 * 按模块标识快速删除多条用户
 * @version 1.6.0 首次引入
 * @param string $module 必需;模块名;默认：空
 * @return array $array 基础数据与扩展数据删除结果
 */
function DcUserDeleteModule($module=''){
    return \daicuo\User::delete_module($module);
}
/**
 * 按条件修改一个用户
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type int $id 可选;用户ID;默认：空
 *     @type string $name 可选;用户名称;默认：空
 *     @type string $slug 可选;用户别名;默认：空
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @param array $data 必需;待修改的数据（普通表单数组形式）;默认：空
 * @return mixed $mixed 查询结果（array|null）
 */
function DcUserUpdate($args=[], $data=[]){
    //动态参数
    $where = DcWhereFilter($args, ['id','name','slug','module','status'], 'eq', 'user_');
    //参数合并
    if($args['where']){
        $where = DcArrayArgs($args['where'], $where);
    }
    return \daicuo\User::update($where, $data, 'user_meta');
}
/**
 * 按Id快速修改一个用户
 * @param int $id 必需;ID值;默认：空
 * @param array $data 必需;表单数据（一维数组）;默认：空 
 * @return mixed $mixed obj|null
 */
function DcUserUpdateId($id, $data){
    return \daicuo\User::update_id($id, $data);
}
/**
 * 按字段名称与值快速删除一条用户
 * @version 1.6.0 首次引入
 * @param string $field 必需;字段名(user_id|user_name|user_email|user_mobile|user_token);默认：user_id
 * @param mixed $value 必需;字段值;默认：空
 * @param array $data 必需;表单数据（一维数组）;默认：空 
 * @return bool $bool true|false
 */
function DcUserUpdateField($field='user_id', $value='', $data=[]){
    return \daicuo\User::update_user_by($field, $value, $data);
}
/**
 * 按条件获取一个用户数据
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type int $id 可选;用户ID;默认：空
 *     @type mixed $name 可选;用户名称(stirng|array);默认：空
 *     @type mixed $slug 可选;用户别名(stirng|array);默认：空
 *     @type mixed $module 可选;模型名称(stirng|array);默认：空
 *     @type mixed $email 可选;用户邮箱(int|array);默认：空
 *     @type mixed $mobile 可选;用户手机(stirng|array);默认：空
 *     @type mixed $token 可选;用户令牌(string|array);默认：空
 *     @type mixed $meta_key 可选;扩展字段限制条件(string|array);默认：空
 *     @type mixed $meta_value 可选;扩展字段值限制条件(string|array);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcUserFind($args=[]){
    //where动态字段参数
    $where = DcWhereFilter($args, ['id','name','slug','module','status','email','mobile','token','meta_key','meta_value'], 'eq', 'user_');
    //where动态数组参数
    if($args['where']){
        $args['where'] = DcArrayArgs($args['where'], $where);
    }else{
        $args['where'] = $where;
    }
    //返回结果
    return \daicuo\User::get( DcArrayArgs($args) );
}
/**
 * 按ID快速获取一条用户数据
 * @version 1.6.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @return mixed $mixed 查询结果(obj|null)
 */
function DcUserFindId($id='', $cache=true){
    return \daicuo\User::get_user_by('user_id', $id, $cache);
}
/**
 * 按字段名称与值快速获取一条用户数据
 * @version 1.6.0 首次引入
 * @param string $field 必需;字段名(user_id|user_name|user_email|user_mobile|user_token);默认：user_id
 * @param mixed $value 必需;字段值;默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @return bool $bool true|false
 */
function DcUserFindField($field='user_id', $value='', $cache=true){
    return \daicuo\User::get_user_by($field, $value, $cache);
}
/**
 * 获取多条用户数据
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：false
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $field 可选;查询字段;默认：*
 *     @type string $result 可选;返回结果类型(array|tree|obj);默认：array
 *     @type string $sort 可选;排序字段名(user_id|user_views|user|hits|user_meta_key|user_meta|value|meta_value_num);默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $search 可选;搜索关键词（名称与别名）;默认：空
 *     @type mixed $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $module 可选;来源模块(stirng|array);默认：空
 *     @type mixed $controll 可选;来源控制器(stirng|array);默认：空
 *     @type mixed $action 可选;来源操作(stirng|array);默认：空
 *     @type mixed $id 可选;用户ID(int|array);默认：空
 *     @type mixed $slug 可选;分类别名(stirng|array);默认：空
 *     @type mixed $name 可选;用户名(stirng|array);默认：空
 *     @type mixed $nice_name 可选;呢称(stirng|array);默认：空
 *     @type mixed $email 可选;用户邮箱(stirng|array);默认：空
 *     @type mixed $mobile 可选;用户手机(stirng|array);默认：空
 *     @type mixed $creatime_time 可选;创建时间(int|array);默认：空
 *     @type mixed $update_time 可选;修改时间(int|array);默认：空
 *     @type mixed $creatime_ip 可选;注册时IP(stirng|array);默认：空
 *     @type mixed $update_ip 可选;最后登录IP(string|array);默认：空
 *     @type mixed $views 可选;人气值(int|array);默认：空
 *     @type mixed $hits 可选;点击数(int|array);默认：空
 * }
 * @return mixed $mixed obj|array|null 查询结果
 */
function DcUserSelect($args=[]){
    //基础定义
    $defaults = array();
    $defaults['cache']    = true;
    $defaults['result']   = [];
    $defaults['group']    = '';
    $defaults['with']     = 'user_meta';
    $defaults['join']     = [];
    $defaults['view']     = [];
    $defaults['where']    = [];
    $defaults['paginate'] = [];
    //分页处理
    if(!$args['paginate']){
        if($defaults['paginate'] = DcPageFilter($args)){
            unset($args['limit']);
            unset($args['page']);
        }
    }
    //多条件采用JOIN查询
    if($args['meta_query']){
        $defaults['field'] = '*';
        $defaults['alias'] = 'user';
        $defaults['group'] = 'user.user_id';
        //where动态参数
        foreach(DcWhereFilter($args, ['id','name','slug','email','mobile','token','status','module','controll','action'], 'eq', 'user.user_') as $key=>$where){
            array_push($defaults['where'],[$key=>$where]);
        }
        //where动态拼装自定义字段多条件与JOIN
        foreach($args['meta_query'] as $key=>$where){
            //join参数拼装
            array_push($defaults['join'],['user_meta t'.$key, 't'.$key.'.user_id = user.user_id']);
            //where参数拼装
            $whereSon = [];
            if( isset($where['key']) ){
                $whereSon['t'.$key.'.user_meta_key']  = DcWhereValue($where['key'],'eq');
            }
            if( isset($where['value']) ){
                $whereSon['t'.$key.'.user_meta_value'] = DcWhereValue($where['value'],'eq');
            }
            if($whereSon){
                array_push($defaults['where'], $whereSon);
            }
        }
        //where搜索参数
        if($args['search']){
            array_push($defaults['where'],['user.user_name|user.user_slug|user.user_nice_name|user.user_email|user.user_mobile'=>['like','%'.DcHtml($args['search']).'%']]);
            unset($args['search']);
        }
        //where参数合并
        if(!$args['where']){
            $args['where'] = $defaults['where'];
        }else{
            $args['where'] = DcArrayArgs($args['where'], $defaults['where']);
        }
        unset($defaults['where']);
        unset($args['meta_query']);
        //排序处理
        if($args['sort'] == 'meta_value_num'){
            $args['sort'] = '';
            $args['orderRaw'] = 't0.user_meta_value+0 '.$args['order'];
        }
        //返回结果
        return \daicuo\User::result(\daicuo\User::all(DcArrayArgs($args, $defaults)), DcEmpty($args['result'],'array'));
    }
    //视图查询
    if($args['meta_key'] || $args['meta_value']){
        $defaults['field'] = '';
        $defaults['group'] = 'user.user_id';
        $defaults['view']  = [
            ['user', '*'],
            ['user_meta', 'user_meta_id', 'user_meta.user_id=user.user_id'],
        ];
    }
    //where动态参数
    $defaults['where'] = DcWhereFilter($args, ['id','name','slug','email','mobile','token','status','module','controll','action','meta_key','meta_value'], 'eq', 'user_');
    //where搜索参数
    if($args['search']){
        $defaults['where'] = DcArrayArgs($defaults['where'], ['user_name|user_slug|user_nice_name|user_email|user_mobile'=>['like','%'.DcHtml($args['search']).'%']]);
        unset($args['search']);
    }
    //where参数合并
    if($args['where']){
        $args['where'] = DcArrayArgs($args['where'], $defaults['where']);
    }else{
        $args['where'] = $defaults['where'];
    }
    unset($defaults['where']);
    //排序处理
    if($args['sort'] == 'meta_value_num'){
        $args['sort'] = '';
        $args['orderRaw'] = 'user_meta_value+0 '.$args['order'];
    }
    //返回结果
    return \daicuo\User::result(\daicuo\User::all(DcArrayArgs($args, $defaults)), DcEmpty($args['result'],'array'));
}
/**
 * 通过用户名与密码快速登录
 * @version 1.6.0 首次引入
 * @param array $post 必需;查询条件数组格式 {
 *     @type string $user_name 必需;用户名、邮箱、手机自动判断（name|email|mobile）;默认：name
 *     @type string $user_pass 必需;用户密码;默认：空
 *     @type bool $user_expire 可选;是否记住登录（由后台配置记录时长）;默认：false
 * }
 * @return bool $bool 是否登录成功 
 */
function DcUserLogin($post=[]){
    return \daicuo\User::Login($post);
}
/**
 * 退出用户登录
 * @version 1.6.0 首次引入
 * @return bool $bool 是否登录成功 
 */
function DcUserLogout(){
    return \daicuo\User::Logout();
}
/**
 * 判断当前用户是否被锁定
 * @version 1.6.0 首次引入
 * @return bool $bool true|false  
 */
function DcUserCurrentIsLock(){
    return \daicuo\User::is_lock();
}
/**
 * 判断当前用户是否已登录
 * @version 1.6.0 首次引入
 * @return bool $bool true|false  
 */
function DcUserCurrentIsLogin(){
    return \daicuo\User::is_logged_in();
}
/**
 * 获取当前用户登录信息
 * @version 1.6.0 首次引入
 * @return array $array 用户信息（未登录则为游客） 
 */
function DcUserCurrentGet(){
    return \daicuo\User::get_current_user();
}
/**
 * 获取当前用户登录信息
 * @version 1.6.0 首次引入
 * @return array $array 用户信息（未登录则为游客） 
 */
function DcUserCurrentGetId(){
    return \daicuo\User::get_current_user_id();
}
/**
 * 通过ID设置当前用户登录信息（但不会登录为该用户ID）
 * @version 1.6.0 首次引入
 * @param int $id 必需;用户ID;默认：空
 * @return array $array 用户信息（未登录则为游客） 
 */
function DcUserCurrentSet($id=0){
    return \daicuo\User::set_current_user($id);
}
/**
 * 通过用户ID与metaKey快速增加一条自定义信息
 * @version 1.6.0 首次引入
 * @param string $userId 必需;用户ID;默认：空
 * @param string $metaKey 必需;自定义字段;默认：空
 * @param mixed $metaValue 必需;自定义字段段（int|array|string）;默认：空
 * @return int $int 影响条数
 */
function DcUserMetaSave($userId=0, $metaKey='', $metaValue=''){
    return \daicuo\User::save_user_meta($userId, $metaKey, $metaValue);
}
/**
 * 通过用户ID与metaKey快速删除自定义信息
 * @version 1.6.0 首次引入
 * @param string $userId 必需;用户ID;默认：空
 * @param string $metaKey 可选;自定义字段名为空时删除所有自定义字段;默认：空
 * @return int $int 影响条数
 */
function DcUserMetaDelete($userId=0, $metaKey=''){
    return \daicuo\User::delete_user_meta($userId, $metaKey);
}
/**
 * 通过用户ID与metaKey快速修改一条自定义信息
 * @version 1.6.0 首次引入
 * @param string $userId 必需;用户ID;默认：空
 * @param string $metaKey 必需;自定义字段;默认：空
 * @param mixed $metaValue 必需;自定义字段段（int|array|string）;默认：空
 * @return int $int 影响条数
 */
function DcUserMetaUpdate($userId=0, $metaKey='', $metaValue=''){
    return \daicuo\User::update_user_meta($userId, $metaKey, $metaValue);
}
/**
 * 通过用户ID与metaKey快速获取一条metaValue
 * @version 1.6.0 首次引入
 * @param string $userId 必需;用户ID;默认：空
 * @param string $metaKey 必需;自定义字段;默认：空
 * @return int $int 影响条数
 */
function DcUserMetaGet($userId=0, $metaKey=''){
    return \daicuo\User::get_user_meta($userId, $metaKey);
}
/**
 * 通过用户ID与metaKey快速获取一条metaValue
 * @version 1.6.0 首次引入
 * @param string $userId 必需;用户ID;默认：空
 * @return array $array keyvalue形式的数组
 */
function DcUserMetaSelect($userId){
    return \daicuo\User::select_user_meta($userId);
}
/**
 * 按条件删除一条用户扩展表数据
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type int $id 可选;用户ID;默认：空
 *     @type int $meta_id 可选;扩展ID;默认：空
 *     @type string $meta_key 可选;自定义字段键名;默认：空
 *     @type string $meta_value 可选;自定义字段键值;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed obj|null
 */
function DcUserMetaDeletes($args=[]){
    //动态参数
    $where = DcWhereFilter($args, ['id','meta_id','meta_key','meta_value'], 'eq', 'user_');
    //参数合并
    if($args['where']){
        $where = DcArrayArgs($args['where'], $where);
    }
    return \daicuo\User::delete_user_meta($where);
}

/************************************用户TOKEN模块*******************************************************/
/**
 * 通过用户名与密码快速获取TOKEN信息、每次成功后会刷新过期时间
 * @version 1.6.0 首次引入
 * @param string $userName 必需;用户名;默认：空
 * @param string $userPass 必需;用户密码;默认：空
 * @return mixed $mixed 登录成功时返回token值与过期时间(array|false)
 */
function DcUserTokenLogin($userName='',$userPass=''){
    return \daicuo\User::token_login($userName,$userPass);
}
/**
 * 通过Token值验证是否正常（是否存在并未过期）
 * @version 1.6.0 首次引入
 * @param string $userToken 必需;用户TOKEN;默认：空
 * @return bool $bool true|false
 */
function DcUserTokenCheck($userToken=''){
    return \daicuo\User::token_check($userToken);
}
/**
 * 通过Token值延迟过期时间
 * @version 1.6.0 首次引入
 * @param string $userToken 必需;用户旧TOKEN;默认：空
 * @param string $userExpire 必需;延迟时长（天）;默认：30
 * @return array $array TOKEN值与新的过期时间
 */
function DcUserTokenExpire($userToken='', $userExpire=30){
    return \daicuo\User::token_expire($userToken, $userExpire);
}
/**
 * 加密用户Token
 * @version 1.6.0 首次引入
 * @param string $userToken 必需;待加密的用户TOKEN;默认：空
 * @return string $string 加密后的TOKEN
 */
function DcUserTokenEncode($userToken=''){
    return \daicuo\User::token_encode($userToken);
}
/**
 * 解密用户Token
 * @version 1.6.0 首次引入
 * @param string $userToken 必需;待解密的用户TOKEN;默认：空
 * @return string $string 解密后的TOKEN
 */
function DcUserTokenDecode($userToken=''){
    return \daicuo\User::token_decode($userToken);
}
/**
 * 通过用户ID生成新的TOKEN
 * @version 1.6.0 首次引入
 * @param int $userId 必需;用户ID;默认：空
 * @return string $string 用户TOKEN
 */
function DcUserTokenCreate($userId=0){
    return \daicuo\User::token_create($userId);
}
/**
 * 通过用户ID设置用户TOKEN过期
 * @version 1.6.0 首次引入
 * @param int $userId 必需;用户ID;默认：空
 * @return string $string 用户TOKEN
 */
function DcUserTokenDelete($userId=0){
    return \daicuo\User::token_delete($userId);
}
/**
 * 通过用户ID修改Token过期时间与修改Token值
 * @version 1.6.0 首次引入
 * @param int $userId 必需;用户ID;默认：空
 * @param string $userExpire 必需;延迟时长（天）;默认：30
 * @param string $userToken 可选;自定义TOKEN（不修改传入旧的）;默认：空
 * @return mixed $mixed 成功时返回Token值与过期时间(array|false)
 */
function DcUserTokenUpdate($userId=0, $userExpire=30, $userToken=''){
    return \daicuo\User::token_update($userId, $userExpire, $userToken);
}
/**
 * 通过Token请求获取用户信息（Header['HTTP-TOKEN']>Url['token']）
 * @version 1.6.0 首次引入
 * @return array $array 失败时返回游客信息
 */
function DcUserTokenGet(){
    return \daicuo\User::token_current_user();
}

/************************************用户权限模块*******************************************************/
/**
 * 添加一个角色与权限关系至数据库
 * @version 1.6.0 首次引入
 * @param string $role 必需;角色名;默认：空
 * @param string $note 必需;权限节点（如：index/index/index）;默认：空
 * @return int $int 自增ID
 */
function DcAuthSave($role='', $note=''){
    if(!$role || !$note){
        return 0;
    }
    $data = [];
    $data['op_name']     = $role;
    $data['op_value']    = $note;
    $data['op_module']   = 'common';
    $data['op_controll'] = 'system';
    $data['op_action']   = 'auth';
    $data['op_order']    = 0;
    $data['op_status']   = 'normal';
    $data = DcArrayArgs(['op_name'=>$role,'op_value'=>$note], DcAuthData());
    $result = \daicuo\Op::save($data);
    if($result){
        DcCache('auth_all', null);
    }
    return $result;
}
/**
 * 批量添加多个角色与权限关系至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;keyValue数组格式;默认：空
 * @return mixed $mixed obj|null
 */
function DcAuthSaveAll($data=[]){
    if(!$data){
        return null;
    }
    $result = \daicuo\Op::write($data,'common','system','auth','0','no');
    if(!is_null($result)){
        DcCache('auth_all', null);
    }
    return $result;
}
/**
 * 按ID快速删除一条权限关系
 * @version 1.6.0 首次引入
 * @param int $id 必需;Id值;默认：空
 * @return int $int 影响条数
 */
function DcAuthDeleteId($id=''){
    return DcCacheResult(\daicuo\Op::delete_id($id), 'auth_all', 'key');
}
/**
 * 获取系统已配置的角色与权限对应关系
 * @version 1.6.0 首次引入
 * @return array $array 二维数组对应关系
 */
function DcAuthConfig(){
    return \daicuo\Auth::get_config();
}
/**
 * 获取系统已配置的角色列表
 * @version 1.6.0 首次引入
 * @return array $array 普通数组
 */
function DcAuthRoles(){
    return \daicuo\Auth::get_roles();
}
/**
 * 获取系统已配置的权限节点列表
 * @version 1.6.0 首次引入
 * @return array $array 普通数组
 */
function DcAuthCaps(){
    return \daicuo\Auth::get_caps();
}
/**
 * 获取一个用户拥有的所有权限
 * @version 1.6.0 首次引入
 * @param string|array $userRoles 必需;用户的角色名;默认空
 * @param string|array $userCaps 可选;用户的权限节点名（可单独设置）
 * @return array $array 普通数组
 */
function DcAuthCapsUser($userRoles=[], $userCaps=[]){
    return \daicuo\Auth::get_user_caps($userRoles, $userCaps);
}
/**
 * 验证一个用户拥有的所有权限是否在权限节点内
 * @version 1.6.0 首次引入
 * @param string|array $name 必需;需要验证的规则列表,支持逗号分隔的权限规则或索引数组;默认空
 * @param string|array $userRoles 必需;用户的角色名(为用户ID时自动查询);默认空
 * @param string|array $userCaps 可选;用户的权限节点名（可单独设置）;默认空
 * @param string $relation 可选;验证关系(and|or);默认：or
 * @param string $mode 可选;执行验证的模式,可分为url,normal;默认：url
 * @return bool $bool true|false
 */
function DcAuthCheck($name, $userRoles='', $userCaps='', $relation = 'or', $mode = 'url'){
    return \daicuo\Auth::check($name, $userRoles, $userCaps, $relation, $mode);
}
/**
 * 获取权限列表keyValue数组格式
 * @version 1.3.0 首次引入
 * @return array $array 数组
 */
function DcAuthOption(){
    $options = [];
    foreach(\daicuo\Auth::get_roles() as $role){
        $options[$role] = lang($role);
    }
    return $options;
}

/**************************************************内容模块***************************************************/
/**
 * 添加一个内容至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;普通表单数组形式;默认：空
 * @return mixed $mixed 自增ID
 */
function DcInfoSave($data=[]){
    $data = DcArrayArgs($data,[
        'info_status'       => 'normal',
        'info_hits'         => 0,
        'info_views'        => 0,
    ]);
    return \daicuo\Info::save($data,'info_meta,term_map');
}
/**
 * 批量添加内容至数据库
 * @version 1.6.0 首次引入
 * @param array $data 必需;二维数组;默认：空
 * @return obj $obj 多条数据集
 */
function DcInfoSaveAll($data=[]){
    return \daicuo\Info::save_all($data);
}
/**
 * 按条件删除一条内容数据
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type int $id 可选;配置ID;默认：空
 *     @type string $name 可选;配置名称;默认：空
 *     @type string $slug 可选;配置名称;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcInfoDelete($args=[]){
    //动态参数
    $where = DcWhereFilter($args, ['id','title','name','slug','excerpt','password','parent','user_id','create_time','update_time','type','mime_type','views','hits','status','module','controll','action'], 'eq', 'Info_');
    //参数合并
    if($args['where']){
        $where = DcArrayArgs($args['where'], $where);
    }
    return \daicuo\Info::delete($where, 'info_meta,term_map');
}
/**
 * 按ID快速删除一条内容
 * @version 1.6.0 首次引入
 * @param int $value 必需;Id值;默认：空
 * @return bool $bool true|false
 */
function DcInfoDeleteId($id=''){
    return \daicuo\Info::delete_id($id);
}
/**
 * 按条件修改一个内容
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type int $id 可选;内容ID;默认：空
 *     @type string $name 可选;内容名称;默认：空
 *     @type string $slug 可选;内容别名;默认：空
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @param array $data 必需;待修改的数据（普通表单数组形式）;默认：空
 * @return mixed $mixed 查询结果（array|null）
 */
function DcInfoUpdate($args=[], $data=[]){
    //动态参数
    $where = DcWhereFilter($args, ['id','title','name','slug','excerpt','password','parent','user_id','create_time','update_time','type','mime_type','views','hits','status','module','controll','action'], 'eq', 'Info_');
    //参数合并
    if($args['where']){
        $where = DcArrayArgs($args['where'], $where);
    }
    return \daicuo\Info::update($where, $data, 'info_meta,term_map');
}
/**
 * 按Id快速修改一个内容
 * @param int $id 必需;ID值;默认：空
 * @param array $data 必需;表单数据（一维数组）;默认：空 
 * @return mixed $mixed obj|null
 */
function DcInfoUpdateId($id, $data){
    return \daicuo\Info::update_id($id, $data);
}
/**
 * 按条件获取一个内容数据
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type int $id 可选;内容ID;默认：空
 *     @type mixed $name 可选;内容名称(stirng|array);默认：空
 *     @type mixed $slug 可选;内容别名(stirng|array);默认：空
 *     @type mixed $module 可选;模型名称(stirng|array);默认：空
 *     @type mixed $meta_key 可选;扩展字段限制条件(string|array);默认：空
 *     @type mixed $meta_value 可选;扩展字段值限制条件(string|array);默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function DcInfoFind($args=[]){
    //where动态字段参数
    $where = DcWhereFilter($args, ['id','title','name','slug','excerpt','password','parent','user_id','create_time','update_time','type','mime_type','views','hits','status','module','controll','action'], 'eq', 'info_');
    //where动态数组参数
    if($args['where']){
        $args['where'] = DcArrayArgs($args['where'], $where);
    }else{
        $args['where'] = $where;
    }
    //返回结果
    return \daicuo\Info::get( DcArrayArgs($args) );
}
/**
 * 按ID快速获取一条内容数据
 * @version 1.6.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(obj|null)
 */
function DcInfoFindId($id='', $cache=true, $status='normal'){
    return \daicuo\Info::get_by('info_id', $id, $cache, false, $status);
}
/**
 * 按别名快速获取一条内容数据
 * @version 1.6.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(obj|null)
 */
function DcInfoFindSlug($id='', $cache=true, $status='normal'){
    return \daicuo\Info::get_by('info_slug', $id, $cache, false, $status);
}
/**
 * 获取多条内容数据
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $field 可选;查询字段;默认：*
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $sort 可选;排序字段名(info_id|info_order|info_views|info_hits|meta_value_num);默认：info_id
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $search 可选;搜索关键词（info_name|info_slug|info_excerpt）;默认：空
 *     @type mixed $id 可选;内容ID限制条件(int|array);默认：空
 *     @type mixed $title 可选;标题限制条件(stirng|array);默认：空
 *     @type mixed $name 可选;名称限制条件(stirng|array);默认：空
 *     @type mixed $slug 可选;别名限制条件(stirng|array);默认：空
 *     @type mixde $module 可选;所属模块(stirng|array);默认：空
 *     @type mixde $controll 可选;所属迭制器(stirng|array);默认：空
 *     @type mixde $action 可选;所属操作(stirng|array);默认：空
 *     @type mixed $term_id 可选;分类法ID限制条件(string|array);默认：空
 *     @type array $meta_query 可选;自定义字段(二维数组[key=>['eq','key'],value=>['in','key']]);默认：空
 *     @type string $result 可选;返回结果类型(array|obj);默认：array
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed obj|array|null 查询结果
 */
function DcInfoSelect($args=[]){
    //基础定义
    $defaults = array();
    $defaults['cache']    = true;
    $defaults['result']   = [];
    $defaults['group']    = '';
    $defaults['with']     = 'info_meta,term,user';
    $defaults['join']     = [];
    $defaults['view']     = [];
    $defaults['where']    = [];
    $defaults['paginate'] = [];
    //分页处理
    if(!$args['paginate']){
        if($defaults['paginate'] = DcPageFilter($args)){
            unset($args['limit']);
            unset($args['page']);
        }
    }
    //多条件采用JOIN查询
    if($args['meta_query']){
        $defaults['field'] = '*';
        $defaults['alias'] = 'info';
        $defaults['group'] = 'info.info_id';
        //join固定参数
        array_push($defaults['join'],['term_map','term_map.detail_id = info.info_id']);
        //where动态参数
        foreach(DcWhereFilter($args, ['id','title','name','slug','excerpt','password','parent','user_id','create_time','update_time','type','mime_type','views','hits','status','module','controll','action'], 'eq', 'info.info_') as $key=>$where){
            array_push($defaults['where'],[$key=>$where]);
        }
        //where动态拼装自定义字段多条件与JOIN
        foreach($args['meta_query'] as $key=>$where){
            //join参数拼装
            array_push($defaults['join'],['info_meta t'.$key, 't'.$key.'.info_id = info.info_id']);
            //where参数拼装
            $whereSon = [];
            if( isset($where['key']) ){
                $whereSon['t'.$key.'.info_meta_key']  = DcWhereValue($where['key'],'eq');
            }
            if( isset($where['value']) ){
                $whereSon['t'.$key.'.info_meta_value'] = DcWhereValue($where['value'],'eq');
            }
            if($whereSon){
                array_push($defaults['where'], $whereSon);
            }
            /*
            array_push($defaults['where'],[
                't'.$key.'.info_meta_key'   => [DcEmpty($where['compare'],'eq'), $where['key']],
                't'.$key.'.info_meta_value' => [DcEmpty($where['compare'],'eq'), $where['value']],
            ]);*/
        }
        //where搜索参数
        if($args['search']){
            array_push($defaults['where'],['info.info_title|info.info_name|info.info_slug|info.info_excerpt'=>['like','%'.DcHtml($args['search']).'%']]);
            unset($args['search']);
        }
        //where分类法参数
        if( $args['term_id'] ){
            array_push($defaults['where'],['term_map.term_id' => DcWhereValue($args['term_id'],'in')]);
            unset($args['much_id']);
        }
        //where参数合并
        if(!$args['where']){
            $args['where'] = $defaults['where'];
        }else{
            $args['where'] = DcArrayArgs($args['where'], $defaults['where']);
        }
        unset($defaults['where']);
        unset($args['meta_query']);
        //排序处理
        if($args['sort'] == 'meta_value_num'){
            $args['sort'] = '';
            $args['orderRaw'] = 't0.info_meta_value+0 '.$args['order'];
        }
        //返回结果
        return \daicuo\Info::result(\daicuo\Info::all(DcArrayArgs($args, $defaults)), DcEmpty($args['result'],'array'));
    }
    //是否视图查询
    if($args['meta_key'] || $args['meta_value'] || $args['term_id']){
        //重置默认条件
        $defaults['field'] = '';
        $defaults['group'] = 'info.info_id';
        $defaults['view']  = [
            ['info', '*'],
            ['info_meta', 'info_meta_id', 'info_meta.info_id=info.info_id']
        ];
        //队列ID存在时还需关联关系表
        if($args['term_id']){
            array_push($defaults['view'], ['term_map' , 'term_id', 'term_map.detail_id=info.info_id'] );
        }
        //自定义字段排序处理
        if($args['sort'] == 'meta_value_num'){
            $args['sort'] = '';
            $args['orderRaw'] = 'info_meta_value+0 '.$args['order'];
        }
    }
    //where动态参数
    $defaults['where'] = DcWhereFilter($args, ['id','title','name','slug','excerpt','password','parent','user_id','create_time','update_time','type','mime_type','views','hits','status','module','controll','action','meta_key','meta_value'], 'eq', 'info_');
    //where搜索参数
    if($args['search']){
        $defaults['where'] = DcArrayArgs($defaults['where'], ['info_title|info_name|info_slug|info_excerpt'=>['like','%'.DcHtml($args['search']).'%']]);
        unset($args['search']);
    }
    //where分类法参数(视图查询)
    if( $args['term_id'] ){
        $defaults['where']['term_map.term_id'] = DcWhereValue($args['term_id'], 'in');
        unset($args['term_id']);
    }
    //where参数合并
    if($args['where']){
        $args['where'] = DcArrayArgs($args['where'], $defaults['where']);
    }else{
        $args['where'] = $defaults['where'];
    }
    unset($defaults['where']);
    //返回结果
    return \daicuo\Info::result(\daicuo\Info::all(DcArrayArgs($args, $defaults)), DcEmpty($args['result'], 'array'));
}

/**************************************************兼容旧版本函数***************************************************/
function DcLoadConfig($file, $name = '', $range = ''){
    return DcConfigLoad($file, $name, $range);
}
function DcWhereByQuery($fields=[], $condition='eq', $requestParams=[]){
    return DcWhereQuery($fields, $condition, $requestParams);
}