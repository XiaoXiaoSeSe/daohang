<?php
namespace app\api\controller;

use app\common\controller\Api;

class Token extends Api
{
    // 初始化
    public function _initialize()
    {
        // 不需要登录的操作
        $this->auth['none_login'] = ['api/token/index','api/token/login'];
        // 继承上级
		parent::_initialize();
    }
    
    // 通过code换取Token?code=xxx
    public function index()
    {
        $code = str_replace(['%20',' '], '+', input('get.code/s'));
        
        $code = DcDesDecode($code, config('common.site_secret'));
        
        if(!$code){
            $this->error( lang('secret_error') );
        }
        
        //拆角CODE
        list($token, $expire, $time) = explode(',', $code);
        
        //临时CODE超时
        if( time()-$time > 7200){
            $this->error( lang('secret_error') );
        }
        
        //返回token
        $this->success( lang('success'), ['user_token'=>$token,'user_expire'=>$expire] );
    }
    
    /**
     * 登录获取Token(支持授权回调：api/tokin/login/?callbak=xxxx&state=1)
     * @return array
     */
    public function login()
    {
        if( $this->request->isPost() ){
            //验证码
            if( DcBool(config('common.site_captcha')) ){
                if(captcha_check(input('post.user_captcha')) == false){
                    $this->error( DcError(lang('user_captcha_error')) );
                }
            }
            //验证用户名密码
            $data = \daicuo\User::token_login(input('post.user_name/s'), input('post.user_pass/s'));
            if( $data ){
                //是否回跳
                $callbak = model('api/User','Loglic')->callBak($data, input('post.callbak/u',''), input('post.state/s'));
                if( $callbak ){
                    $this->redirect($callbak, 302);
                }
                $this->success( lang('success'), $data );
            }else{
                $this->error( DcError(\daicuo\User::getError()) );
            }
        }
        //GET请求
        $this->site['path_view'] = 'apps/api/view/token/';
        $this->assign($this->site);
        return $this->fetch();
	}
    
    // 刷新后的token信息/刷新授权时间
    public function refresh()
    {
        $data = \daicuo\User::token_update($this->site['user']['user_id'], $this->site['user']['user_token']);
        if($data){
            $this->success( lang('success'), $data );
        }
        $this->error( lang('error') );
    }
    
    // 删除Token
    public function delete()
    {
        if( \daicuo\User::token_delete($this->site['user']['user_id']) ){
            $this->success( lang('success') );
        }
        $this->error( lang('error') );
    }
    
    // 回跳处理
    private function callbak($callbak, $data, $state){
        if(!$callbak || !$data){
            return null;
        }
        if(!config('common.token_domain')){
            return null;
        }
        //白名单域名
        if( !in_array(DcDomain($callbak), explode(',',config('common.token_domain'))) ){
            return null;
        }
        //增加时间参数
        $data['time']   = time();
        //回跳网址拼装
        $query = [];
        $query['code']   = DcDesEncode(implode(',',$data), config('common.site_secret'));//加密CODE参数
        $query['state']  = DcHtml($state);//自定义回跳参数
        return $callbak.'?'.http_build_query($query);
    }
}