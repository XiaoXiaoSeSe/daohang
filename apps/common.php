<?php
error_reporting(E_ERROR);
/**************************************************数据库模型操作***************************************************/
/**
 * 普通数据转多对多关系,通常为post提交的一维数组数据(同一字段以数组形式多个值)
 * @version 1.2.0 首次引入
 * @param array $data 必需;待转化的数据;['trem_much_id'=>[1,2,3,4]]
 * @param string $fields 必需;需要转化的字段,多个用逗号分隔或传入数组;默认：空
 * @param string $prefix 必需;关联表名;默认：term_map
 * @return array $array 转化成TP入库的数据格式
 */
function DcDataToMuch($data, $fields='', $prefix='term_map'){
    if(empty($data) || empty($fields)){
        return $data;
    }
    if( is_string($fields) ){
        $fields = explode(',', $fields);
    }
    foreach($fields as $key=>$field){
        if( isset($data[$field]) ){
            foreach($data[$field] as $key2=>$value2){
                $data[$prefix][$key2][$field] = $value2;
            }
        }
        unset($data[$field]);
    }
    return $data;
}
/**
 * 普通数据转一对多关系,通常为post提交的一维数组数据
 * @version 1.2.0 首次引入
 * @param array $data 必需;待转化的数据
 * @param string $fields 必需;需要转化的字段,多个用逗号分隔或传入数组;默认：空
 * @param string $prefix 必需;关联表名;默认：user_meta
 * @return array $array 转化成TP入库的数据格式
 */
function DcDataToMany($data, $fields='', $prefix='user_meta'){
    if(empty($data) || empty($fields)){
        return $data;
    }
    if( is_string($fields) ){
        $fields = explode(',', $fields);
    }
    foreach($fields as $key=>$field){
        if( isset($data[$field]) ){
            $data[$prefix][$key][$prefix.'_key'] = $field;
            $data[$prefix][$key][$prefix.'_value'] = $data[$field];
        }
        unset($data[$field]);
    }
    return $data;
}
/**
 * 一对多关系转普通数据,通常为数据库查询后的数据
 * @version 1.2.0 首次引入
 * @param array $data 必需;待转化的数据
 * @param string $prefix 必需;待修改的关联表名(可以理解为修改器);默认：user_meta
 * @return array $array 转化后的数据/一维数组
 */
function DcManyToData($data, $prefix='user_meta'){
    if(empty($data)){
        return $data;
    }
    $data_meta = array();
    foreach($data[$prefix] as $value){
        $data_meta[$value[$prefix.'_key']] = $value[$prefix.'_value'];
    }
    unset($data[$prefix]);
    return array_merge($data, $data_meta);
}
/**
 * 模型别名唯一值处理
 * @version 1.2.0 首次引入
 * @param string $table 必需;待验证的表名
 * @param string $value 必需;待验证字段值
 * @param string $id 可选;主键ID;默认：0
 * @return string $string 不是唯一值时自动添加-**
 */
function DcSlugUnique($table, $value, $id=0){
    //空值是唯一
    if( empty($value) ){
        return uniqid();
    }
    //查询是否存在
    $count = db($table)->where(config('common.where_slug_unique'))->where([$table.'_id'=>['neq',$id]])->where($table.'_slug',[['eq',$value],['like',$value.'-%'],'or'])->fetchSql(false)->count();
    //还原为空
    config('common.where_slug_unique',[]);
    //已存在
    if($count){
        $value = $value.'-'.($count+1);
    }
    return $value;
}
/**
 * 根据query参数生成查询条件
 * @version 1.2.0 首次引入
 * @param array $fields 必需;白名单字段
 * @param string $condition 可选;关系,eq|neq|gt|lt|in;默认：eq
 * @param array $requestParams 可选;数组格式的地址栏参数;默认：空
 * @return array $array 用于TP查询的Where条件
 */
function DcWhereQuery($fields=[], $condition='eq', $requestParams=[]){
    //是否自动获取query参数
    if(!$requestParams){
        $requestParams = request()->param();
    }
    //空值过滤
    $query = array_filter($requestParams, function($value){
        if($value || $value=='0'){
            return true;
        }
        return false;
    });
    //字段过滤
    return DcWhereFilter($query, $fields, $condition);
}
/**
 * 根据字段过滤查询参数
 * @version 1.6.0 首次引入
 * @param array $args 必需;参数列表
 * @param array $fields 必需;白名单字段
 * @param string $condition 可选;关系(eq|neq|gt|lt|in|like);默认：eq
 * @param string $prefix 可选;KEY前缀;默认：空
 * @param string $suffix 可选;KEY后缀;默认：空
 * @return array $array 只返回字段中的条件语句
 */
function DcWhereFilter($args=[], $fields=[], $condition='eq', $prefix='', $suffix=''){
    $where = array();
    foreach($args as $key=>$value){
        if( in_array($key, $fields) ){
            $where[$prefix.$key.$suffix] = DcWhereValue($value, $condition);
        }
    }
    return $where;
}
/**
 * 格式化Where查询参数为数组
 * @version 1.6.0 首次引入
 * @param mixed $value 必需;条件值(string|array);默认：空
 * @return array $array 返回TP的数组WHERE条件
 */
function DcWhereValue($value='', $compare='eq'){
    if(is_string($value)){
        return [$compare, DcHtml($value)];
    }
    if( DcIsArray($value, true) ){
        return $value;//值为二维数组时，['a'=>1,'b'=>2]
    }else{
        return [$value[0], $value[1]];//值为普通数组时，['like','%keyword%']
    }
}
/**
 * 将数据添加至数据库
 * @version 1.1.0 首次引入
 * @param string $name 资源地址(common/Nav)
 * @param array $data 待写入数据(关联写入则包含二维数组)
 * @param array $relationTables 关联表/多个用,分隔/不需要表前缀/user_meta
 * @return int $int 返回自增ID或0
 */
function DcDbSave($name, $data=[], $relationTables=''){
    //实例化模型
    $model = model($name);
    //获取主键
    $pk = $model->getPk();
    //是否需要关联新增
    if($relationTables){
        //基础数据
        $dataBase = [];
        $tableBase = $model->getTableFields();
        foreach($tableBase as $key=>$value){
            if( isset($data[$value]) ){
                $dataBase[$value] = $data[$value];
            }
        }
        //$model->allowField(true)->save($dataBase);
        $model->data($dataBase, true)->allowField(true)->isUpdate(false)->save();
        //关联数据表
        if(is_string($relationTables)){
            $relationTables = explode(',', $relationTables);
        }
        foreach($relationTables as $key=>$tableName){
            if($relationData = $data[$tableName]){
                //驼峰转化
                $relationTable = camelize($tableName);
                //关联新增方式
                if( DcIsArray($relationData, true) ){
                    $model->$relationTable()->saveAll($relationData);
                }else{
                    $model->$relationTable()->save($relationData);
                }
            }
        }
    }else{
        $model->data($data, true)->allowField(true)->isUpdate(false)->save();
    }
    return $model->$pk;
}
/**
 * 删除一条数据
 * @version 1.1.0 首次引入
 * @param string $name 资源地址(common/Nav)
 * @param array $where 查询条件
 * @param array $relationTables 关联表/多个用,分隔/不需要表前缀/如:user_meta
 * @return null|obj 不为空时返回修改后的obj
 */
function DcDbDelete($name, $where=[], $relationTables=''){
    $result = array();
    //模型实例化
    $model = model($name);
    //获取模型主键
    $modelPk = $model->getPk();
    //获取数据
    $data = $model->with($relationTables)->where($where)->find();
    if( is_null($data) ){
        return null;
    }
    //删除基础数据
    $result[0] = $data->delete();
    //是否需要关联删除
    if($relationTables){
        //$data->together($relationTables)->delete();//只能关联删除一对一
        //关联数据表
        if(is_string($relationTables)){
            $relationTables = explode(',', $relationTables);
        }
        //删除关联数据
        foreach($relationTables as $key=>$tableName){
            $tableName = camelize($tableName);//驼峰转化
            array_push($result, $data->$tableName()->delete());//追加删除结果
        }
    }
    //缓存处理
    if( (config('cache.expire_detail') > 0) || (config('cache.expire_detail')===0) ){
        if( !is_null($info) ){
            DcCacheTag($modelPk.'_'.$data->$modelPk, 'clear', 'clear');
            //DcCacheTag($name.'/Item', 'clear', 'clear');
        }
    }
    //定义删除结果
    $data->RESULT = $result;
    //返回数据
    return $data;
}
/**
 * 修改一条数据
 * @version 1.6.0 优化附加删除字段
 * @version 1.1.0 首次引入
 * @param string $name 资源地址(common/Nav)
 * @param array $where 更新条件
 * @param array $data 待写入数据(关联写入则包含二维数组)
 * @param array $relationTables 关联表/多个用,分隔/不需要表前缀/user_meta
 * @param mixed $deleteFields 一对多关联更新时附加的删除条件(array|false),如：['term_meta_key']
 * @return obj|null 不为空时返回修改后的obj
 */
function DcDbUpdate($name, $where=[], $data=[], $relationTables='', $deleteFields='default'){
    //实例化模型
    $model = model($name);
    //获取模型主键
    $modelPk = $model->getPk();
    //数据主键过滤
    unset($data[$modelPk]);
    //数据查询
    $info = $model->get($where);
    if( is_null($info) ){
        return null;
    }
    //基础数据表强制更新（即使有主键ID）
    $info->allowField(true)->isUpdate()->save($data);
    //关联数据表更新
    if($relationTables){
        //关联数据表
        if(is_string($relationTables)){
            $relationTables = explode(',', $relationTables);
        }
        //关联操作
        foreach($relationTables as $key=>$tableName){
            //当前关联表数据
            $tableData    = $data[$tableName];
            //当前关联表名驼峰样式
            $tableNameCame = camelize($tableName);
            //关联数据验证
            if( empty($tableData) ){
                continue;
            }
            /**************************************************
            ** 采用哪种关联模式(由data关联表的数据决定,二维数组则是一对多)
            ** 多维数组采用一对多关联,先批量删除对应的值后再批量新增（model里定义hasMany）
            ** 普通数组采用一对一关联更新（model里定义hasOne）
            ********************************************************/
            if( DcIsArray($tableData, true) ){
                //默认附加删除条件xxx_key字段
                if($deleteFields == 'default'){
                    $deleteFields = [$tableName.'_key'];
                }
                
                //动态生成删除条件(按关联字段值+附加条件删除)
                $where = array();
                foreach($deleteFields as $deleteKey=>$deleteField){
                    foreach($tableData as $keyOne=>$valueOne){
                        if(isset($valueOne[$deleteField])){
                            $where[$deleteField][] = ['eq', $valueOne[$deleteField]];
                        }
                    }
                    if($where[$deleteField]){
                        array_push($where[$deleteField],'or');
                    }
                }
                //dump($info->$tableNameCame()->fetchSql(true)->where($where)->delete());
                $info->$tableNameCame()->where($where)->delete();
                
                //批量增加关联数据
                $info->$tableNameCame()->saveAll($tableData);
            }else{
                if( is_null($info->$tableNameCame) ){
                    $info->$tableNameCame()->save($tableData);//无关联数据时新增
                }else{
                    $info->$tableNameCame->isUpdate()->save($tableData);//已有关联数据时直接修改
                }
            }
            
        }
    }
    //缓存处理
    if( (config('cache.expire_detail') > 0) || (config('cache.expire_detail')===0) ){
        if( !is_null($info) ){
            DcCacheTag($modelPk.'_'.$info->$modelPk, 'clear');
            //DcCacheTag($name.'/Item', 'clear');
        }
    }
    return $info;
}
/**
 * 查询单条数据
 * @version 1.1.0 首次引入
 * @param string $name 资源地址(common/Nav)
 * @param array $params 查询参数
 * @return obj|null 不为空时返回obj
 */
function DcDbFind($name, $params){
    $model = model($name);
    $modelPk = $model->getPk();
    $cacheExpire = config('cache.expire_detail');
    //
    $args = array();
    $args['field'] = '*';
    $args['alias'] = '';
    $args['where'] = [];
    $args['whereOr'] = [];
    $args['wheretime'] = '';
    //
    $args['join'] = [];
    $args['union'] = [];
    $args['view'] = [];
    //
    $args['relation'] = '';
    $args['with'] = [];
    $args['bind'] = '';
    //
    $args['sort'] = '';
    $args['order'] = '';
    //
    $args['fetchSql'] = false;
    $args['cache'] = true;
    $args['cacheKey'] = '';
    //合并参数
    if($params){
        $args = array_merge($args, $params);
    }
    //缓存管理
    if($args['cache'] && (false == $args['fetchSql']) ){
        if( ($cacheExpire > 0) || ($cacheExpire===0) ){
            //缓存前缀
            $args['cacheKey'] = DcCacheKey($args);
            //无缓存的时候返回false
            if( $info = DcCache($args['cacheKey']) ){
                return $info;
            }
        }
    }
    //非关联条件
    if($args['fetchSql']){
        $model->fetchSql($args['fetchSql']);
    }
    if($args['field']){
        $model->field($args['field']);
    }
    //2021.06.12增加别名
    if($args['alias']){
        $model->alias($args['alias']);
    }
    if($args['where']){
        if( isset($args['where'][0]) ){
            foreach($args['where'] as $keyWhere=>$valueWhere){
                $model->where($valueWhere);
            }
        }else{
            $model->where($args['where']);
        }
    }
    if($args['whereOr']){
        if( isset($args['whereOr'][0]) ){
            foreach($args['whereOr'] as $keyWhereOr=>$valueWhereOr){
                $model->whereOr($valueWhereOr);
            }
        }else{
            $model->whereOr($args['whereOr']);
        }
    }
    if($args['wheretime']){
        $model->wheretime($args['wheretime']);
    }
    if($args['join']){
        $model->join($args['join']);
    }
    if($args['union']){
        $model->union($args['union']);
    }
    if($args['view']){
        $model->view($args['view']);
    }
    if($args['relation']){
        $model->relation($args['relation']);
    }
    if($args['with']){
        $model->with($args['with']);
    }
    if($args['bind']){
        $model->bind($args['bind']);
    }
    if($args['orderRaw']){
        $model->orderRaw($args['orderRaw']);
    }
    if( is_array($args['order']) ){
        $model->order($args['order']);
    }else{
        if($args['sort'] && $args['order'] ){
            $model->order($args['sort'].' '.$args['order']);
        }
    }
    //查询数据库
    $info = $model->find();
    //无结果
    if( is_null($info) ){
        return null;
    }
    //sql语句生成
    if( is_string($info) ){
        return $info;
    }
    //缓存写入
    if($args['cache'] && (false == $args['fetchSql']) && $args['cacheKey'] ){
        if( ($cacheExpire > 0) || ($cacheExpire===0) ){
            DcCacheTag($modelPk.'_'.$info->$modelPk, $args['cacheKey'], $info, $cacheExpire);
        }
    }
    return $info;
}
/**
 * 查询多条数据
 * @version 1.1.0 首次引入
 * @param string $name 资源地址(common/Nav)
 * @param array $params 查询参数
 * @return obj|null|string 分页模询统一返回obj,sql语句为对象属性|为空时null不为空时obj,sql语句返回字符串
 */
function DcDbSelect($name, $params){
    $model = model($name);
    $cacheExpire = config('cache.expire_item');
    //
    $args = array();
    $args['field'] = '*';
    $args['alias'] = '';
    $args['where'] = [];
    $args['whereOr'] = [];
    $args['wheretime'] = '';
    //
    $args['group'] = '';
    $args['having'] = '';
    $args['join'] = [];
    $args['union'] = [];
    $args['view'] = [];
    //
    $args['hasWhere'] = [];
    $args['relation'] = '';
    $args['with'] = [];
    $args['bind'] = '';
    //
    $args['limit'] = 0;
    $args['page'] = 0;
    $args['sort'] = '';
    $args['order'] = '';
    $args['force'] = '';
    //
    $args['lock'] = false;
    $args['distinct'] = false;
    $args['paginate'] = [];
    //
    $args['cache'] = true;
    $args['fetchSql'] = false;
    //合并参数
    if($params){
        $args = array_merge($args, $params);
    }
    //缓存管理
    if($args['cache'] && (false == $args['fetchSql']) ){
        if( ($cacheExpire > 0) || ($cacheExpire===0) ){
            $args['cacheKey'] = DcCacheKey($args);
            if( $list = DcCache($args['cacheKey']) ){
                return $list;
            }
        }
    }
    //取消hasWhere关联条件(因为只能一个表 有局恨性)
    if($args['field']){
        $model->field($args['field']);
    }
    //2021.06.12增加别名
    if($args['alias']){
        $model->alias($args['alias']);
    }
    //2021.06.12多维数组
    if($args['where']){
        if( isset($args['where'][0]) ){
            foreach($args['where'] as $keyWhere=>$valueWhere){
                $model->where($valueWhere);
            }
        }else{
            $model->where($args['where']);
        }
    }
    //2021.06.12多维数组
    if($args['whereOr']){
        if( isset($args['whereOr'][0]) ){
            foreach($args['whereOr'] as $keyWhereOr=>$valueWhereOr){
                $model->whereOr($valueWhereOr);
            }
        }else{
            $model->whereOr($args['whereOr']);
        }
    }
    if($args['wheretime']){
        $model->wheretime($args['wheretime']);
    }
    if($args['group']){
        $model->group($args['group']);
    }
    if($args['having']){
        $model->having($args['having']);
    }
    if($args['join']){
        $model->join($args['join']);
    }
    if($args['union']){
        $model->union($args['union']);
    }
    if($args['view']){
        $model->view($args['view']);
    }
    if($args['relation']){
        $model->relation($args['relation']);
    }
    if($args['with']){
        $model->with($args['with']);
    }
    if($args['bind']){
        $model->bind($args['bind']);
    }
    if($args['limit']){
        $model->limit($args['limit']);
    }
    if($args['page']){
        $model->page($args['page']);
    }
    if($args['orderRaw']){
        $model->orderRaw($args['orderRaw']);
    }
    if( is_array($args['order']) ){
        $model->order($args['order']);
    }else{
        if($args['sort'] && $args['order'] ){
            $model->order($args['sort'].' '.$args['order']);
        }
    }
    if($args['force']){
        $model->force($args['force']);
    }
    if($args['lock']){
        $model->lock($args['lock']);
    }
    if($args['distinct']){
        $model->distinct($args['distinct']);
    }
    if($args['fetchSql']){
        $model->fetchSql($args['fetchSql']);
    }
    if($args['paginate']){
        if($args['paginate'][1] === true){
            $list = $model->paginate($args['paginate'][0], true);//简洁分页模式
        }else{
           $list = $model->paginate($args['paginate']); 
        }
    }else{
        $list = $model->select();
        if( is_string($list) ){
            return $list;
        }
    }
    if($list->isEmpty()){
        return null;
    }
    //缓存写入
    if($args['cache'] && (false == $args['fetchSql']) && $args['cacheKey'] ){
        if( ($cacheExpire > 0) || ($cacheExpire===0) ){
            DcCacheTag($name.'/Item', $args['cacheKey'], $list, $cacheExpire);
        }
    }
    return $list;
}
/**
 * 模型的get方法查询单条数据
 * @version 1.1.0 首次引入
 * @param string $name 资源地址(common/Nav)
 * @param array $where 查询条件
 * @param array $relationTables 关联表/多个用,分隔/不需要表前缀/如:user_meta
 * @return obj|null 不为空时返回obj
 */
function DcDbGet($name, $where=[], $relationTables=''){
    $model = model($name);
    //是否需要关联预载入查询
    if($relationTables){
        return $model->get($where, $relationTables);
    }else{
        return $model->get($where);
    }
}
/**
 * 模型的all方法查询多条数据
 * @version 1.1.0 首次引入
 * @param string $name 资源地址(common/Nav)
 * @param array $where 查询条件
 * @param array $relationTables 关联表/多个用,分隔/不需要表前缀/如:user_meta
 * @return obj|null 不为空时返回obj
 */
function DcDbAll($name, $where=[], $relationTables=''){
    $model = model($name);
    //是否需要关联预载入查询
    if($relationTables){
        //查询数据
        return $model->all($where, $relationTables);
    }else{
        return $model->all($where);
    }
}
/*---------------------------------------------数据库基础操作------------------------------------------------------------*/
/**
 * 添加一条数据
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $data 数据
 * @return int $int 返回记录数
 */
function dbInsert($name='', $data){
    return model($name)->allowField(true)->data($data)->save();//返回的是影响的记录数
    //return db($name)->insertGetId($data);//返回添加数据的自增主键
    //return db($name)->insert($data);返回添加成功的条数
}
/**
 * 添加多条数据
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $data 数据、二维数组
 * @return mixed $mixed 返回数据集(obj|null)
 */
function dbInsertAll($name='', $list){
    $status = model($name)->allowField(true)->saveAll($list, false);
    if($status->isEmpty()){
        return null;
    }
    return $status;
    //返回的是包含新增模型（带自增ID）的数据集（数组） 当数据中存在主键的时候会认为是更新操作 加上false强制新增
    //return db($name)->insertAll($data);
}
/**
 * 删除数据
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $where 条件
 * @return int $int 返回影响数据的条数，没有删除返回0
 */
function dbDelete($name='',$where){
    return model($name)->where($where)->delete();
}
/**
 * 更新数据
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $where 条件
 * @param array $data 数据
 * @return int $int 影响条数
 */
function dbUpdate($name='', $where=[], $data=[]){
    return model($name)->allowField(true)->save($data, $where);
    //return db($table)->where($where)->update($data);
}
/**
 * 批量更新数据(批量更新仅能根据主键值进行更新，其它情况请使用foreach遍历更新)
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $list 数据、二维数组(如果不包含主键则需要复合主键才可以成功)
 * @return int $int 影响条数
 */
function dbUpdateAll($name='', $list=[]){
    return model($name)->allowField(true)->isUpdate()->saveAll($list);//强制更新操作
}
/**
 * 更新某个字段的值
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $where 条件 
 * @param string field 字段名
 * @param string value 字段值
 * @return int $int 返回影响数据的条数，没修改任何数据字段返回 0
 */
function dbUpdateField($name='',$where=[], $field='', $value=''){
    return model($name)->where($where)->setField($field, $value);
}
/**
 * 自减某字段的值
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $where 条件
 * @param string field 字段名
 * @param string num 递增值
 * @param string time 延迟更新时长
 * @return int $int 影响数据的条数
 */
function dbUpdateDec($name='',$where=[], $field='', $num=1, $time=0){
    return model($name)->where($where)->setDec($field, $num, $time);
}
/**
 * 自增某字段的值
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $where 条件  
 * @param string field 字段名
 * @param string num 递增值
 * @param string time 延迟更新时长
 * @return int $int 影响数据的条数
 */
function dbUpdateInc($name='',$where=[], $field='', $num=1, $time=0){
    return model($name)->where($where)->setInc($field, $num, $time);
}
/**
 * 写入多条数据自动判断新增与修改,当数据中存在主键的时候会认为是更新操作,否则为新增
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $list 数据、二维数组
 * @return mixed $mixed 新增时为数据集,更新时为影响数据的条数（obj|int）
 */
function dbWriteAuto($name='', $list=[]){
    return model($name)->allowField(true)->saveAll($list);
    //返回的是包含新增模型（带自增ID）的数据集（数组） 当数据中存在主键的时候会认为是更新操作 否则为新增
}
/**
 * 数据查询单个格式：[模块/]控制器
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $where 查询条件
 * @param string $whereOr 查询条件
 * @param bool $fetchSql 显示查询语句
 * @return obj $obj 数据集|null
 */
function dbFind($name='', $where=[], $whereOr='', $cache=[], $fetchSql=false){
    if(!$name){
        return false;
    }
    //缓存参数初始化
    if(is_array($cache)){
        $cache = array_merge(['key'=>false,'time'=>false,'tag'=>false], $cache);
    }else{
        $cache = ['key'=>false,'time'=>false,'tag'=>false];
    }
    //缓存条件处理
    if($cache['key'] && $cache['time']){
        return model($name)->where($where)->whereOr($whereOr)->fetchSql($fetchSql)->cache($cache['key'],$cache['time'])->find();
    }else{
        return model($name)->where($where)->whereOr($whereOr)->fetchSql($fetchSql)->find();
    }
}
/**
 * 根据条件快捷查询某个字段的值
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $where 必需;查询条件;默认：空
 * @param string $field 必需;待查询的字段值;默认：空
 * @return mixed $mixed 获取的数据或null
 */
function dbFindValue($name='',$where=[], $field=''){
    if(!$name || !$where || !$field){
        return null;
    }
    return model($name)->where($where)->value($field);
}
/**
 * 数据查询多个
 * @version 1.0.0 首次引入
 * @param string $name Model名称
 * @param array $where 查询条件
 * @param array $params 查询参数
 * @return obj $obj 数据集|null
 */
function dbSelect($name='', $where=[], $params=[], $cache=[]){
    if(!$name){
        return false;
    }
    //缓存参数初始化
    if(is_array($cache)){
        $cache = array_merge(['key'=>false,'time'=>false,'tag'=>false], $cache);
    }else{
        $cache = ['key'=>false,'time'=>false,'tag'=>false];
    }
    //参数初始化
    $params = array_merge(['fetchSql'=>false,'field'=>'*'], $params);
    //分页
    if($params['page']){
        return model($name)->field($params['field'])->where($where)->order($params['sort'].' '.$params['order'])->fetchSql($params['fetchSql'])->cache($cache['key'],$cache['time'],$cache['tag'])->paginate($params['paginate']);
    }else{
        return model($name)->field($params['field'])->where($where)->limit($params['limit'])->order($params['sort'].' '.$params['order'])->fetchSql($params['fetchSql'])->cache($cache['key'],$cache['time'],$cache['tag'])->select();
    }
}

/**************************************************扩展函数－驼峰**************************************************/
/**
* 下划线转驼峰
* @version 1.4.0 首次引入
* @param string $uncamelized_words 下划线样式的字符串
* @param string $separator 分隔符/默认'_'
* @return string 驼峰样式的字符串
* step1.原字符串转小写,原字符串中的分隔符用空格替换,在字符串开头加上分隔符
* step2.将字符串中每个单词的首字母转换为大写,再去空格,去字符串首部附加的分隔符.
*/
function camelize($uncamelized_words, $separator='_'){
    $uncamelized_words = $separator. str_replace($separator, " ", strtolower($uncamelized_words));
    return ltrim(str_replace(" ", "", ucwords($uncamelized_words)), $separator );
}
/**
* 驼峰命名转下划线命名
* @version 1.4.0 首次引入
* @param string $camelCaps 驼峰命名字符串
* @param string $separator 分隔符/默认'_'
* @return string 小写和大写紧挨一起的地方,加上分隔符,然后全部转小写
*/
function uncamelize($camelCaps, $separator='_'){
    return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $camelCaps));
}

/**************************************************扩展函数－文件与目录***************************************************/
/**
 * 读取文件
 * @param string $path 完整文件路径名
 * @return bool
 */
function read_file($path){
    $file = new \files\File();
    return $file->read($path);
}
/**
 * 写入文件
 * @param string $filename 完整文件路径名
 * @param string $data 要写入文件的内容 
 * @return bool
 */
function write_file($filename='', $data=''){
    $file = new \files\File();
    return $file->write($filename, $data);
}
/**
 * 数组保存到文件
 * @param string $filename 完整文件路径名
 * @param string $dataArray 数组
 * @return bool
 */
function write_array($filename, $dataArray=''){
    $file = new \files\File();
    return $file->write_array($filename, $dataArray);
}
/**
 * 递归创件目录
 * @param string $dirs 完整文件路径名
 * @return bool
 */
function mkdir_ss($dirs) {
    $file = new \files\File();
    return $file->d_create($dirs);
}
/**
 * 列出目录下单层所有文件夹名
 * @param string $dir 完整文件夹路径
 * @return array
 */
function glob_basename($path = 'apps/index/theme/') {
    $list = glob($path.'*');
    foreach ($list as $i=>$file){
        $dir[] = basename($file);
    }    
    return $dir;
}

/**************************************************无限层级分类***************************************************/
/**
 * 获取指定分类的所有子集(递归法)
 * @param array $categorys 数组列表
 * @param int $catId 主键ID值
 * @param int $level 层级记录数
 * @param int $pk 主键名称
 * @param int $pid 父级名称
 * @return array;
 */
function get_childs($categorys, $catId=0, $level=1, $pk='term_id', $pid='term_parent'){
    $subs = array();
    foreach($categorys as $item){
        if($item[$pid] == $catId){
            $item['level'] = $level;
            $subs[] = $item;
            $subs = array_merge($subs, get_childs($categorys, $item[$pk], $level+1, $pk, $pid) );
        }
    }
    return $subs;
}
/**
 * 获取某一个子类的所有父级(递归法)
 * @param array $categorys 数组列表
 * @param int $parentId 父级ID值
 * @param int $pk 主键名称
 * @param int $pid 父级名称
 * @return mixed null|array;
 */
function get_parents($categorys, $parentId, $pk='term_id', $pid='term_parent'){
    $tree = array();
    foreach($categorys as $item){
        if($item[$pk] == $parentId){
            $tree[] = $item;
            $tree = array_merge($tree, get_parents($categorys, $item[$pid], $pk, $pid) );
        }
    }
    return $tree;
}
/**
 * 将list_to_tree的树还原成带层维数的数据列表/用于表格展示
 * @param  array $tree  原来的树
 * @param  string $pkName 要添加符号的键名
 * @param  string $level 记录无限层级关系 
 * @param  string $child 孩子节点的键
 * @param  array  $list  过渡用的中间数组，
 * @return array 返回排过序的列表数组
 */
function tree_to_level($tree, $pkName='', $level=0, $child='_child', &$list = array()){
    if(is_array($tree)) {
        $icon   = '';
        if ($level > 0) {
            $icon = '|';
            for ($i=0; $i < $level; $i++) {
                //$icon .= '&nbsp;&nbsp;&nbsp;';
                $icon .= '─ ';
            }
            //$icon .= '├&nbsp;';
        }
        $refer = array();
        foreach ($tree as $value) {
            $reffer = $value;
            if($pkName){
                $reffer[$pkName] = $icon.$reffer[$pkName];
            }
            if(isset($reffer[$child])){
                unset($reffer[$child]);
                $list[] = $reffer;
                tree_to_level($value[$child], $pkName, $level+1, $child, $list);
            }else{
                $list[] = $reffer;
            }
        }
    }
    return $list;
}
/**
 * 将原生数据集生成options的选项
 * @param array $list 原生数据
 * @param intval $pid 父级ID
 * @param intval $sid 选中ID
 * @param array $did 禁止选择
 * @param int $level 当前层数
 * @param array $config 初始配置  
 * @return string 返回格式化后的option选项
 */
function list_to_option($list = [], $pid = 0, $sid = 0, $did = [], $level = 0, $config=[]){
    $config_ = array_merge(['id'=>'op_id','pid'=>'nav_parent','name'=>'nav_text'], $config);
    $tree = new \daicuo\Tree($config_);
    return $tree->toOptions($tree->toTree($list, $pid, 0, $level), $sid);
}

/**************************************************ThinkPHP扩展函数库***************************************************/

/**
 * 判断邮箱
 * @param string $str 要验证的邮箱地址
 * @return bool
 */
function is_email($str) {
    return preg_match("/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/", $str);
}
/**
 * 判断手机号
 * @param string $num 要验证的手机号
 * @return bool
 */
function is_mobile($num) {
    return preg_match("/^1(3|4|5|6|7|8|9)\d{9}$/", $num);
}
/**
 * 判断用户名
 * 用户名支持中文、字母、数字、下划线，但必须以中文或字母开头，长度3-20个字符
 * @param string $str 要验证的字符串
 * @return bool
 */
function is_username($str) {
    return preg_match("/^[\x80-\xffA-Za-z]{1,1}[\x80-\xff_A-Za-z0-9]{2,19}+$/", $str);
}
/**
 * 判断数据不是JSON格式
 * @param string $str 要验证的字符串
 * @return bool
 */
function is_not_json($str){  
    return is_null(json_decode($str));
}
/**
 * 在数据列表中搜索
 * @param array $list 数据列表
 * @param mixed $condition 查询条件
 * 支持 array('name'=>$value) 或者 name=$value
 * @return array
 */
function list_search($list,$condition) {
    if(is_string($condition))
        parse_str($condition,$condition);
    // 返回的结果集合
    $resultSet = array();
    foreach ($list as $key=>$data){
        $find   =   false;
        foreach ($condition as $field=>$value){
            if(isset($data[$field])) {
                if(0 === strpos($value,'/')) {
                    $find   =   preg_match($value,$data[$field]);
                }elseif($data[$field]==$value){
                    $find = true;
                }
            }
        }
        if($find)
            $resultSet[]     =   &$list[$key];
    }
    return $resultSet;
}
/**
 * 对查询结果集进行排序
 * @param array $list 查询结果
 * @param string $field 排序的字段名
 * @param array $sortby 排序类型 (asc正向排序 desc逆向排序 nat自然排序)
 * @return array
 */
function list_sort_by($list,$field, $sortby='asc') {
   if(is_array($list)){
       $refer = $resultSet = array();
       foreach ($list as $i => $data)
           $refer[$i] = &$data[$field];
       switch ($sortby) {
           case 'asc': // 正向排序
                asort($refer);
                break;
           case 'desc':// 逆向排序
                arsort($refer);
                break;
           case 'nat': // 自然排序
                natcasesort($refer);
                break;
       }
       foreach ( $refer as $key=> $val)
           $resultSet[] = &$list[$key];
       return $resultSet;
   }
   return false;
}
/*** 把返回的数据集转换成Tree
 * @param array $list 要转换的数据集
 * @param string $pid parent标记字段
 * @param string $level level标记字段
 * @return array 
 */
function list_to_tree($list, $pk='id', $pid = 'pid', $child = '_child', $root=0)
{
    // 创建Tree
    $tree = array();
    if(is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] =& $list[$key];
            }else{
                if (isset($refer[$parentId])) {
                    $parent =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
    }
    return $tree;
}
/**
 * 将list_to_tree的树还原成列表
 * @param  array $tree  原来的树
 * @param  string $child 孩子节点的键
 * @param  string $order 排序显示的键，一般是主键 升序排列
 * @param  array  $list  过渡用的中间数组，
 * @return array 返回排过序的列表数组
 */
function tree_to_list($tree, $child = '_child', $order='id', &$list = array()){
    if(is_array($tree)) {
        $refer = array();
        foreach ($tree as $key => $value) {
            $reffer = $value;
            if(isset($reffer[$child])){
                unset($reffer[$child]);
                tree_to_list($value[$child], $child, $order, $list);
            }
            $list[] = $reffer;
        }
        $list = list_sort_by($list, $order, 'asc');
    }
    return $list;
}
/**
* XSS漏洞过滤
* @param string $val 待验证的字符串
* @return string 去掉敏感信息的字符串
*/
function remove_xss($val) {
   $val = preg_replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '', $val);
   $search = 'abcdefghijklmnopqrstuvwxyz';
   $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $search .= '1234567890!@#$%^&*()';
   $search .= '~`";:?+/={}[]-_|\'\\';
   for ($i = 0; $i < strlen($search); $i++) {
      $val = preg_replace('/(&#[xX]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val); // with a ;
      $val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val); // with a ;
   }
   $ra1 = array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
   $ra2 = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
   $ra = array_merge($ra1, $ra2);
   $found = true; // keep replacing as long as the previous round replaced something
   while ($found == true) {
      $val_before = $val;
      for ($i = 0; $i < sizeof($ra); $i++) {
         $pattern = '/';
         for ($j = 0; $j < strlen($ra[$i]); $j++) {
            if ($j > 0) {
               $pattern .= '(';
               $pattern .= '(&#[xX]0{0,8}([9ab]);)';
               $pattern .= '|';
               $pattern .= '|(&#0{0,8}([9|10|13]);)';
               $pattern .= ')*';
            }
            $pattern .= $ra[$i][$j];
         }
         $pattern .= '/i';
         $replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2); // add in <> to nerf the tag
         $val = preg_replace($pattern, $replacement, $val); // filter out the hex tags
         if ($val_before == $val) {
            // no replacements were made, so exit the loop
            $found = false;
         }
      }
   }
   return $val;
}
if(!function_exists('array_column')){
    /*
     * 适用于 PHP 5.4 更早版本的 array_column() 函数
     * @param array $input 原始数组
     * @param string|integer|null $column_key 键名
     * @param string|integer $index_key 原始数组中作为结果数组键名的键名
     * @return null|array|false
    */
    function array_column($input, $column_key, $index_key=''){
        if(!is_array($input)) return;
        $results=array();
        if($column_key===null){
            if(!is_string($index_key)&&!is_int($index_key)) return false;
            foreach($input as $_v){
                    if(array_key_exists($index_key,$_v)){
                            $results[$_v[$index_key]]=$_v;
                    }
            }
            if(empty($results)) $results=$input;
        }else if(!is_string($column_key)&&!is_int($column_key)){
            return false;
        }else{
            if(!is_string($index_key)&&!is_int($index_key)) return false;                        
            if($index_key===''){
                foreach($input as $_v){
                    if(is_array($_v)&&array_key_exists($column_key,$_v)){
                        $results[]=$_v[$column_key];
                    }
                }                                
            }else{
                foreach($input as $_v){
                    if(is_array($_v)&&array_key_exists($column_key,$_v)&&array_key_exists($index_key,$_v)){
                        $results[$_v[$index_key]]=$_v[$column_key];
                    }
                }
            }

        }
        return $results;
    }
}