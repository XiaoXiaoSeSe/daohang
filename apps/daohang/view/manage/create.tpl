{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>添加分类导航－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  添加分类导航
</h6>
<!-- -->
{:DcBuildForm([
    'name'     => 'daohang_create',
    'class'    => 'bg-white form-create py-2',
    'action'   => DcUrlAddon(['module'=>'daohang','controll'=>'manage','action'=>'save']),
    'method'   => 'post',
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => '',
    'ajax'     => true,
    'data'     => '',
    'items'    => DcFormItems($items),
])}
{/block}