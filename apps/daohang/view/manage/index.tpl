{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>分类导航管理－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="header_addon"}
<script src="{$domain}{$path_root}{$path_addon}view/theme.js?{:config('daicuo.version')}"></script>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  分类导航管理
</h6>
<!-- -->
<form action="{:DcUrlAddon(['module'=>'daohang','controll'=>'manage','action'=>'delete'])}" method="post" data-toggle="form">
<div class="toolbar mb-2" id="toolbar">
  <a class="btn btn-sm btn-light border" href="javascript:;" data-toggle="reload">
    <i class="fa fa-refresh fa-fw"></i> {:lang('refresh')}
  </a>
  <a class="btn btn-sm btn-outline-purple" href="{:DcUrlAddon(['module'=>'daohang','controll'=>'manage','action'=>'create'])}">
    <i class="fa fa-plus fa-fw"></i> {:lang('create')}
  </a>
  <button class="btn btn-sm btn-outline-danger" type="submit" data-toggle="delete">
    <i class="fa fa-trash"></i> {:lang('delete')}
  </button>
</div>
{:DcBuildTable([
    'data-escape'             => false,
    'data-toggle'             => 'bootstrap-table',
    'data-url'                => DcUrlAddon(['module'=>'daohang','controll'=>'manage','action'=>'index']),
    'data-url-sort'           => '',
    'data-url-preview'        => DcUrlAddon(['module'=>'daohang','controll'=>'manage','action'=>'preview','id'=>'']),
    'data-url-edit'           => DcUrlAddon(['module'=>'daohang','controll'=>'manage','action'=>'edit','id'=>'']),
    'data-url-delete'         => DcUrlAddon(['module'=>'daohang','controll'=>'manage','action'=>'delete','id'=>'']),
    'data-buttons-prefix'     => 'btn',
    'data-buttons-class'      => 'purple',
    'data-buttons-align'      => 'none float-md-right',
    'data-icon-size'          => 'sm',
    
    'data-toolbar'            => '.toolbar',
    'data-toolbar-align'      => 'none float-md-left',
    'data-buttons-align'      => 'none',
    'data-search-align'       => 'none float-md-right',
    'data-search'             => 'true',
    'data-show-search-button' => 'true',
    'data-show-refresh'       => 'false',
    'data-show-toggle'        => 'false',
    'data-show-fullscreen'    => 'false',
    'data-smart-display'      => 'false',
    
    'data-unique-id'          => 'info_id',
    'data-id-field'           => 'info_id',
    'data-select-item-name'   => 'id[]',
    'data-query-params-type'  => 'params',
    'data-query-params'       => 'daicuo.table.query',
    'data-sort-name'          => 'info_id',
    'data-sort-order'         => 'desc',
    'data-sort-class'         => 'table-active',
    'data-sort-stable'        => 'true',
    
    'data-side-pagination'    => 'server',
    'data-total-field'        => 'total',
    'data-data-field'         => 'data',
    'data-pagination'         => 'true',
    
    'data-page-number'        => $page,
    'data-page-size'          => '20',
    'data-page-list'          => '[20, 50, 100]',
    
    'columns'=>[
        [
            'data-checkbox'   => 'true',
        ],
        [
            'data-field'        => 'info_id',
            'data-title'        => 'id',
            'data-sortable'     => 'true',
            'data-sort-name'    => 'info_id',
            'data-sort-order'   => 'desc',
            //'data-width'      => '30',
            //'data-width-unit' => 'px',
            'data-class'        => '',
            'data-align'        => 'center',
            'data-valign'       => 'middle',
            'data-halign'       => 'center',
            'data-falign'       => 'center',
            'data-visible'      => 'true',
            //'data-formatter'  => '',
            //'data-footer-formatter'=>'',
        ],
        [
            'data-field'        => 'info_name',
            'data-title'        => lang('info_name'),
            'data-escape'       => true,
            'data-class'        => 'text-wrap',
        ],
        [
            'data-field'        => 'url_web',
            'data-title'        => lang('url_web'),
            'data-align'        => 'left',
            'data-halign'       => 'center',
            'data-escape'       => true,
        ],
        [
            'data-field'        => 'info_create_time',
            'data-title'        => lang('info_create_time'),
            'data-align'        => 'center',
            'data-halign'       => 'center',
            'data-sortable'     => 'true',
            'data-sort-name'    => 'info_create_time',
            'data-sort-order'   => 'desc',
        ],
        [
            'data-field'        => 'info_update_time',
            'data-title'        => lang('info_update_time'),
            'data-align'        => 'center',
            'data-halign'       => 'center',
            'data-sortable'     => true,
            'data-sort-name'    => 'info_update_time',
            'data-sort-order'   => 'desc',
        ],
        [
            'data-field'        => 'info_views',
            'data-title'        => lang('info_views'),
            'data-align'        => 'center',
            'data-halign'       => 'center',
            'data-sortable'     => true,
            'data-sort-name'    => 'info_views',
            'data-sort-order'   => 'desc',
        ],
        [
            'data-field'        => 'info_hits',
            'data-title'        => lang('info_hits'),
            'data-align'        => 'center',
            'data-halign'       => 'center',
            'data-sortable'     => true,
            'data-sort-name'    => 'info_hits',
            'data-sort-order'   => 'desc',
        ],
        [
            'data-field'        => 'info_order',
            'data-title'        => lang('info_order'),
            'data-align'        => 'center',
            'data-halign'       => 'center',
            'data-sortable'     => true,
            'data-sort-name'    => 'info_order',
            'data-sort-order'   => 'desc',
        ],
        [
            'data-field'        => 'user_name',
            'data-title'        => lang('info_user_name'),
            'data-align'        => 'center',
            'data-halign'       => 'center',
            'data-escape'       => true,
        ],
        [
            'data-field'        => 'info_user_id',
            'data-title'        => lang('info_user_id'),
            'data-align'        => 'center',
            'data-halign'       => 'center',
            'data-sortable'     => 'true',
            'data-sort-name'    => 'info_user_id',
            'data-sort-order'   => 'desc',
        ],
        [
            'data-field'        => 'operate',
            'data-title'        => lang('operate'),
            'data-align'        => 'center',
            'data-halign'       => 'center',
            'data-width'        => '130',
            'data-width-unit'   => 'px',
            'data-events'       => 'daicuo.admin.daohang.events',
            'data-formatter'    => 'daicuo.table.operate',
        ]
    ]
])}
</form>
{/block}