{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>修改分类导航－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  修改分类导航
</h6>
<!-- -->
{:DcBuildForm([
    'name'     => 'daohang_edit',
    'class'    => 'bg-white form-edit py-2',
    'action'   => DcUrlAddon(['module'=>'daohang','controll'=>'manage','action'=>'update']),
    'method'   => 'post',
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => '',
    'ajax'     => true,
    'data'     => $data,
    'items'    => DcFormItems($items),
])}
{/block}