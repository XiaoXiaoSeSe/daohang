{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("daohang_config")}－{:lang('appName')}</title>
{/block}
{block name="header_addon"}
<link href="{$path_root}{$path_addon}view/theme.css">
<script src="{$path_root}{$path_addon}view/theme.js"></script>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("daohang_config")}
</h6>
{:DcBuildForm([
    'name'     => 'daohang_config',
    'class'    => 'bg-white py-2',
    'action'   => DcUrlAddon(['module'=>'daohang','controll'=>'admin','action'=>'update'],''),
    'method'   => 'post',
    'ajax'     => true,
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => '',
    'items'    => DcFormItems([
        'site_theme'        => ['type'=>'select', 'value'=>config('site_theme'), 'option'=>$optionThemes],
        'theme_wap'         => ['type'=>'select', 'value'=>config('theme_wap'), 'option'=>$optionThemes],
        'search_interval'   => ['type'=>'number', 'value'=>config('daohang.search_interval')],
        'page_size'         => ['type'=>'number', 'value'=>config('daohang.page_size')],
        'rewrite_index'     => ['type'=>'text', 'value'=>config('daohang.rewrite_index')],
        'rewrite_page'      => ['type'=>'text', 'value'=>config('daohang.rewrite_page'),'class_tips'=>$tipsClass,'tips'=>$tipsValue],
        'rewrite_category'  => ['type'=>'text', 'value'=>config('daohang.rewrite_category'),'class_tips'=>$tipsClass,'tips'=>$tipsValue.$tipsPage],
        'rewrite_tag'       => ['type'=>'text', 'value'=>config('daohang.rewrite_tag'),'class_tips'=>$tipsClass,'tips'=>$tipsValue.$tipsPage],
        'rewrite_search'    => ['type'=>'text', 'value'=>config('daohang.rewrite_search'),'class_tips'=>$tipsClass,'tips'=>$tipsSearch.$tipsPage],
        'rewrite_web'       => ['type'=>'text', 'value'=>config('daohang.rewrite_web'),'class_tips'=>$tipsClass,'tips'=>$tipsValue],
    ])
])}
{/block}
<!-- -->
{block name="js"}
{/block}