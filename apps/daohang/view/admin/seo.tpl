{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("daohang_config_seo")}－{:lang('appName')}</title>
{/block}
{block name="header_addon"}
<link href="{$path_root}{$path_addon}view/theme.css">
<script src="{$path_root}{$path_addon}view/theme.js"></script>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("daohang_config_seo")}
</h6>
{:DcBuildForm([
    'name'     => 'daohang_config',
    'class'    => 'bg-white py-2',
    'action'   => DcUrlAddon(['module'=>'daohang','controll'=>'admin','action'=>'save'],''),
    'method'   => 'post',
    'ajax'     => true,
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => '',
    'items'    => DcFormItems([
        'index_title'        => ['type'=>'text', 'value'=>config('daohang.index_title')],
        'index_keywords'     => ['type'=>'text', 'value'=>config('daohang.index_keywords')],
        'index_description'  => ['type'=>'text', 'value'=>config('daohang.index_description')],
        'search_title'       => ['type'=>'text', 'value'=>config('daohang.search_title')],
        'search_keywords'    => ['type'=>'text', 'value'=>config('daohang.search_keywords')],
        'search_description' => ['type'=>'text', 'value'=>config('daohang.search_description')],
        'web_title'          => ['type'=>'text', 'value'=>config('daohang.web_title')],
        'web_keywords'       => ['type'=>'text', 'value'=>config('daohang.web_keywords')],
        'web_description'    => ['type'=>'text', 'value'=>config('daohang.web_description')],
    ])
])}
{/block}
<!-- -->
{block name="js"}
{/block}