{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>修改单页－{:lang('appName')}</title>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  修改单页
</h6>
<!-- -->
{:DcBuildForm([
    'name'     => 'daohang_edit',
    'class'    => 'bg-white form-edit p-2',
    'action'   => DcUrlAddon(['module'=>'daohang','controll'=>'page','action'=>'update']),
    'method'   => 'post',
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => '',
    'ajax'     => true,
    'data'     => $data,
    'items'    => DcFormItems($items),
])}
{/block}