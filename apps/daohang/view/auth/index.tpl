{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{:lang("daohang_config_auth")}－{:lang('appName')}</title>
{/block}
{block name="header_addon"}
<link href="{$path_root}{$path_addon}view/theme.css">
<script src="{$path_root}{$path_addon}view/theme.js"></script>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  {:lang("daohang_config_auth")}
</h6>
{:DcBuildForm([
    'name'     => 'daohang_auth',
    'class'    => 'bg-white py-2',
    'action'   => DcUrlAddon(['module'=>'daohang','controll'=>'auth','action'=>'update'],''),
    'method'   => 'post',
    'ajax'     => false,
    'submit'   => lang('submit'),
    'reset'    => lang('reset'),
    'close'    => false,
    'disabled' => false,
    'callback' => '',
    'items'    => DcFormItems([
        'auth_search[]'     => [
             'type'   => 'checkbox',
             'value'  => config('daohang.auth_search'),
             'option' => DcAuthOption(),
             'class_right_check' => 'form-check form-check-inline py-1',
        ],
        'auth_status[]'      => [
             'type'   => 'checkbox',
             'value'  => config('daohang.auth_status'),
             'option' => DcAuthOption(),
             'class_right_check' => 'form-check form-check-inline py-1',
        ],
        'auth_save[]'      => [
             'type'   => 'checkbox',
             'value'  => config('daohang.auth_save'),
             'option' => DcAuthOption(),
             'class_right_check' => 'form-check form-check-inline py-1',
        ],
        'auth_save_web[]'      => [
             'type'   => 'checkbox',
             'value'  => config('daohang.auth_save_web'),
             'option' => DcAuthOption(),
             'class_right_check' => 'form-check form-check-inline py-1',
        ],
    ])
])}
{/block}
<!-- -->
{block name="js"}
{/block}