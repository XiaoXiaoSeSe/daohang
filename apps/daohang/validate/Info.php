<?php
namespace app\daohang\validate;

use think\Validate;

class Info extends Validate
{
	
	protected $rule = [
        'info_name'   => 'require',
        'info_slug'   => 'require',
        'info_module' => 'require',
        'info_id'     => 'require',
        'url_web'     => 'unique_url_web',
	];
	
	protected $message = [
		'info_name.require'   => '{%info_name_require}',
        'info_module.require' => '{%info_module_require}',
        'info_id.require'     => '{%info_id_require}',
	];
	
	protected $scene = [
		'save'   =>  ['info_module','info_name','url_web'],
		'update' =>  ['info_module','info_name','info_id'],
	];
    
    //网址是否收录
    protected function unique_url_web($value, $rule, $data, $field)
    {
        $where = array();
        $where['info_meta_key']   = ['eq','url_web'];
        $where['info_meta_value'] = ['eq',$value];
        $info = db('infoMeta')->where($where)->value('info_meta_id');
        //无记录直接验证通过
        if(is_null($info)){
            return true;
        }
        //已有记录
		return lang('url_web_unique');
	}
}