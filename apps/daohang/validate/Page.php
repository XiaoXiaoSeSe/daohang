<?php
namespace app\daohang\validate;

use think\Validate;

class Page extends Validate
{
	
	protected $rule = [
        'info_name'     => 'require',
        'info_slug'     => 'require',
        'info_module'   => 'require',
        'info_controll' => 'require',
        'info_id'       => 'require',
	];
	
	protected $message = [
		'info_name.require'     => '{%info_name_require}',
        'info_slug.require'     => '{%info_slug_require}',
        'info_module.require'   => '{%info_module_require}',
        'info_controll.require' => '{%info_controll_require}',
        'info_id.require'       => '{%info_id_require}',
	];
	
	protected $scene = [
		'save'   =>  ['info_module','info_controll','info_name','info_slug'],
		'update' =>  ['info_module','info_controll','info_name','info_slug','info_id'],
	];
}