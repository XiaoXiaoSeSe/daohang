<?php
namespace app\daohang\validate;

use think\Validate;

class User extends Validate{
	
	protected $rule = [
		'user_name'         =>  'require|length:1,40',
		'user_email'        =>  'require|email|unique:user',
		'user_mobile'       =>  'require|mobile|unique:user',
        'user_pass'         =>  'require',
        'user_pass_confirm' =>  'require|confirm:user_pass',
        'user_pass_old'     =>  'require',
	];
	
	protected $message = [
		'user_name.require'         => '{%user_name_require}',
		'user_name.length'          => '用户名长度为1-40个字符',
        'user_email.require'        => '{%user_email_require}',
        'user_email.unique'         => '{%user_email_unique}',
		'user_email.email'          => '{%user_email_isemail}',
        'user_mobile.require'       => '{%user_mobile_require}',
        'user_mobile.unique'        => '{%user_mobile_unique}',
		'user_mobile.mobile'        => '{%user_mobile_ismobile}',
        'user_pass.require'         => '请输入用户密码',
        'user_pass_confirm.require' => '请再次输入用户密码',
        'user_pass_confirm.confirm' => '两次输入的密码不一致，请重新输入',
        'user_pass_old.require'     => '请输入用户旧的密码',
	];
	
	//验证场景
	protected $scene = [
		'login'     =>  ['user_name'],
        'register'  =>  ['user_name','user_email','user_mobile'],
        'repwd'     =>  ['user_pass','user_pass_confirm','user_pass_old'],
	];
    
    //自定义规则
    protected function mobile($value, $rule, $data, $field)
    {
		if(!is_mobile($value)){
            return lang('user_mobile_ismobile');
		}
		return true;
	}
}