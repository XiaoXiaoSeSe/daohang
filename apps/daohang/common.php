<?php
/**
 * 添加一个导航数据
 * @version 1.0.0 首次引入
 * @param array $post 必需;数组格式,支持的字段列表请参考手册
 * @return mixed $mixed 成功时返回obj,失败时null
 */
function daohangSave($post=[])
{
    //DcConfigMerge('custom_fields.info_meta', config('daohang.fields'));
  
    config('common.validate_name','daohang/Info');
        
    config('common.validate_scene','save');

    config('common.where_slug_unique',['info_module'=>['eq','daohang'],'info_controll'=>['eq','web']]);
    
    $post = DcArrayArgs($post,[
        'info_module'   => 'daohang',
        'info_controll' => 'web',
        'info_staus'    => 'normal',
        'info_tpl'      => 'index',
    ]);

    return \daicuo\Info::save(daohangPostData($post), 'info_meta,term_map');
}

/**
 * 删除一条或多条导航数据
 * @version 1.0.0 首次引入
 * @param mixed $ids 必需;多个用逗号分隔或使用数组传入(array|string);默认：空 
 * @return array $array ID作为键名,键值为删除结果(bool)
 */
function daohangDelete($ids=[])
{
    $result = [];
    if(is_string($ids)){
        $ids = explode(',', $ids);
    }
    foreach($ids as $key=>$id){
        $result[$id] = \daicuo\Info::delete_id($id);
    }
    return $result;
}

/**
 * 修改一个导航数据(需传入主键值作为更新条件)
 * @version 1.0.0 首次引入
 * @param array $post 必需;数组格式,支持的字段列表请参考手册 {
 *     @type int $info_id 必需;按ID修改导航;默认：空
 * }
 * @return mixed $mixed 成功时返回obj,失败时null
 */
function daohangUpdate($post=[])
{
    config('common.validate_name','daohang/Info');
        
    config('common.validate_scene','update');

    config('common.where_slug_unique',['info_module'=>['eq','daohang'],'info_controll'=>['eq','web']]);
    
    $post = DcArrayArgs($post,[
        'info_module'   => 'daohang',
        'info_controll' => 'web',
        'info_tpl'      => 'index',
    ]);
    
    return \daicuo\Info::update_id($post['info_id'], daohangPostData($post), 'info_meta,term_map');
}

/**
 * 按ID快速获取一条内容数据
 * @version 1.6.0 首次引入
 * @param int $value 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangId($value='', $cache=true, $status='normal')
{
    return \daicuo\Info::get_by('info_id', $value, $cache, false, $status);
}

/**
 * 按ID快速获取一条内容数据
 * @version 1.6.0 首次引入
 * @param int $value 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangSlug($value='', $cache=true, $status='normal')
{
    if ( !$value ) {
        return null;
    }
    $args = [];
    $args['cache'] = $cache;
    $args['where']['info_module']   = ['eq', 'daohang'];
    $args['where']['info_controll'] = ['eq', 'web'];
    $args['where']['info_status']   = ['eq', $status];
    $args['where']['info_slug']     = ['eq', $value];
    return \daicuo\Info::meta_attr( \daicuo\Info::get($args) );
    //return \daicuo\Info::get_by('info_slug', $value, $cache, false, $status);
}

/**
 * 按ID快速获取一条内容数据
 * @version 1.0.0 首次引入
 * @param int $value 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangName($value='', $cache=true, $status='normal')
{
    return \daicuo\Info::get_by('info_name', $value, $cache, false, $status);
}

/**
 * 按条件查询单个网址数据
 * @version 1.0.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type mixed $id 可选;内容ID(stirng|array);默认：空
 *     @type mixed $name 可选;内容名称(stirng|array);默认：空
 *     @type mixed $slug 可选;内容别名(stirng|array);默认：空
 *     @type mixed $title 可选;内容别名(stirng|array);默认：空
 *     @type mixed $user_id 可选;用户ID(stirng|array);默认：空
 *     @type array $with 可选;自定义高级查询条件;默认：空
 *     @type array $view 可选;自定义高级查询条件;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function daohangGet($args)
{
    $args = DcArrayArgs($args,[
        'cache'    => 'true',
        'module'   => 'daohang',
        'controll' => 'web',
    ]);
    return DcInfoFind($args);
}

/**
 * 按条件查询多个网址数据
 * @version 1.0.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $field 可选;查询字段;默认：*
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $sort 可选;排序字段名(info_id|info_order|info_views|info_hits|meta_value_num);默认：info_id
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $search 可选;搜索关键词（info_name|info_slug|info_excerpt）;默认：空
 *     @type mixed $id 可选;内容ID限制条件(int|array);默认：空
 *     @type mixed $title 可选;标题限制条件(stirng|array);默认：空
 *     @type mixed $name 可选;名称限制条件(stirng|array);默认：空
 *     @type mixed $slug 可选;别名限制条件(stirng|array);默认：空
 *     @type mixde $action 可选;所属操作(stirng|array);默认：空
 *     @type mixed $term_id 可选;分类法ID限制条件(string|array);默认：空
 *     @type array $meta_query 可选;自定义字段(二维数组[key=>['eq','key'],value=>['in','key']]);默认：空
 *     @type string $result 可选;返回结果类型(array|obj);默认：array
 *     @type array $with 可选;自定义高级查询条件;默认：空
 *     @type array $view 可选;自定义高级查询条件;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed 查询结果（array|null）
 */
function daohangSelect($args)
{
    $args = DcArrayArgs($args,[
        'cache'    => 'true',
        'module'   => 'daohang',
        'controll' => 'web',
        'result'   => 'array',
    ]);
    //
    if($args['term_id'][0] == 'in' && is_null($args['term_id'][1])){
        unset($args['term_id']);
    }
    return DcInfoSelect($args);
}

/**********************************************************************************************************/

/**
 * 按ID快速获取分类信息
 * @version 1.0.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(array|null)
 */
function daohangCategoryId($id='', $cache=true, $status='normal')
{
    return DcTermFindId($id, $cache, $status);
}

/**
 * 按别名快速获取分类信息
 * @version 1.0.0 首次引入
 * @param string $value 必需;别名值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(array|null)
 */
function daohangCategorySlug($value='', $cache=true, $status='normal')
{
    return \daicuo\Term::get_by('term_slug', $value, $cache, 'category', $status);
}

/**
 * 按分类名称快速获取分类信息
 * @version 1.0.0 首次引入
 * @param string $value 必需;分类名称；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(array|null)
 */
function daohangCategoryName($value='', $cache=true, $status='normal')
{
    return \daicuo\Term::get_by('term_name', $value, $cache, 'category', $status);
}

/**
 * 获取网站分类列表
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $result 可选;模型名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed obj|null 查询结果
 */
function daohangCategorySelect($args=[])
{
    return DcTermSelect( DcArrayArgs($args,[
        'cache'   => true,
        'type'    => 'category',
        'module'  => 'daohang',
        'result'  => 'array',
    ]) );
}

/**********************************************************************************************************/

/**
 * 按ID快速获取分类信息
 * @version 1.0.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(array|null)
 */
function daohangTagId($id='', $cache=true, $status='normal')
{
    return DcTermFindId($id, $cache, $status);
}

/**
 * 按别名快速获取分类信息
 * @version 1.0.0 首次引入
 * @param string $value 必需;别名值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(array|null)
 */
function daohangTagSlug($value='', $cache=true, $status='normal')
{
    return \daicuo\Term::get_by('term_slug', $value, $cache, 'tag', $status);
}

/**
 * 按分类名称快速获取标签信息
 * @version 1.0.0 首次引入
 * @param string $value 必需;分类名称；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(array|null)
 */
function daohangTagName($value='', $cache=true, $status='normal')
{
    return \daicuo\Term::get_by('term_name', $value, $cache, 'tag', $status);
}

/**
 * 获取多个标签列表的某一个字段
 * @version 1.0.0 首次引入
 * @param int $limit 必需;数量限制；默认：10
 * @param string $field 可选;返回字段;默认：term_name
 * @return mixed $mixed 查询结果(array|null)
 */
function daohangTags($limit=10, $field='term_name')
{
    return array_column(daohangTagSelect(['status'=>'normal','limit'=>$limit,'sort'=>'term_count desc,term_id','order'=>'desc']), $field);
}

/**
 * 获取网站分类列表
 * @version 1.0.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $result 可选;模型名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed obj|null 查询结果
 */
function daohangTagSelect($args=[])
{
    return DcTermSelect( DcArrayArgs($args,[
        'cache'   => true,
        'type'    => 'tag',
        'module'  => 'daohang',
        'result'  => 'array',
    ]) );
}

/**
 * 获取网站导航列表
 * @version 1.6.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type bool $tree 可选;是否转化为树型;默认：tree
 *     @type bool $level 可选;是否将树型还原为层级结构;默认：false
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $module 可选;模型名称;默认：空
 *     @type string $controll 可选;控制器名称;默认：空
 *     @type string $action 可选;操作名称;默认：空
 *     @type string $field 可选;查询字段;默认：*
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed obj|null 查询结果
 */
function daohangNavSelect($args=[])
{
    return DcNavSelect( DcArrayArgs($args,[
        'status' => 'normal',
        'tree'   => true,
        'limit'  => 999,
    ]) );
}

/********************************************************************************************/

/**
 * 添加一个单页
 * @version 1.0.0 首次引入
 * @param array $post 必需;数组格式,支持的字段列表请参考手册
 * @return mixed $mixed 成功时返回obj,失败时null
 */
function daohangPageSave($post=[])
{
    config('common.validate_name','daohang/Page');
        
    config('common.validate_scene','save');

    config('common.where_slug_unique',['info_module'=>['eq','daohang'],'info_controll'=>['eq','page']]);
    
    $post = DcArrayArgs($post,[
        'info_module'   => 'daohang',
        'info_controll' => 'page',
        'info_staus'    => 'normal',
        'info_tpl'      => 'index',
    ]);

    return \daicuo\Info::save($post, 'info_meta');
}

/**
 * 删除一条或多条单页
 * @version 1.0.0 首次引入
 * @param mixed $ids 必需;多个用逗号分隔或使用数组传入(array|string);默认：空 
 * @return array $array ID作为键名,键值为删除结果(bool)
 */
function daohangPageDelete($ids=[])
{
    $result = [];
    if(is_string($ids)){
        $ids = explode(',', $ids);
    }
    foreach($ids as $key=>$id){
        $result[$id] = \daicuo\Info::delete_id($id);
    }
    return $result;
}

/**
 * 修改一个单页(需传入主键值作为更新条件)
 * @version 1.0.0 首次引入
 * @param array $post 必需;数组格式,支持的字段列表请参考手册 {
 *     @type int $info_id 必需;按ID修改导航;默认：空
 * }
 * @return mixed $mixed 成功时返回obj,失败时null
 */
function daohangPageUpdate($post=[])
{
    config('common.validate_name','daohang/Page');
        
    config('common.validate_scene','update');

    config('common.where_slug_unique',['info_module'=>['eq','daohang'],'info_controll'=>['eq','page']]);
    
    $post = DcArrayArgs($post,[
        'info_module'   => 'daohang',
        'info_controll' => 'page',
        'info_tpl'      => 'index',
    ]);
    
    return \daicuo\Info::update_id($post['info_id'], $post, 'info_meta');
}

//查询单个导航数据
function daohangPageGet($args=[])
{
    $args = DcArrayArgs($args,[
        'cache'    => 'true',
        'module'   => 'daohang',
        'controll' => 'page',
        'with'     => 'info_meta',
    ]);
    return \daicuo\Info::meta_attr( DcInfoFind($args) );
}

/**
 * 按条件获取多条单页
 * @version 1.0.0 首次引入
 * @param array $args 必需;查询条件数组格式 {
 *     @type bool $cache 可选;是否缓存;默认：true
 *     @type int $limit 可选;分页大小;默认：0
 *     @type int $page 可选;当前分页;默认：0
 *     @type string $sort 可选;排序字段名;默认：op_order
 *     @type string $order 可选;排序方式(asc|desc);默认：asc
 *     @type string $status 可选;显示状态（normal|hidden）;默认：空
 *     @type string $result 可选;模型名称;默认：空
 *     @type array $where 可选;自定义高级查询条件;默认：空
 *     @type array $paginate 可选;自定义高级分页参数;默认：空
 * }
 * @return mixed $mixed obj|null 查询结果
 */
function daohangPageSelect($args=[])
{
    $args = DcArrayArgs($args,[
        'cache'    => 'true',
        'module'   => 'daohang',
        'controll' => 'page',
        'with'     => 'info_meta',
        'result'   => 'array',
    ]);
    return DcInfoSelect($args);
}

/**********************************************************************************************************/

/**
 * 表单数据处理
 * @version 1.0.0 首次引入
 * @param array $post 必需;数组格式,支持的字段列表请参考手册
 * @return array 处理后的数据
 */
function daohangPostData($post){
    //分类处理
    if($post['category_id']){
        $post['term_id'] = $post['category_id'];
        unset($post['category_id']);
    }
    
    //标签处理
    if($post['tag_name']){
        $post['tag_id'] = daohangTermNameToIds($post['tag_name'], 'tag', true);
        unset($post['tag_name']);
    }
    if($post['tag_id']){
        $post['term_id'] = DcArrayArgs($post['tag_id'],$post['term_id']);
        unset($post['tag_id']);
    }
    
    return $post;
}

/**
 * 将urlPid写入COOKIE中(后者推广覆盖前者)
 * @param string $userPid 上级用户ID
 * @return boole
 */
function doahangPidSet(){
    $userPid = input('pid/d', 0);
    if( $userPid < 1){
        return false;
    }
    cookie('userPid', $userPid, 2592000);
    return true;
}

/**
 * 读取PID、优先urlPid
 * @return number
 */
function doahangPidGet(){
    $userPid = input('pid/d', 0);
    if($userPid < 1){
        $userPid = cookie('userPid');
    }
    return intval($userPid);
}

/**
 * 获取搜索页链接
 * @version 1.0.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param array $info 可选;数据状态;默认：normal
 * @param mixed $page 可选;是否缓存;默认：true
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangUrlSearch($args=[], $action='index')
{
    $args = DcArrayFilter($args,['searchText','pageNumber','pageSize','sortName','sortOrder']);
    $route = config('daohang.rewrite_search');
    if( strpos($route,':pageSize') === false ){
        unset($args['pageSize']);
    }
    if( strpos($route,':sortName') === false ){
        unset($args['sortName']);
    }
    if( strpos($route,':sortOrder') === false ){
        unset($args['sortOrder']);
    }
    return DcUrl('daohang/search/'.$action, $args, '');
}

/**
 * 获取导航分类页链接
 * @version 1.0.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param array $info 可选;数据状态;默认：normal
 * @param mixed $page 可选;是否缓存;默认：true
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangUrlCategory($info=[], $pageNumber='')
{
    $field = daohangRouteToField(config('daohang.rewrite_category'));
    if(!$info['term_'.$field]){
        return 'javascript:;';
    }
    //默认参数
    $args = [
        $field => $info['term_'.$field]
    ];
    if($pageNumber){
        $args['pageNumber'] = $pageNumber;
    }
    return DcUrl('daohang/category/index', $args, '');
}

/**
 * 获取导航标签链接
 * @version 1.0.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param array $info 可选;数据状态;默认：normal
 * @param mixed $page 可选;是否缓存;默认：true
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangUrlTag($info=[], $pageNumber='')
{
    $field = daohangRouteToField(config('daohang.rewrite_tag'));
    if(!$info['term_'.$field]){
        return 'javascript:;';
    }
    //默认参数
    $args = [
        $field => $info['term_'.$field]
    ];
    if($pageNumber){
        $args['pageNumber'] = $pageNumber;
    }
    return DcUrl('daohang/tag/index', $args, '');
}

/**
 * 获取导航详情页链接(controll决定)
 * @version 1.0.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangUrlInfo($info)
{
    return daohangUrlWeb($info);
}

/**
 * 获取导航详情页链接(网址)
 * @version 1.0.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangUrlWeb($info)
{
    $field = daohangRouteToField(config('daohang.rewrite_web'));
    if(!$info['info_'.$field]){
        return 'javascript:;';
    }
    return DcUrl('daohang/web/index',[$field=>$info['info_'.$field]],'');
}

/**
 * 获取单页链接
 * @version 1.0.0 首次引入
 * @param int $id 必需;Id值；默认：空
 * @param bool $cache 可选;是否缓存;默认：true
 * @param string $status 可选;数据状态;默认：normal
 * @return mixed $mixed 查询结果(obj|null)
 */
function daohangUrlPage($info)
{
    $field = daohangRouteToField(config('daohang.rewrite_page'));
    if(!$info['info_'.$field]){
        if(in_array($field,['id','name']) && $info['info_slug']){
            return DcUrl('daohang/page/index',['slug'=>$info['info_slug']],'');
        }
        return 'javascript:;';
    }
    return DcUrl('daohang/page/index',[$field=>$info['info_'.$field]],'');
}

/**
 * 获取图片附件链接
 * @version 1.0.0 首次引入
 * @param string $file 必需;图片传件路径;默认：空
 * @param string $root 可选;根目录;默认：/
 * @param string $default 可选;默认图片地址;默认：x.gif
 * @return string $string 无图片时返回默认横向图
 */
function daohangUrlImage($file='', $root='/', $default='public/images/x.gif')
{
    return DcEmpty(DcUrlAttachment($file), $root.$default);
}

/**
 * 获取默认地址栏参数
 * @version 1.0.0 首次引入
 * @param string $sort 可选;排序字段;默认：info_id
 * @param string $order 可选;排序方式;默认：desc
 * @param string $limit 可选;每页大小;默认：10
 * @param string $page 可选;当前分页;默认：1
 * @return array $array 默认地址栏五个参数
 */
function daohangUrlParams($sort='info_id', $order='desc', $limit=10, $page=1)
{
    return [
        'sort'   => htmlspecialchars(trim(input('param.sortName/s',$sort))),
        'order'  => htmlspecialchars(trim(input('param.sortOrder/s',$order))),
        'limit'  => input('param.pageSize/d', DcEmpty(config('daohang.page_size'),$limit)),
        'page'   => input('param.pageNumber/d', $page),
        'search' => htmlspecialchars(strip_tags(trim(input('param.searchText/s')))),
    ];
}

/**
 * 根据对日期或时间进行格式化
 * @version 1.0.0 首次引入
 * @param string $format 必需;规定时间戳的格式;空
 * @param mixed $timestamp 可选;规定时间戳;空
 * @return string $string 格式化后的时间
 */
function daohangDate($format='Y-m-d', $timestamp='')
{
    if(!is_numeric($timestamp)){
        $timestamp = strtotime($timestamp);
    }
    return date($format, $timestamp);
}

/**
 * 根据对日期或时间进行格式化
 * @version 1.0.0 首次引入
 * @param string $color 必需;规定时间戳的格式;空
 * @param string $default 可选;默认颜色值;空
 * @return string $string 格式化后的时间
 */
function daohangColor($color, $default='text-dark')
{
    return DcEmpty($color, $default);
}

/**
 * 多个队列名转化为多个队例ID
 * @version 1.6.0 首次引入
 * @param mixed $values 必需;多个队列名称,逗号分隔或数组(string|array)；默认：空
 * @param string $type 必需;队例类型(category|tag)；默认：category
 * @param bool $save 可选;是否自动新增不存在的关键字；默认：flase
 * @return int $int 父级ID值
 */
function daohangTermNameToIds($values='', $type='category', $save=false){
    if(!$values){
        return 0;
    }
    if( is_string($values) ){
        $values = explode(',',$values);
    }
    $list = DcTermSelect([
        'field' => 'term_id,term_name,term_slug,term_status',
        'with'  => '',
        'result'=> 'array',
        'cache' => true,
        'type'  => $type,
        'name'  => ['in', $values],
    ]);
    //只返回已存在的，不自动新增
    if($save == false){
        return array_column($list, 'term_id');
    }
    //键名队例名，键值队例ID
    $listDb = [];
    foreach($list as $key=>$value){
        $listDb[$value['term_name']] = $value['term_id'];
    }
    //需要新添加的field
    $listSave = [];
    foreach($values as $key=>$value){
        if(array_key_exists($value, $listDb)){
            $listSave[$value] = $listDb[$value];
        }else{
            $listSave[$value] = \daicuo\Term::Save(['term_module'=>'daohang','term_type'=>$type,'term_name'=>$value,'term_slug'=>'']);
            DcCacheTag('common/Term/Item', 'clear');
        }
    }
    return array_values($listSave);
}

/**
 * 批量添加标签或分类
 * @version 1.0.0 首次引入
 * @param string $type 必需;队列方式(category|tag);默认：category
 * @param array $post 必需;数组格式,支持的字段列表请参考手册;默认：空
 * @param string $parentName 可选;父级名称,默认：空
 * @return mixed $mixed 成功时返回obj,失败时null
 */
function daohangTermSave($type='category', $post=[], $parentName='')
{
    $default = [
        'term_name'       => '网址导航',
        'term_slug'       => 'wzdh',
        'term_module'     => 'daohang',
        'term_status'     => 'normal',
        'term_order'      => 0,
        'term_type'       => $type,
        'term_info'       => '收录网址导航相关的网站',
        'term_parent'     => 0,
        'term_tpl'        => 'index',
    ];
    $list = [];
    if($parentName){
        foreach($post as $key=>$value){
            array_push($list, DcArrayArgs([
                'term_name' => $value,
                'term_slug' => DcPinYin($value),
                'term_info' => '收录与'.$value.'相关的网站',
                'term_parent' => DcTermNameToId($parentName),
            ], $default));
        }
    }else{
        foreach($post as $key=>$value){
            array_push($list, DcArrayArgs([
                'term_name' => $value,
                'term_slug' => DcPinYin($value),
                'term_info' => '收录与'.$value.'相关的网站',
            ], $default));
        }
    }
    return \daicuo\Term::save_all($list);
}

/**
 * 根据伪静态规则关键字返回对应的字段名
 * @version 1.0.0 首次引入
 * @param string $route 必需;伪静态规则名;空
 * @return string $string 操作名
 */
function daohangRouteToField($route='')
{
    if( strpos($route,':slug') !== false ){
        return 'slug';
    }elseif( strpos($route,':name') !== false ){
        return 'name';
    }
    return 'id';
}

/**
 * 根据伪静态规则关键字返回对应的字段名
 * @version 1.0.0 首次引入
 * @param string $route 必需;伪静态规则名;空
 * @return string $string 操作名
 */
function daohangSeo($string)
{
    $search = ['[siteName]', '[siteDomain]', '[pageNumber]'];
    $replace = [config('common.site_name'), config('common.site_domain'), input('param.pageNumber/d',1)];
    return str_replace($search, $replace, $string);
}