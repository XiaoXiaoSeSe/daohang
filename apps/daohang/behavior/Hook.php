<?php
namespace app\daohang\behavior;

//初始钩子

class Hook
{
    // 表单生成
    public function formBuild(&$params)
    {
        //追加表单字段(分类与标签)
        if( in_array($params['name'],['category_create','category_edit','tag_create','tag_edit']) ){
            $default = [
                'type'        => 'text',
                'class_left'  => 'col-12',
                'class_right' => 'col-12',
            ];
            $params['items'] = array_merge($params['items'], DcFormItems([
                'term_title'       => DcArrayArgs(['value'=>$params['data']['term_title']],$default),
                'term_keywords'    => DcArrayArgs(['value'=>$params['data']['term_keywords']],$default),
                'term_description' => DcArrayArgs(['value'=>$params['data']['term_description']],$default),
            ]));
        }
    }
    
}