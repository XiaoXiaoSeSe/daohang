<?php
namespace app\daohang\controller;

use app\common\controller\Front;

class Search extends Front
{

    public function _initialize()
    {
        parent::_initialize();
    }
    
    //站内搜索
    public function index()
    {           
        //post请求
        if( $this->request->isPost() ){
            $this->redirect(daohangUrlSearch(['searchText'=>input('post.searchText/s')],'index'),302);
        }
        
        //判断是否有关键词
        if(!$this->query['searchText']){
            $this->error(lang('please input keywords'),'daohang/index/index');
        }
        
        //请求限制
        if( !$this->searchRequest() ){
            $this->error(lang('please have a rest'),'daohang/index/index');
        }
        
        //地址栏参数
        $info = daohangUrlParams('info_order desc,info_id');
        
        //分页路径
        $info['path'] = daohangUrlSearch([
            'searchText' => $info['search'],
            'pageNumber' => '[PAGE]',
            'pageSize'   => $info['limit'],
            'sortName'   => $info['sort'],
            'sortOrder'  => $info['order'],
        ],'index');
        
        //所有搜索引擎链接
        $info = $this->searchUrls($info);
        
        //处理SEO优化
        $info = $this->searchSeo($info);
        
        $this->assign($info);
        
        return $this->fetch();
    }
    
    //搜索网址
    public function web()
    {
        //post请求
        if( $this->request->isPost() ){
            $this->redirect(daohangUrlSearch(['searchText'=>input('post.searchText/s')],'web'),302);
        }
        
        //判断是否有关键词
        if(!$this->query['searchText']){
            $this->error(lang('please input keywords'),'daohang/index/index');
        }
        
        //请求限制
        if( !$this->searchRequest() ){
            $this->error(lang('please have a rest'),'daohang/index/index');
        }
        
        //地址栏参数
        $info = daohangUrlParams('info_order desc,info_id');
        
        //分页路径
        $info['path'] = daohangUrlSearch([
            'searchText' => $info['search'],
            'pageNumber' => '[PAGE]',
            'pageSize'   => $info['limit'],
            'sortName'   => $info['sort'],
            'sortOrder'  => $info['order'],
        ],'web');
        
        //所有搜索引擎链接
        $info = DcArrayArgs($info, $this->searchUrls($info));
        
        //处理SEO优化
        $info = $this->searchSeo($info);
        
        $this->assign($info);
        
        return $this->fetch();
    }
    
    public function baidu()
    {
        $this->redirect('https://www.baidu.com/s?wd='.$this->searchText(),302);
    }
    
    public function sogou()
    {
        $this->redirect('https://www.sogou.com/web?query='.$this->searchText(),302);
    }
    
    public function toutiao()
    {
        $this->redirect('https://so.toutiao.com/search?mod=website&keyword='.$this->searchText(),302);
    }
    
    public function bing()
    {
        $this->redirect('https://cn.bing.com/search?q='.$this->searchText(),302);
    }
    
    public function so()
    {
        $this->redirect('https://www.so.com/s?q='.$this->searchText(),302);
    }
    
    //搜索请求是否合法（间隔睦长）
    private function searchRequest()
    {
        if(config('daohang.search_interval')){
            //搜索限制白名单
            if( array_intersect(config('daohang.auth_search'), $this->site['user']['user_capabilities']) ){
                return true;
            }
            //客户端唯一标识 $_SERVER['REMOTE_ADDR']
            $cacheKey = md5('searchdh'.$this->request->ip().$this->request->header('user-agent'));
            if( DcCache($cacheKey) ){
                return false;
            }
            //写入请求标识
            DcCache($cacheKey, 1, config('daohang.search_interval'));
        }
        return true;
    }
    
    //获取关键字
    private function searchText()
    {
        if( $this->request->isPost() ){
            return DcHtml(input('post.searchText/s','呆错导航系统'));
        }
        return DcHtml(input('searchText/s','呆错导航系统'));
    }
    
    //处理SEO
    private function searchSeo($info){
        $info['seoTitle'] = str_replace('[searchText]', $info['search'], daohangSeo(config('daohang.search_title')));
        $info['seoKeywords'] = str_replace('[searchText]', $info['search'], daohangSeo(config('daohang.search_keywords')));
        $info['seoDescription'] = str_replace('[searchText]', $info['search'], daohangSeo(config('daohang.search_description')));
        return $info;
    }
    
    //其它搜索引擎路径
    private function searchUrls($info)
    {
        //标题搜索
        $info['searchIndex'] = daohangUrlSearch([
            'searchText' => $info['search'],
            'pageSize'   => $info['limit'],
            'sortName'   => $info['sort'],
            'sortOrder'  => $info['order'],
        ],'index');
        
        //网址搜索
        $info['searchWeb'] = daohangUrlSearch([
            'searchText' => $info['search'],
            'pageSize'   => $info['limit'],
            'sortName'   => $info['sort'],
            'sortOrder'  => $info['order'],
        ],'web');
        
        $info['searchBaidu'] = daohangUrlSearch(['searchText' => $info['search']], 'baidu');
        
        $info['searchSogou'] = daohangUrlSearch(['searchText' => $info['search']], 'sogou');
        
        $info['searchToutiao'] = daohangUrlSearch(['searchText' => $info['search']], 'toutiao');
        
        $info['searchBing'] = daohangUrlSearch(['searchText' => $info['search']], 'bing');
        
        $info['searchSo'] = daohangUrlSearch(['searchText' => $info['search']], 'so');
        
        return $info;
    }
}