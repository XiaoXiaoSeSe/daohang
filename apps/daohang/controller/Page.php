<?php
namespace app\daohang\controller;

use app\common\controller\Front;

class Page extends Front
{

    public function _initialize()
    {
        // 移除标签
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        // 继承上级
        parent::_initialize();
    }
    
    public function index()
    {
        if( isset($this->query['id']) ){
            $info = daohangPageGet([
                'id'     => ['eq',$this->query['id']],
                'status' => 'normal',
            ]);
        }elseif( isset($this->query['slug']) ){
            $info = daohangPageGet([
                'slug'   => ['eq',$this->query['slug']],
                'status' => 'normal',
            ]);
        }elseif( isset($this->query['name']) ){
            $info = daohangPageGet([
                'name'   => ['eq',$this->query['name']],
                'status' => 'normal',
            ]);
        }else{
            $this->error(lang('mustIn'),'daohang/index/index');
        }
        
        if(!$info){
            $this->error(lang('empty'),'daohang/index/index');
        }
        
        //变量赋值与加载模板
        $this->assign($info);
        return $this->fetch(DcEmpty($info['info_tpl'],'index'));
    }
  
    public function _empty($name)
    {
        $this->error(lang('empty'),'daohang/index/index');
    }
  
}