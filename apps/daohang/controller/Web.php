<?php
namespace app\daohang\controller;

use app\common\controller\Front;

class Web extends Front
{

    public function _initialize()
    {
        // 移除标签
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        // 继承上级
        parent::_initialize();
    }
    
    public function index()
    {
        if( isset($this->query['id']) ){
            $info = daohangId($this->query['id']);
        }elseif( isset($this->query['slug']) ){
            $info = daohangSlug($this->query['slug']);
        }elseif( isset($this->query['name']) ){
            $info = daohangName($this->query['name']);
        }else{
            $this->error(lang('mustIn'),'daohang/index/index');
        }
        
        if(!$info){
            $this->error(lang('empty'),'daohang/index/index');
        }
    
        //变量赋值与加载模板
        $this->assign($info);
        return $this->fetch(DcEmpty($info['info_tpl'],'index'));
    }
}