<?php
namespace app\daohang\controller;

use app\common\controller\Front;

class Publish extends Front
{

    public function _initialize()
    {
        $this->auth['check'] = true;
        
        $this->auth['none_login'] = ['daohang/publish/index'];
        
        $this->auth['none_right'] = ['daohang/publish/index'];
        
        $this->auth['error_login'] = 'daohang/index/index';
        
        $this->auth['error_right'] = 'daohang/index/index';
        
        parent::_initialize();
    }
    
    //网站快捷发布保存
    public function index()
    {
        $this->assign([
            'seoTitle'       => daohangSeo(config('daohang.web_title')),
            'seoKeywords'    => daohangSeo(config('daohang.web_keywords')),
            'seoDescription' => daohangSeo(config('daohang.web_description')),
        ]);
        return $this->fetch();
    }
    
    //网站快捷发布保存
    public function save()
    {
        $url = input('post.url/s');
        if(!$url){
            $this->error('请输入待收录的网址');
        }
        $html = DcCurl('windows', 10, $url, '', 'https://www.baidu.com');
        if(!$html){
            $this->error('您输入的网址不正确，请重新输入');
        }
        $data = [];
        $data['info_title']       = trim(DcPregMatch('<title>([\s\S]*?)<\/title>', $html));
        $data['info_keywords']    = trim(str_replace('，',',',DcPregMatch('<meta name="keywords" content="([\s\S]*?)"', $html)));
        $data['info_description'] = trim(DcPregMatch('<meta name="description" content="([\s\S]*?)"', $html));
        //
        $data['info_name']     = DcEmpty($data['info_title'], '标题获取失败');
        $data['info_slug']     = uniqid();
        $data['info_views']    = rand(99,999);
        $data['info_user_id']  = $this->site['user']['user_id'];
        $data['info_excerpt']  = $data['info_description'];
        $data['info_content']  = $data['info_description'];
        $data['url_web']       = $url;
        $data['term_id']       = [];
        $data['tag_name']      = input('post.tag_name/s');
        $data['category_id']   = input('post.category_id/a');
        $data['info_module']   = 'daohang';
        $data['info_controll'] = 'web';
        $data['info_status']   = $this->status();
        //保存至数据库
        if( !DcArrayResult(daohangSave($data)) ){
		    $this->error(\daicuo\Info::getError());
        }
        $this->success(lang('success'));
    }
    
    //用户中心发布网站
    public function saveWeb()
    {
        //$this->request->filter('trim,strip_tags,htmlspecialchars');
        //$post = $this->request->post();
        $data = [];
        $data['info_module']   = 'daohang';
        $data['info_controll'] = 'web';
        $data['info_status']   = $this->status();
        $data['info_user_id']  = $this->site['user']['user_id'];
        $data['term_id']       = [];
        //
        $data['info_name']        = input('post.info_name/s','未填写标题');
        $data['info_slug']        = input('post.info_slug/s','');
        $data['info_excerpt']     = input('post.info_excerpt/s','');
        $data['info_content']     = input('post.info_content/s','');
        $data['image_level']      = input('post.image_level/s','');
        $data['url_web']          = input('post.url_web/s','');
        $data['info_title']       = input('post.info_title/s','');
        $data['info_keywords']    = input('post.info_keywords/s','');
        $data['info_description'] = input('post.info_description/s','');
        $data['tag_name']         = input('post.tag_name/s');
        $data['category_id']      = input('post.category_id/a');
        //保存至数据库
        if( !DcArrayResult(daohangSave($data)) ){
		    $this->error(\daicuo\Info::getError());
        }
        $this->success(lang('success'));
    }
    
    //更新网站发布
    public function update()
    {
        
    }
    
    //发布免审状态
    private function status()
    {
        if( array_intersect(config('daohang.auth_status'), $this->site['user']['user_capabilities']) ){
            return 'normal';
        }
        return 'hidden';
    }
}