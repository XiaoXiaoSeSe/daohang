<?php
namespace app\daohang\controller;

use app\common\controller\Front;

class User extends Front
{
    protected $auth = [
         'check'       => true,
         'none_login'  => ['daohang/user/register','daohang/user/login','daohang/user/logout'],
         'none_right'  => '*',
         'error_login' => 'daohang/user/login',
         'error_right' => '',
    ];
    
	public function _initialize()
    {
		parent::_initialize();
	}
    
    //网址发布
    public function web(){
        return $this->fetch();
    }
    
	public function repwd()
    {
        if( $this->request->isPost() ){
            //接收表单数据
            $post = [];
            $post['user_pass'] = input('post.user_pass/s');
            $post['user_pass_confirm'] = input('post.user_pass_confirm/s');
            $post['user_pass_old'] = input('post.user_pass_old/s');
             //旧密码验证
            if($this->site['user']['user_pass'] != md5($post['user_pass_old'])){
                $this->error('您输入的旧密码不正确','daohang/user/repwd');
            }
            //验证规则
            config('common.validate_name','daohang/User');
            config('common.validate_scene','repwd');
            //更新接口
            $result = \daicuo\User::update(['user_id'=>['eq',$this->site['user']['user_id']]], $post);
            if(!$result){
                $this->error(\daicuo\User::getError(),'daohang/user/repwd');
            }
            //退出当前登录
            \daicuo\User::logout();
            $this->success('密码修改成功，请重新登录','daohang/user/login');
        }
		return $this->fetch();
	}

	public function register()
    {
        if( $this->request->isPost() ){
            //验证码
            $this->validateCaptcha(input('post.user_captcha/s'));
            //验证规则
            config('common.validate_name','daohang/User');
            config('common.validate_scene','register');
            //注册信息
            $data = array();
            $data['user_name']   = input('post.user_name/s');
            $data['user_mobile'] = input('post.user_mobile/s');
            $data['user_email']  = input('post.user_email/s');
            $data['user_pass']   = input('post.user_pass/s');
            $data['user_pid']    = doahangPidGet();
            $data['user_capabilities'] = ['subscriber'];
            $data['user_score'] = 0;
            // 自动登录
            if( $user_id = \daicuo\User::save($data) ){

                \daicuo\User::set_current_user($user_id);
                
                \daicuo\User::set_auth_cookie($user_id);
                
                $this->redirect('daohang/user/index');
            }else{
                $this->success(DcError(\daicuo\User::getError()));
            }
        }
		return $this->fetch();
	}
    
	public function login()
    {
        if( $this->request->isPost() ){
            //验证码
            $this->validateCaptcha(input('post.user_captcha/s'));
            //表单
            $post = [];
            $post['user_name']   = input('post.user_name/s');
            $post['user_pass']   = input('post.user_pass/s');
            $post['user_expire'] = input('post.user_expire/d',0);
            //COOKIE时长延长至30天
            config('common.user_expire', 2592000);
            //登录接口
            if(\daicuo\User::login($post) == false){
                $this->error(\daicuo\User::getError());
            }
            //用户中心
            $this->redirect('daohang/user/index');
        }
        return $this->fetch();
	}
    
	public function logout()
    {
		\daicuo\User::logout();
        
        $this->success(lang('success'), 'daohang/index/index');
	}
    
	public function index()
    {
		return $this->fetch();
	}
    
    /**
     * 验证码验证
     * @param string $captcha 用户输入的验证码
     * @return array 用户信息
     */
    private function validateCaptcha($captcha='')
    {
        if( DcBool(config('common.site_captcha')) ){
            if(captcha_check($captcha) == false){
                $this->error( DcError(lang('user_captcha_error')) );
            }
        }
    }
}