<?php
namespace app\daohang\controller;

use app\common\controller\Front;

class Category extends Front
{

    public function _initialize()
    {
        // 移除标签
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        // 继承上级
        parent::_initialize();
    }
    
    public function index()
    {
        if( isset($this->query['id']) ){
            $info = daohangCategoryId($this->query['id']);
        }elseif( isset($this->query['slug']) ){
            $info = daohangCategorySlug($this->query['slug']);
        }elseif( isset($this->query['name']) ){
            $info = daohangCategoryName($this->query['name']);
        }else{
            $this->error(lang('mustIn'),'daohang/index/index');
        }
        
        if(!$info){
            $this->error(lang('empty'),'daohang/index/index');
        }
        
        //分页路径
        $info['path'] = daohangUrlCategory($info,'[PAGE]');
        
        //地址栏参数
        $info = DcArrayArgs($info, daohangUrlParams('info_order desc,info_update_time'));
        
        //变量赋值
        $this->assign($info);
        
        //加载模板
        return $this->fetch(DcEmpty($info['term_tpl'],'index'));
    }
}