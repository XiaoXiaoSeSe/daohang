{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$info_title|daohangSeo|DcEmpty='呆错导航系统'}</title>
<meta name="keywords" content="{$info_keywords|daohangSeo|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$info_description|daohangSeo|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<script>$('#pagecategory').addClass('active');</script>
<div class="container pt-2">
<div class="card mb-2 py-2 text-center">
  {:adsenseShow("doubi97090")}
</div>
<ol class="breadcrumb bg-white mb-2">
  <li class="breadcrumb-item"><a class="text-danger" href="{:DcUrl('daohang/index/index','','')}">首页</a></li>
  <li class="breadcrumb-item active">{$info_name|DcHtml}</li>
</ol>
<div class="card px-3 pb-3">
  {foreach name=":daohangCategorySelect(['status'=>'normal','sort'=>'term_order','order'=>'desc','result'=>'tree'])" item="category"}
  <h6 class="py-4 border-bottom">
    <a class="btn btn-dark btn-sm" href="{:daohangUrlCategory($category)}" target="_self">{$category.term_name}</a>
  </h6>
  <div class="row dh-row">
    {volist name="category._child" id="son"}
    <div class="col-4 col-md-1 py-2 px-1">
      <a class="btn btn-light btn-block btn-sm" href="{:daohangUrlCategory($son)}" target="_self">{$son.term_name}</a>
    </div>
    {/volist}
  </div>
  {/foreach}
</div>
</div>
{/block}
{block name="footer"}{include file="block/footer" /}{/block}
<!-- -->