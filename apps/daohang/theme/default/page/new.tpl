{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$info_title|daohangSeo|DcEmpty='呆错导航系统'}</title>
<meta name="keywords" content="{$info_keywords|daohangSeo|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$info_description|daohangSeo|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<script>$('#pagenew').addClass('active');</script>
<div class="container pt-2">
<div class="card mb-2 py-2 text-center">
  {:adsenseShow("doubi97090")}
</div>
<ol class="breadcrumb bg-white mb-2">
  <li class="breadcrumb-item"><a class="text-danger" href="{:DcUrl('daohang/index/index','','')}">首页</a></li>
  <li class="breadcrumb-item active">{$info_name|DcHtml}</li>
</ol>
<div class="row dh-row">
{volist name=":daohangCategorySelect(['status'=>'normal','sort'=>'term_order','order'=>'desc','result'=>'tree'])" id="category"}
  <div class="col-12 col-md-3 px-1 mb-2">
    <div class="card">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-flag mr-1 text-danger"></i>{$category.term_name}</span>
        <a class="small text-danger" href="{:daohangUrlCategory($category)}">更多>></a>
      </div>
      <div class="card-body pb-0 px-2">
        {volist name=":daohangSelect(['status'=>'normal','limit'=>'10','sort'=>'info_id','order'=>'desc','term_id'=>['in',DcTermSubIds($category['term_id'],'category','array')]])" id="daohang"}
        <p class="text-truncate">
          {gt name="i" value="3"}
          <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
          {else/}
          <span class="badge dh-badge badge-danger mr-2">{$i}</span>
          {/gt}
          <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml|DcSubstr=0,11,false}</a>
          <small class="float-right text-muted">{$daohang.info_create_time|daohangDate='m.d',###}</small>
        </p>
        {/volist}
      </div> 
    </div>
  </div>
{/volist}
</div>
</div>
{/block}
{block name="footer"}{include file="block/footer" /}{/block}
<!-- -->