{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$info_title|daohangSeo|DcEmpty='呆错导航系统'}</title>
<meta name="keywords" content="{$info_keywords|daohangSeo|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$info_description|daohangSeo|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container pt-2">
<ol class="breadcrumb bg-white mb-2">
  <li class="breadcrumb-item"><a class="text-danger" href="{:DcUrl('daohang/index/index','','')}">首页</a></li>
  <li class="breadcrumb-item active">{$info_name|DcHtml}</li>
</ol>
<div class="bg-white px-3 pb-3">
  <h1 class="display-6 text-truncate py-2">{$info_name|DcHtml}</h1>
  <p class="text-muted text-dh lead">{$info_excerpt|DcHtml}</p>
  <div class="">
      {$info_content|DcEditor}
  </div>
</div>
</div>
{/block}
{block name="footer"}{include file="block/footer" /}{/block}
<!-- -->