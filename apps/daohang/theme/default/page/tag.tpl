{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$info_title|daohangSeo|DcEmpty='呆错导航系统'}</title>
<meta name="keywords" content="{$info_keywords|daohangSeo|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$info_description|daohangSeo|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
{assign name="list" value=":daohangTagSelect(['limit'=>'30','status'=>'normal','sort'=>'term_count desc,term_id','order'=>'desc'])" /}
<script>$('#pagetag').addClass('active');</script>
<div class="container pt-2">
<div class="card mb-2 py-2 text-center">
  {:adsenseShow("doubi97090")}
</div>
<ol class="breadcrumb bg-white mb-2">
  <li class="breadcrumb-item"><a class="text-danger" href="{:DcUrl('daohang/index/index','','')}">首页</a></li>
  <li class="breadcrumb-item active">{$info_name|DcHtml}</li>
</ol>
<div class="row dh-row">
  {volist name="list" id="tag"}
  <div class="col-6 col-md-3 px-1 mb-2">
    <a class="btn btn-light btn-block btn-sm py-3 bg-white" href="{:daohangUrlTag($tag)}">
      <p>
        <strong class="text-danger">{$tag.term_name}</strong>
        <span class="small text-muted">（{$tag.term_count}）</span>
      </p>
      <p class="text-left mb-0 small text-truncate">{$tag.term_info|DcEmpty='关于'.$tag['term_name'].'相关的网站'}</p>
    </a>
  </div>
  {/volist}
  <!---->
</div>
</div>
{/block}
{block name="footer"}{include file="block/footer" /}{/block}
<!-- -->