{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$term_title|DcEmpty=$term_name}</title>
<meta name="keywords" content="{$term_keywords|DcEmpty=$term_name}" />
<meta name="description" content="{$term_description|DcEmpty=$term_name}"  />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container pt-2">
<div class="card mb-2 py-2 text-center">
  {:adsenseShow("doubi97090")}
</div>
<div class="row dh-row">
  <!---->
  <div class="col-12 col-md-9 px-1">
    <ol class="breadcrumb bg-white mb-2">
      <li class="breadcrumb-item"><a class="text-danger" href="{:DcUrl('daohang/index/index','','')}">首页</a></li>
      <li class="breadcrumb-item"><a href="{:daohangUrlPage(['info_slug'=>'tag'])}">标签</a></li>
      <li class="breadcrumb-item active">{$term_name|DcHtml}</li>
    </ol>
    <div class="card mb-2 py-2 px-3">
      <h3 class="text-truncate">{$term_name|DcHtml}</h3>
      <p class="text-muted mb-0">{$term_info|DcEmpty='与'.$term_name.'相关的网站'|DcHtml}，截止 {:date('Y-m-d',time())} 共包含<strong>{$total}</strong>个网站！</p>
    </div>
    <!---->
    {assign name="list" value=":daohangSelect([
      'cache'   => true,
      'status'  =>'normal',
      'sort'    => $sort,
      'order'   => $order,
      'limit'   => $limit,
      'page'    => $page,
      'term_id' => $term_id,
    ])" /}
    <!---->
    <div class="list-group mb-2">
      {volist name="list.data" id="daohang"}
      <li class="list-group-item px-3">
        <h6 class="mt-0 text-truncate">
          <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml}</a>
          <a class="badge badge-danger dh-badge-pink font-weight-normal dh-" href="{$daohang.url_web|DcHtml}" target="_blank">浏览</a>
        </h6>
        <p class="text-muted small">
          {$daohang.info_excerpt|DcHtml}
        </p>
        <div class="w-100 d-flex justify-content-between">
          <ul class="list-inline small mb-0">
            <li class="list-inline-item">收录时间：<label class="text-muted">{$daohang.info_create_time|daohangDate='Y-m-d',###}</label></li>
            <li class="list-inline-item">浏览人数：<label class="text-muted">{$daohang.info_views|number_format}</label></li>
            <li class="list-inline-item">点击次数：<label class="text-muted">{$daohang.info_hits|number_format}</label></li>
            <li class="list-inline-item"><i class="fa fa-fw fa-tags"></i>
              {volist name="daohang.tag" id="tag" offset="0" length="3"}
              <a class="text-muted" href="{:daohangUrlTag($tag)}">{$tag.term_name}</a>
              {/volist}
            </li>
          </ul>
          <div class="small text-muted d-none d-md-inline">
            <a class="text-danger" href="{:daohangUrlInfo($daohang)}">网站详情>></a>
          </div>
        </div>
      </li>
      {/volist}
    </div>
    <!---->
    {gt name="list.last_page" value="1"}
    <div class="border rounded bg-white py-2 d-flex justify-content-center d-md-none mb-2">
      {:DcPageSimple($list['current_page'], $list['last_page'], $path)}
    </div>
    <div class="d-none border rounded bg-white py-2 d-md-flex justify-content-center mb-2">
      {:DcPage($page, $list['per_page'], $list['total'], $path)}
    </div>
    {/gt}
  </div>
  <!---->
  <div class="col-12 col-md-3 px-1">
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-tags mr-1 text-danger"></i>热门标签</span>
        <a class="small text-danger" href="{:daohangUrlPage(['info_slug'=>'tag'])}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0 text-center row dh-row">
        {volist name=":daohangTagSelect(['limit'=>'30','status'=>'normal','sort'=>'term_count','order'=>'desc'])" id="tag"}
        <p class="col-4 px-1">
          <a class="btn btn-light btn-block btn-sm" href="{:daohangUrlTag($tag)}">{$tag.term_name|DcSubstr=0,4,false}</a>
        </p>
        {/volist}
      </div> 
    </div>
    <div class="card mb-2 py-2 text-center">
      {:adsenseShow("doubi250250")}
    </div>
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-flag mr-1 text-danger"></i>最新收录</span>
        <a class="small text-danger" href="{:daohangUrlPage(['info_slug'=>'new'])}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0">
        {volist name=":daohangSelect(['status'=>'normal','limit'=>'10','sort'=>'info_id','order'=>'desc'])" id="daohang"}
        <p class="text-truncate">
          {gt name="i" value="3"}
          <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
          {else/}
          <span class="badge dh-badge badge-danger mr-2">{$i}</span>
          {/gt}
          <a class="text-dh text-dark" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml|DcSubstr=0,12,false}</a>
          <small class="float-right text-muted">{$daohang.info_create_time|daohangDate='m.d',###}</small>
        </p>
        {/volist}
      </div> 
    </div>
  </div>
  <!---->
</div>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="block/footer" /}{/block}