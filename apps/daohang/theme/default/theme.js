/*标签输入组件
window.daicuo.tagsInput = {
    //初始配置
    defaults: {
        selectorInput: '#tag_name',
        selectorClick: '.tag-list',
        maxTags: 8,
        maxChars: 20,
        trimValue: true,
        //itemValue: 'value',
        //itemText: 'text',
        tagClass: 'badge badge-pill- badge-secondary'
    },
    //组件初始化
    init : function(options){
        //合并初始配置
        options = $.extend({}, this.defaults, options);
        //动态加载回调
        window.daicuo.tagsInput.ajaxLoad(function(){
            window.daicuo.tagsInput.tagsInit(options);
            window.daicuo.tagsInput.clickAdd(options.selectorClick, options.selectorInput);
        });
    },
    //插件初始化
    tagsInit: function(options){
        $(options.selectorInput).tagsinput(options);
    },
    //点击添加标签方法
    clickAdd: function(selectorClick, selectorInput){
        $(selectorClick).on('click', document.body, function(){
            $(selectorInput).tagsinput('add', $(this).text());
        });
    },
    // 动态加载插件包
    ajaxLoad: function(callBack) {
        daicuo.ajax.script(["https://lib.baomitu.com/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"], function(){
            daicuo.ajax.css("https://lib.baomitu.com/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css");
            daicuo.tools.callBack(callBack);
        });
    }
};*/


window.daicuo.daohang = {
    init: function(){
        this.dropdownEvent();
        this.searchDropdown();
    },
    dropdownEvent: function(){
        //$('#searchDropdown').dropdown('show');
        $('#searchDropdown').on('show.bs.dropdown', function () {
            //alert(123);
        })
    },
    searchDropdown: function(){
        $('#searchDropdown .dropdown-item').on('click', function(){
            var formAction = $('form[id="search"]').attr('action');
            var formText   = $('form [data-toggle="dropdown"]').text();
            var thisAction = $(this).data('href');
            var thisText   = $(this).text();
            //表单
            $('form[id="search"]').attr('action',thisAction);
            $('form [data-toggle="dropdown"]').text(thisText);
            //当前
            $(this).data('href',formAction);
            $(this).text(formText);
        })
    }
}

//主题JS
$(document).ready(function() { 
    //框架脚本初始化
    window.daicuo.init();
    window.daicuo.captcha.refresh();
    //window.daicuo.table.init();
    //window.daicuo.language.init();
    //window.daicuo.sortable.init();
    
    //主题脚本初始化
    window.daicuo.daohang.init();
});