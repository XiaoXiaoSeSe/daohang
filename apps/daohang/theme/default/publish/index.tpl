{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle|DcEmpty='呆错导航系统免费收录网站'}</title>
<meta name="keywords" content="{$seoKeywords|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$seoDescription|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container pt-2">
<div class="card px-3 pb-3">
  <h2 class="text-center pt-5 pb-3">免费收录网站</h2>
  <p class="text-muted py-3">{:config('common.site_name')}是个优秀网站发布和推荐平台，旨在为搜索引擎和网友提供数据参考，同时为广大站长朋友提供宣传推广自己网站的服务，增加站长朋友的网站访问量，欢迎大家在这里提交你的网站以及你推荐的网站。</p>
  <form class="mb-4" action="{:DcUrl('daohang/publish/save','','')}" method="post" target="_self" data-toggle="form" data-callback="">
    <div class="input-group mb-4">
      <input class="form-control" type="text" name="url" id="url" placeholder="请输入待收藏的网址，http(s)开头" autocomplete="off" required>
      <div class="input-group-append">
        <button class="btn btn-danger" type="submit">提交</button>
      </div>
    </div>
    <div class="form-group">
      <label for="tag_name"><strong><i class="mr-1 fa fa-fw fa-tags"></i>标签</strong></label>
      <input class="form-control" name="tag_name" id="tag_name" type="text" value="" data-toggle="tags">
      <small class="form-text pt-2">
        {volist name=":daohangTags(8)" id="tag"}
        <a class="tag-list text-danger mr-2" href="javascript:;">{$tag}</a>
        {/volist}
      </small>
    </div>
    <div class="form-group row">
      <label class="col-12 mb-2"><strong><i class="mr-1 fa fa-fw fa-list"></i>分类</strong></label>
      {foreach name=":daohangCategorySelect(['module'=>'daohang','result'=>'array'])" item="category" key="checkKey"}
      <div class="col-6 col-md-2">
        <input type="checkbox" id="category_{$category.term_id}" name="category_id[]" value="{$category.term_id}">
        <label for="category_{$category.term_id}" class="small text-muted">{$category.term_name|DcSubstr=0,6,false}</label>
      </div>
      {/foreach}
    </div>
  </form>
  <section class="mb-4">
    <h6 class="border-bottom pb-3 mb-3">{:config('common.site_name')}收录条款</h6>
    <p class="small text-muted">1、只收录拥有顶级域名的网站；</p>
    <p class="small text-muted">2、不收录非法或灰色边缘网站；</p>
    <p class="small text-muted">3、只收录页面制作精良的网站；</p>
    <p class="small text-muted">3、务必在当天做好{:config('common.site_name')}的连接；</p>
    <p class="small text-muted">4、不收录没做好友情连接的网站；</p>
  </section>
  <section class="mb-0">   
    <h6 class="border-bottom pb-3 mb-3">{:config('common.site_name')}链接信息</h6>
    <p class="small text-muted">网站名称：{:config('common.site_name')}</p>
    <p class="small text-muted">网站地址：{$domain}</p>
  </section>
</div>
</div>
{/block}
{block name="footer"}{include file="block/footer" /}{/block}