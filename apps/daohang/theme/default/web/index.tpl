{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$info_title|DcEmpty=$info_name}</title>
<meta name="keywords" content="{$info_keywords|DcEmpty=$info_name}" />
<meta name="description" content="{$info_description|DcEmpty=$info_name}"  />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<div class="bg-white py-5 mb-2">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-10 order-2 order-md-1">
        <h2 class="mb-3 text-truncate">{$info_name|DcHtml}</h2>
        <p class="text-dh text-muted mb-0">{$info_excerpt|DcHtml}</p>
      </div>
      <div class="col-12 col-md-2 text-center order-1 order-md-2 mb-3 col-mb-0">
        <img class="img-thumbnail rounded-circle img-title" src="{:daohangUrlImage($image_level,$path_root)}" alt="{$info_name|DcHtml}">
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row dh-row">
    <div class="col-12 col-md-9 px-1">
      <ol class="breadcrumb bg-white mb-2">
        <li class="breadcrumb-item"><a class="text-danger" href="{:DcUrl('daohang/index/index','','')}">首页</a></li>
        {volist name="category" id="dh"}
        <li class="breadcrumb-item"><a href="{:daohangUrlCategory($dh)}">{$dh.term_name}</a></li>
        {/volist}
        <li class="breadcrumb-item active">站点详细</li>
      </ol>
      <div class="card mb-2">
        <div class="card-header bg-white">
          <i class="fa fa-fw fa-flag mr-1 text-danger"></i>网站相关信息
        </div>
        <div class="card-body pb-0">
          <div class="row">
            <p class="col-12 text-truncate">网站地址：<a class="text-danger" href="{:DcHtml($url_web)}" target="_blank">{$url_web|DcStrip}</a></p>
            <p class="col-12 col-md-6 text-truncate">网站名称：{$info_name|DcHtml}</p>
            <p class="col-12 col-md-6">收录时间：{$info_create_time}</p>
            <p class="col-12 col-md-6">浏览次数：{$info_views}</p>
            <p class="col-12 col-md-6">出站次数：{$info_hits}</p>
            <p class="col-12 col-md-6">网站不错：<i class="fa fa-fw fa-thumbs-o-up"></i>{$info_up|default=0}</p>
            <p class="col-12 col-md-6">网站垃圾：<i class="fa fa-fw fa-thumbs-o-down"></i>{$info_down|default=0}</p>
            <p class="col-12 col-md-6">网站微章：{$info_hits}</p>
            <p class="col-12">网站介绍：{$info_content|DcStrip}</p>
          </div>
        </div> 
      </div>
      <div class="card mb-2 py-2 text-center">
        {:adsenseShow("doubi72890")}
      </div>
      <div class="card mb-2">
        <div class="card-header bg-white">
          <i class="fa fa-fw fa-flag mr-1 text-danger"></i>免责申明
        </div>
        <div class="card-body text-dh pb-0">
          <p>1、本文数据来源于{$info_name|DcHtml}（{$url_web|DcDomain}）版权归原网站所有。</p>
          <p>2、本站收录{$info_name|DcHtml}时该网站内容都正常，如失效，请联系网站管理员处理。</p>
          <p>3、本站仅提供{$info_name|DcHtml}的信息展示平台，不承担相关法律责任。</p>
          <p>4、本站不接受违法信息，如有违法内容，请立即举报。</p>
          <p>6、本文地址 {$domain}{:daohangUrlInfo(['info_id'=>$info_id,'info_slug'=>$info_slug,'info_name'=>$info_name])}，复制请保留版权链接。</p>
        </div>
      </div>
    </div>
    <!---->
    <div class="col-12 col-md-3 px-1">
      <div class="card mb-2 py-2 text-center">
        {:adsenseShow("doubi250250")}
      </div>
      <div class="card">
        <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
          <span><i class="fa fa-fw fa-flag mr-1 text-danger"></i>最新收录</span>
          <a class="small text-danger" href="{:daohangUrlPage(['info_slug'=>'new'])}">更多>></a>
        </div>
        <div class="card-body px-2 pb-0">
          {volist name=":daohangSelect(['term_id'=>['in',$category_id],'status'=>'normal','limit'=>'10','sort'=>'info_id','order'=>'desc'])" id="daohang"}
          <p class="text-truncate">
          {gt name="i" value="3"}
          <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
          {else/}
          <span class="badge dh-badge badge-danger mr-2">{$i}</span>
          {/gt}
          <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml|DcSubstr=0,12,false}</a>
          <small class="float-right text-muted">{$daohang.info_create_time|daohangDate='m.d',###}</small>
          </p>
          {/volist}
        </div>  
      </div>
    </div>
  </div>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="block/footer" /}{/block}