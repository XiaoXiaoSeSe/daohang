{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle|DcEmpty='呆错导航系统'}</title>
<meta name="keywords" content="{$seoKeywords|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$seoDescription|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container pt-2">
<div class="card mb-2 py-2 text-center">
  {:adsenseShow("doubi97090")}
</div>
<!---->
{assign name="list" value=":daohangSelect([
  'cache'   => true,
  'status'  =>'normal',
  'sort'    => $sort,
  'order'   => $order,
  'limit'   => $limit,
  'page'    => $page,
  //'search'  => $search,
  'whereOr' => ['info_name'=>['like','%'.$search.'%']],
  'meta_query'  => [
    [
      'key'   => ['eq','url_web'],
      'value' => ['like','%'.$search.'%']
    ],
  ],
])" /}
<!---->
<div class="row dh-row">
  <div class="col-12 col-md-9 px-1">
    <div class="card mb-2 py-3 px-4">
      <form class="w-100 mx-auto" action="{:daohangUrlSearch('','index')}" method="post" target="_self">
      <div class="input-group">
        <input class="form-control" type="text" name="searchText" id="searchText" value="{$search}" placeholder="网站关键词、描述" autocomplete="off" required>
        <div class="input-group-append">
          <button class="btn btn-danger" type="submit"><i class="fa fa-search mr-1"></i>搜索</button>
        </div>
      </div>
      </form>
    </div>
    <div class="card mb-2 px-4 pt-3">
      <ul class="nav nav-pills mb-3">
        <li class="nav-item">
          <a class="nav-link active" href="{$searchIndex}">综合</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{$searchWeb}">网址</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{$searchBaidu}" target="_blank">百度</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{$searchSogou}" target="_blank">搜狗</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{$searchToutiao}" target="_blank">头条</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{$searchBing}" target="_blank">必应</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{$searchSo}" target="_blank">360搜索</a>
        </li>
      </ul>
      <div class="mb-3 text-muted">
        {:config('common.site_name')}为您找到相关结果约<strong class="mx-1">{$list.total|default=0}</strong>个
      </div>
      {foreach $list['data'] as $daohang}
      <h6 class="mt-0 text-truncate">
        <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml}</a>
      </h6>
      <p class="mb-2">
        {$daohang.info_excerpt|DcHtml|DcSubstr=0,140,true}
      </p>
      <p class="text-muted small mb-4">
        <a class="text-dark mr-2" href="{$daohang.url_web|DcHtml}" target="_blank">{$daohang.url_web|DcHtml}</a>
        <a class="text-danger" href="{:daohangUrlInfo($daohang)}">网站详情</a>
      </p>
      {/foreach}
    </div>
    <!---->
    {gt name="list.last_page" value="1"}
    <div class="border rounded bg-white py-2 d-flex justify-content-center d-md-none mb-2">
      {:DcPageSimple($list['current_page'], $list['last_page'], $path)}
    </div>
    <div class="d-none border rounded bg-white py-2 d-md-flex justify-content-center mb-2">
      {:DcPage($list['current_page'], $list['per_page'], $list['total'], $path)}
    </div>
    {/gt}
  </div>
  <!---->
  <div class="col-12 col-md-3 px-1">
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-tags mr-1 text-danger"></i>热门标签</span>
        <a class="small text-danger" href="{:daohangUrlPage(['info_slug'=>'tag'])}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0 text-center row dh-row">
        {volist name=":daohangTagSelect(['limit'=>'30','status'=>'normal','sort'=>'term_count','order'=>'desc'])" id="tag"}
        <p class="col-4 px-1">
          <a class="btn btn-light btn-block btn-sm" href="{:daohangUrlTag($tag)}">{$tag.term_name|DcSubstr=0,4,false}</a>
        </p>
        {/volist}
      </div> 
    </div>
    <div class="card mb-2 py-2 text-center">
      {:adsenseShow("doubi250250")}
    </div>
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-flag mr-1 text-danger"></i>最新收录</span>
        <a class="small text-danger" href="{:daohangUrlPage(['info_slug'=>'new'])}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0">
        {volist name=":daohangSelect(['status'=>'normal','limit'=>'10','sort'=>'info_id','order'=>'desc'])" id="daohang"}
        <p class="text-truncate">
          {gt name="i" value="3"}
          <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
          {else/}
          <span class="badge dh-badge badge-danger mr-2">{$i}</span>
          {/gt}
          <a class="text-dh text-dark" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml|DcSubstr=0,10,false}</a>
          <small class="float-right text-muted">{$daohang.info_create_time|daohangDate='m.d',###}</small>
        </p>
        {/volist}
      </div> 
    </div>
  </div>
  <!---->
</div>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="block/footer" /}{/block}