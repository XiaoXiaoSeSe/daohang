{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>用户中心-{:config('common.site_name')}</title>
<meta name="keywords" content="{:config('daohang.index_keywords')}" />
<meta name="description" content="{:config('daohang.index_description')}" />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!-- -->
{block name="main"}
<div class="container pt-5 pt-md-3 mb-3">
<div class="row dh-row">
  <div class="col-12 col-md-3 px-1">
    {include file="block/navbar_user" /}
  </div>
  <div class="col-12 col-md-9 px-1">
    <div class="card">
    <div class="card-header h5">用户资料</div>
    <div class="card-body">
      <table class="table table-bordered bg-white mb-0">
        <tbody>
          <tr>
            <td>用户名</td>
            <td>{$user.user_name}</td>
          </tr>
          <tr>
            <td>呢称</td>
            <td>{$user.user_nice_name}</td>
          </tr>
          <tr>
            <td>邮箱</td>
            <td>{$user.user_email}</td>
          </tr>
          <tr>
            <td>手机</td>
            <td>{$user.user_mobile}</td>
          </tr>
          <tr>
            <td>用户组</td>
            <td>{foreach $user['user_capabilities'] as $group} {$group|lang}{/foreach}</td>
          </tr>
          <tr>
            <td>状态</td>
            <td>{$user.user_status_text}</td>
          </tr>
          <tr>
            <td>加入时间</td>
            <td>{$user.user_create_time}</td>
          </tr>
          <tr>
            <td>最后登录</td>
            <td>{$user.user_update_time}</td>
          </tr>
        </tbody>
      </table>
    </div> 
  </div>
  </div> <!--col-12 end-->
</div> <!-- row end-->
</div> <!-- /container -->
{/block}
<!-- -->
{block name="footer"}{include file="block/footer" /}{/block}