{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>用户登录-{:config('common.site_name')}</title>
<meta name="keywords" content="{:config('daohang.index_keywords')}" />
<meta name="description" content="{:config('daohang.index_description')}" />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container pt-2">
<div class="bg-white rounded p-md-5">
<div class="row dh-row">
<div class="col-12 col-md-6 offset-md-3 px-1">
    <div class="card">
      <h6 class="card-header text-center">用户登录</h6>
      <div class="card-body px-md-5">
        <form action="{:DcUrl('daohang/user/login','','')}" method="post" data-toggle="" target="_self" data-callback="">
          <p class="card-title">帐号</p>
          <p class="card-text mb-4">
            <input type="text" class="form-control form-control-sm" name="user_name" placeholder="用户名/邮箱/手机" required="true" autocomplete="off">
          </p>
          <p class="card-title">密码</p>
          <p class="card-text mb-4">
            <input type="password" class="form-control form-control-sm" name="user_pass" placeholder="您的密码" required="true" maxlength="60">
          </p>
          {if DcBool(config('common.site_captcha'))}
          <p class="card-title">验证码</p>
          <p class="card-text mb-4">
            <input type="text" class="form-control form-control-sm" name="user_captcha" placeholder="验证码" value="" autocomplete="off" required="true">
          </p>
          <p class="border rounded py-2 mb-3 text-center">
            <img class="img-fluid" id="captcha" src="../../public/images/x.gif" alt="{:lang('user_captcha')}" data-toggle="captcha"/>
          </p>
          {/if}
          <p class="card-text small">
            <input type="checkbox" name="user_expire" checked="checked" value="1"> 保持登录 
          </p>
          <p class="card-text">
            <button class="btn btn-block btn-dark" type="submit">登录</button>
          </p>
        </form>
      </div>
      <div class="card-footer text-center">
        <a class="small mx-1 text-dark text-decoration-none" href="javascript:;">
          <i class="fa fa-fw fa-user-o"></i> 忘记密码
        </a>
        <a class="small mx-1 text-dark text-decoration-none" href="{:DcUrl('daohang/user/register','','')}">
          <i class="fa fa-fw fa-user"></i> 免费注册
        </a>
      </div>
    </div>
</div>
</div>
</div>
</div>
{/block}
{block name="footer"}{include file="block/footer" /}{/block}