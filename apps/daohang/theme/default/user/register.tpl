{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>免费注册-{:config('common.site_name')}</title>
<meta name="keywords" content="{:config('daohang.index_keywords')}" />
<meta name="description" content="{:config('daohang.index_description')}" />
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!-- -->
{block name="main"}
<div class="container pt-2">
<div class="bg-white rounded p-md-5">
<div class="row dh-row">
<div class="col-12 col-md-6 offset-md-3 px-1">
    <div class="card">
      <h6 class="card-header text-center">用户注册</h6>
      <div class="card-body px-md-5">
        <form action="{:DcUrl('daohang/user/register','','','')}" method="post">
          <p class="card-title">用户名</p>
          <p class="card-text mb-4">
            <input type="text" class="form-control form-control-sm" name="user_name" placeholder="用户名" value="" required="true" autocomplete="off">
          </p>
          <p class="card-title">手机号</p>
          <p class="card-text mb-4">
            <input type="tel" class="form-control form-control-sm" name="user_mobile" placeholder="常用手机号" value="" required="true" autocomplete="off">
          </p>
          <p class="card-title">邮箱</p>
          <p class="card-text mb-4">
            <input type="email" class="form-control form-control-sm" name="user_email" placeholder="常用邮箱" value="" required="true" autocomplete="off">
          </p>
          <p class="card-title">密码</p>
          <p class="card-text mb-4">
            <input type="password" class="form-control form-control-sm" name="user_pass" placeholder="您的密码" value="" required="true" maxlength="60">
          </p>
         {if DcBool(config('common.site_captcha'))}
          <p class="card-title">验证码</p>
          <p class="card-text mb-4">
            <input type="text" class="form-control form-control-sm" name="user_captcha" placeholder="验证码" value="" autocomplete="off" required="true">
          </p>
          <p class="border rounded py-2 mb-3 text-center">
            <img class="img-fluid" id="captcha" src="../../public/images/x.gif" alt="{:lang('user_captcha')}" data-toggle="captcha"/>
          </p>
          {/if}
          <p class="card-text">
            <button class="btn btn-block btn-dark" type="submit">免费注册</button>
           </p>
        </form>
      </div>
      <div class="card-footer text-center">
        <a class="small mx-1 text-dark text-decoration-none" href="javascript:;">
          <i class="fa fa-fw fa-user-o"></i> 忘记密码
        </a>
        <a class="small mx-1 text-dark text-decoration-none" href="{:DcUrl('daohang/user/login','','')}">
          <i class="fa fa-fw fa-sign-in"></i> 帐号登录
        </a>
      </div>
    </div>
</div>
</div>
</div>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="block/footer" /}{/block}