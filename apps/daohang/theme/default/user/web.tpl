{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>发布网址-{:config('common.site_name')}</title>
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!-- -->
{block name="main"}
<section class="container pt-5 pt-md-3 mb-3">
<div class="row dh-row">
  <div class="col-12 col-md-3 px-1">
    {include file="block/navbar_user" /}
  </div>
  <div class="col-12 col-md-9 px-1">
    <div class="card">
      <div class="card-header">发布网址</div>
      <div class="card-body">
      {:DcBuildForm([
        'name'     => 'daohang_user_web',
        'class'    => 'bg-white',
        'action'   => DcUrl('daohang/publish/saveWeb', '', ''),
        'method'   => 'post',
        'submit'   => lang('submit'),
        'reset'    => lang('reset'),
        'close'    => false,
        'ajax'     => true,
        'disabled' => false,
        'callback' => '',
        'data'     => '',
        'items'    => DcFormItems([
            'info_name'     => ['type'=>'text','class_left'=>'col-12','class_right'=>'col-12','maxlength'=>'250','required'=>true],
            'info_slug'     => ['type'=>'text','class_left'=>'col-12','class_right'=>'col-12','maxlength'=>'60','required'=>true],
            'url_web'       => ['type'=>'text','class_left'=>'col-12','class_right'=>'col-12','maxlength'=>'250','required'=>true],
            'image_level'   => ['type'=>'image','class_left'=>'col-12','class_right'=>'col-12'],
            'info_excerpt'  => ['type'=>'text','class_left'=>'col-12','class_right'=>'col-12'],
            'info_content'  => ['type'=>'editor','class_left'=>'col-12','class_right'=>'col-12','rows'=>10],
            'tag_name'      => ['type'=>'tags','class_left'=>'col-12','class_right'=>'col-12','option'=>daohangTags(10)],
            'category_id[]' => ['type'=>'select','class_left'=>'col-12','class_right'=>'col-12','multiple'=>true,'size'=>10,'option'=> DcTermCheck(['module'=>'daohang'])],
        ]),
    ])}
    </div> 
    </div>
  </div>
</div>
</section>
{/block}
<!-- -->
{block name="footer"}{include file="block/footer" /}{/block}