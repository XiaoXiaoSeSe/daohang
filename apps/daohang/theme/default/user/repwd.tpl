{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>修改密码-{:config('common.site_name')}</title>
{/block}
<!-- -->
{block name="header"}{include file="block/header" /}{/block}
<!-- -->
{block name="main"}
<section class="container pt-5 pt-md-3 mb-3">
<div class="row dh-row">
  <div class="col-12 col-md-3 px-1">
    {include file="block/navbar_user" /}
  </div>
  <div class="col-12 col-md-9 px-1">
    <div class="card">
      <div class="card-header">修改密码</div>
      <div class="card-body px-md-5">
      {:DcBuildForm([
        'name'     => 'daohang_user_repwd',
        'class'    => 'bg-white',
        'action'   => DcUrl('daohang/user/repwd', '', ''),
        'method'   => 'post',
        'submit'   => lang('submit'),
        'reset'    => lang('reset'),
        'close'    => false,
        'ajax'     => true,
        'disabled' => false,
        'callback' => '',
        'data'     => '',
        'items'    => DcFormItems([
            'user_pass_old'     => ['type'=>'text','class_left'=>'col-12','class_right'=>'col-12','maxlength'=>'60','required'=>true],
            'user_pass'         => ['type'=>'text','class_left'=>'col-12','class_right'=>'col-12','maxlength'=>'60','required'=>true],
            'user_pass_confirm' => ['type'=>'text','class_left'=>'col-12','class_right'=>'col-12','maxlength'=>'60','required'=>true],
        ]),
    ])}
    </div> 
    </div>
  </div>
</div>
</section>
{/block}
<!-- -->
{block name="footer"}{include file="block/footer" /}{/block}