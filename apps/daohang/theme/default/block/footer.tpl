{if config("dh.footer_pc")}
<div class="container text-center mb-2">
  <div class="bg-white py-2 rounded">{:posterParse("dh.footer_pc")}</div>
</div>
{/if}

<hr class="bg-muted mb-4" style="height:2px">

<!-- -->
<div class="container">

<div class="row">
{volist name=":json_decode(config('dh.link_footer'),true)" id="dc" offset="0" length="12"}
<div class="col-6 col-md-4">
  <h6><a class="text-muted" href="{$dc.url}" target="{$dc.target|default='_blank'}">{$dc.title}</a></h6>
  <p class="small">{$dc.describe}</p>
</div>
{/volist}
</div>

<!-- -->
<p class="text-md-center small">
  {volist name=":daohangCategorySelect(['parent'=>['eq',0],'limit'=>20])" id="category" offset="0" length="12"}
  <a class="text-muted" href="{:daohangUrlCategory($category)}">{$category.term_name|DcSubstr=0,5,false}</a>
  {/volist}
</p>

<!-- -->
<p class="text-md-center mb-0">
  Copyright © 2020-2021 {:config('common.site_domain')} All rights reserved
</p>

</div>