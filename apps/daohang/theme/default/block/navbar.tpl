<div class="container">
  <a class="navbar-brand" href="{:DcUrl('daohang/index/index','','')}">{:config('common.site_name')}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="nav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item {:DcDefault($module.$controll.$action, 'daohangindexindex', 'active', '')}">
        <a class="nav-link" href="{:DcUrl('daohang/index/index','','')}">首页 <span class="sr-only">(current)</span></a>
      </li>
      {volist name=":daohangNavSelect(['controll'=>'navbar','action'=>'header'])" id="daohang" offset="0" length="10"}
      {if $daohang['_child']}
        <li class="position-relative nav-item {:DcDefault($module.$controll.$action, $daohang['nav_active'], 'active', '')}" id="{$daohang.nav_active}">
          <a class="nav-link dropdown-toggle" href="javascript:;" data-toggle="dropdown">{$daohang.nav_text|DcSubstr=0,5,false}</a>
          <div class="dropdown-menu">
            {volist name="daohang._child" id="navSon"}
            <a class="dropdown-item" href="{$navSon.nav_link}" target="{$navSon.nav_target}">{$navSon.nav_text|DcSubstr=0,5,false}</a>
            {/volist}
          </div>
        </li>
      {else/}
        <li class="nav-item {:DcDefault($module.$controll.$action, $daohang['nav_active'], 'active', '')}" id="{$daohang.nav_active}">
          <a class="nav-link" href="{$daohang.nav_link}" target="{$daohang.nav_target}">{$daohang.nav_text|DcSubstr=0,5,false}</a>
        </li>
      {/if}
      {/volist}
      <!---->
      {if $user['user_id']}
      <li class="nav-item mx-1">
        <a class="nav-link {:DcDefault($controll.$action,'userindex','active')}" href="{:DcUrl('daohang/user/index','','')}" title="用户中心">会员中心</a>
      </li>
      <li class="nav-item ml-1 ml-md-0">
        <a class="nav-link" href="{:DcUrl('daohang/user/logout','','')}" title="安全退出">安全退出</a>
      </li>
      {else /}
      <li class="nav-item ml-1 ml-md-0">
        <a class="nav-link {:DcDefault($action,'login','active')}" href="{:DcUrl('daohang/user/login','','')}" title="登录">登录</a>
      </li>
      <li class="nav-item mx-1">
        <a class="nav-link {:DcDefault($action,'register','active')}" href="{:DcUrl('daohang/user/register','','')}" title="注册">注册</a>
      </li>
      {/if}
    </ul>
  </div>
</div>