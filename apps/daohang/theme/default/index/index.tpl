{extend name="apps/common/view/front.tpl" /}
<!-- -->
{block name="header_meta"}
<title>{$seoTitle|DcEmpty='呆错导航系统'}</title>
<meta name="keywords" content="{$seoKeywords|DcEmpty='呆错导航系统,daiduodaohang'}" />
<meta name="description" content="{$seoDescription|DcEmpty='呆错导航系统是一款免费开源的专业分类导航建站系统。'}"  />
{/block}
{block name="header"}{include file="block/header" /}{/block}
<!--main -->
{block name="main"}
<div class="container pt-2 mb-2">
  <div class="bg-gradient rounded">
    <div class="row dh-row">
      <div class="col-12 col-md-8 offset-md-2 px-1">
        <form class="mx-auto" id="search" action="{:daohangUrlSearch('','index')}" method="post" target="_blank">
          <div class="input-group mb-3">
            <input class="form-control" type="text" name="searchText" id="searchText" placeholder="网站关键词、描述" autocomplete="off" required>
            <div class="input-group-prepend position-relative" id="searchDropdown">
              <button class="btn btn-dark dropdown-toggle" type="button" data-toggle="dropdown">站内</button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="javascrit:;" data-href="{:daohangUrlSearch('','web')}">网址</a>
                <a class="dropdown-item" href="javascrit:;" data-href="{:daohangUrlSearch('','baidu')}">百度</a>
                <a class="dropdown-item" href="javascrit:;" data-href="{:daohangUrlSearch('','sogou')}">搜狗</a>
                <a class="dropdown-item" href="javascrit:;" data-href="{:daohangUrlSearch('','toutiao')}">头条</a>
                <a class="dropdown-item" href="javascrit:;" data-href="{:daohangUrlSearch('','bing')}">必应</a>
                <a class="dropdown-item" href="javascrit:;" data-href="{:daohangUrlSearch('','so')}">360</a>
              </div>
            </div>
            <div class="input-group-append">
              <button class="btn btn-danger" type="submit"><i class="fa fa-search mr-1"></i>搜索</button>
            </div>
          </div>
          <p class="d-none d-md-flex flex-row justify-content-between align-items-center">
          {volist name=":daohangSelect(['status'=>'normal','limit'=>'6','sort'=>'info_order','order'=>'desc'])" id="daohang"}
          <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcSubstr=0,8,false}</a>
          {/volist}
          </p>
        </form>
      </div>
    </div>
  </div>
</div>
<!---->
<div class="container">
<div class="row dh-row">
  <div class="col-12 col-md-3 px-1 order-2">
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-flag mr-1 text-danger"></i>最新收录</span>
        <a class="small text-danger" href="{:daohangUrlPage(['info_slug'=>'new'])}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0">
        {volist name=":daohangSelect(['status'=>'normal','limit'=>'10','sort'=>'info_id','order'=>'desc'])" id="daohang"}
        <p class="text-truncate">
          {gt name="i" value="3"}
          <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
          {else/}
          <span class="badge dh-badge badge-danger mr-2">{$i}</span>
          {/gt}
          <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml|DcSubstr=0,12,false}</a>
          <small class="float-right text-muted">{$daohang.info_create_time|daohangDate='m.d',###}</small>
        </p>
        {/volist}
      </div>
    </div>
    <div class="card mb-2 py-2 text-center">
      {:adsenseShow("doubi250250")}
    </div>
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-flag mr-1 text-danger"></i>热门网址</span>
        <a class="small text-danger" href="{:daohangUrlPage(['info_slug'=>'top'])}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0">
        {volist name=":daohangSelect(['status'=>'normal','limit'=>'10','sort'=>'info_views','order'=>'desc'])" id="daohang"}
        <p class="text-truncate">
          {gt name="i" value="3"}
          <span class="badge dh-badge badge-secondary mr-2">{$i}</span>
          {else/}
          <span class="badge dh-badge badge-danger mr-2">{$i}</span>
          {/gt}
          <a class="{$daohang.info_color|daohangColor}" href="{:daohangUrlInfo($daohang)}">{$daohang.info_name|DcHtml|DcSubstr=0,12,false}</a>
          <small class="float-right text-muted">{$daohang.info_views|number_format}</small>
        </p>
        {/volist}
      </div> 
    </div>
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-tags mr-1 text-danger"></i>热门标签</span>
        <a class="small text-danger" href="{:daohangUrlPage(['info_slug'=>'tag'])}">更多>></a>
      </div>
      <div class="card-body px-2 pb-0 text-center row dh-row">
        {volist name=":daohangTagSelect(['limit'=>'30','status'=>'normal','sort'=>'term_count','order'=>'desc'])" id="tag"}
        <p class="col-4 px-1">
          <a class="btn btn-light btn-block btn-sm" href="{:daohangUrlTag($tag)}">{$tag.term_name|DcSubstr=0,4,false}</a>
        </p>
        {/volist}
      </div> 
    </div>
  </div>
  <!---->
  <div class="col-12 col-md-9 px-1 order-1">
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-tags mr-1 text-danger"></i>推荐网站</span>
        <span>{:adsenseShow("doubiText")}</span>
      </div>
      <div class="card-body row ro-dh pb-0">
        {volist name=":daohangSelect(['limit'=>'30'])" id="daohang"}
        <p class="col-6 col-md-2 px-1 text-truncate text-center">
          <a class="text-dark" href="{:daohangUrlInfo($daohang)}" target="_self">{$daohang.info_name|DcHtml|DcSubstr=0,8,false}</a>
        </p>
        {/volist}
      </div> 
    </div>
    <div class="card mb-2 py-2 text-center">
      {:adsenseShow("doubi72890")}
    </div>
    <div class="card mb-2">
      <div class="card-header px-2 bg-white d-flex flex-row justify-content-between align-items-center">
        <span><i class="fa fa-fw fa-tags mr-1 text-danger"></i>网站分类</span>
        <a class="text-danger small" href="{:daohangUrlPage(['info_slug'=>'category'])}" target="_self">全部>></a>
      </div>
      <div class="card-body pb-0">
        {volist name=":daohangCategorySelect(['status'=>'normal','sort'=>'term_order desc term_id','order'=>'desc'])" id="category"}
        <p class="text-truncate d-flex flex-row justify-content-between align-items-center">
          <a class="btn btn-light btn-sm dh-btn" href="{:daohangUrlCategory($category)}" target="_self">{$category.term_name}</a>
          {volist name=":daohangSelect(['limit'=>'6','sort'=>'info_id','order'=>'desc','term_id'=>$category['term_id']])" id="daohang"}
          <a class="{$daohang.info_color|daohangColor} d-none d-md-inline" href="{:daohangUrlInfo($daohang)}" target="_self">{$daohang.info_name|DcHtml|DcSubstr=0,6,false}</a>
          {/volist}
          <a class="text-muted" href="{:daohangUrlCategory($category)}" target="_self">更多>></a>
        </p>
        {/volist}
      </div> 
    </div>
  </div>
</div>
</div>
{/block}
<!-- -->
{block name="footer"}{include file="block/footer" /}{/block}