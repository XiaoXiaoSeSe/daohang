<?php
namespace app\daohang\event;

class Sql
{
    private $validateName = '';
    
    private $validateScene = '';
    
    private $slugUnique = '';
    
    /**
    * 安装时触发/通常用于数据库操作或调用接口
    * @return bool 只有返回true时才会往下执行
    */
	public function install()
    {
        //批量添加权限
        $this->insertAuth();
        
        //批量添加分类
        $this->insertCategory();
        
        //批量添加标签
        $this->insertTag();
        
        return true;
	}
    
    /**
    * 升级时触发/通常用于数据库操作
    * @return bool 只有返回true时才会往下执行
    */
    public function upgrade()
    {
        return true;
    }
    
    /**
    * 卸载时触发/通常用于数据库操作
    * @return bool 只有返回true时才会往下执行
    */
    public function unInstall()
    {
        //删除配置
        \daicuo\Op::delete_module('daohang');
        //删除分类
        \daicuo\Term::delete_module('daohang');
        //删除用户
        \daicuo\User::delete_module('daohang');
        //返回结果
        return true;
	}
    
    //批量添加初始权限
    private function insertAuth()
    {
        \daicuo\Op::delete_all(['op_module'=>'daohang','op_controll'=>'system','op_action'=>'auth']);
        
        return \daicuo\Op::save_all([
            [
                'op_name'       => 'guest',
                'op_value'      => 'daohang/index/index',
                'op_module'     => 'daohang',
                'op_controll'   => 'system',
                'op_action'     => 'auth',
                'op_order'      => 0,
                'op_status'     => 'normal',
            ],
        ]);
    }
    
    //批量添加初始分类
    private function insertCategory()
    {
        //数据验证
        $this->insertValidate('common/Term', 'save', ['term_type'=>['eq','category']]);
        
        //添加一级分类
        daohangTermSave('category', ['生活服务','休闲娱乐','教育文化','医疗健康','体育健身','网络科技','网上购物','政府组织','综合其它']);
        
        //添加二级分类
        daohangTermSave('category', ['手机数码','应用市场','云盘相册','IT综合','科技资讯','数据分析','虚拟现实','网络安全','站长资源','软件下载','硬件设备','设计素材','技术编程','域名主机','聊天通信','创业投资','广告联盟','搜索引擎'], '网络科技');
        
        return true;
    }
    
    //批量添加初始标签
    private function insertTag()
    {
        //数据验证
        $this->insertValidate('common/Term', 'save', ['term_type'=>['eq','category']]);
        //添加标签
        return daohangTermSave('tag', ['公众号','小程序','个人微信','QQ','抖音','快手','腾讯','阿里','百度','今日头条','美团']);
    }
    
    //插入数据前（表单验证/验证场景/别名唯一值）
    private function insertValidate($validateName='', $validateScene='', $slugUnique=[]){
        //定义验证名称
        config('common.validate_name', $validateName);
        //定义验证场景
        config('common.validate_scene', $validateScene);
        //定义别名唯一值
        config('common.where_slug_unique', $slugUnique);
    }
	
}