<?php
namespace app\daohang\event;

use think\Controller;

class Auth extends Controller
{
	
    public function _initialize()
    {
        parent::_initialize();
    }

	public function index()
    {
        $assign = [];
        $assign['optionThemes']  = DcThemeOption('daohang');
        $assign['tipsValue']     = '[:id] [:slug] [:name] ';
        $assign['tipsSearch']    = '[:searchText] ';
        $assign['tipsPage']      = '[:pageNumber] [:pageSize] [:sortName] [:sortNumber]';
        $assign['tipsClass']     = 'col-12 col-md-6 offset-md-2 pt-2 form-text text-muted small';
        $this->assign($assign);
        return $this->fetch('daohang@auth/index');
	}
    
    public function update()
    {
        $post = [];
        $post['auth_search']    = input('post.auth_search/a');
        $post['auth_status']    = input('post.auth_status/a');
        $post['auth_save']      = input('post.auth_save/a');
        $post['auth_save_web']  = input('post.auth_save_web/a');
        //保存权限定义
		if( !\daicuo\Op::write($post, 'daohang', 'auth', 'update', 0 ,'yes') ){
		    $this->error(lang('fail'));
        }
        //清除后台手动配置的权限
        \daicuo\Op::delete_all(['op_module'=>'daohang','op_controll'=>'admin','op_action'=>'auth']);
        //批量添加权限
        $authList = [];
        foreach($post['auth_save'] as $key=>$value){
            array_push($authList,[
                'op_name'       => $value,
                'op_value'      => 'daohang/publish/save',
                'op_module'     => 'daohang',
                'op_controll'   => 'admin',
                'op_action'     => 'auth',
                'op_order'      => 0,
                'op_status'     => 'normal',
            ]);
        }
        foreach($post['auth_save_web'] as $key=>$value){
            array_push($authList,[
                'op_name'       => $value,
                'op_value'      => 'daohang/publish/saveWeb',
                'op_module'     => 'daohang',
                'op_controll'   => 'admin',
                'op_action'     => 'auth',
                'op_order'      => 0,
                'op_status'     => 'normal',
            ]);
        }
        \daicuo\Op::save_all($authList);
        //清空缓存
        DcCache('auth_all', null);
        //返回结果
        $this->success(lang('success'));
	}
	
}