<?php
namespace app\daohang\event;

use think\Controller;

class Page extends Controller
{
    
    public function create()
    {
        $this->assign('items', $this->fields('create'));
        
        return $this->fetch('daohang@page/create');
    }
    
    public function save()
    {
        if( !daohangPageSave(input('post.')) ){
          $this->error(\daicuo\Info::getError());
        }
        $this->success(lang('success'));
    }
    
    public function delete()
    {
        daohangDelete(input('id/a'));
        
        $this->success(lang('success'));
    }
    
    public function edit()
    {
        $info_id = input('id/d',0);
        if(!$info_id){
            $this->error(lang('mustIn'));
        }
        //查询数据
        $data = daohangPageGet(['id'=>['eq',$info_id]]);
        if( is_null($data) ){
            $this->error(lang('empty'));
        }
        //定义变量
        $this->assign('data', $data);
        $this->assign('items', $this->fields('edit', $data));
        return $this->fetch('daohang@page/edit');
    }
    
    public function update()
    {
        if( !daohangPageUpdate(input('post.')) ){
            $this->error(\daicuo\Info::getError());
        }
        $this->success(lang('success'));
    }
    
    public function preview()
    {
        $info_id = input('id/d',0);
        if(!$info_id){
            $this->error(lang('mustIn'));
        }
        $url = str_replace($this->request->baseFile(), '', daohangUrlPage(daohangId($info_id)));//去掉后台入口文件
        $this->redirect($url,302);
    }
    
    public function index()
    {
        
        $this->request->filter('trim,strip_tags,htmlspecialchars');
         
        $query = $this->request->param();
        
        if($this->request->isAjax()){
            $args = array();
            $args['cache']   = false;
            $args['limit']   = DcEmpty($query['pageSize'], 20);
            $args['page']    = DcEmpty($query['pageNumber'], 1);
            $args['sort']    = DcEmpty($query['sortName'], 'info_id');
            $args['order']   = DcEmpty($query['sortOrder'], 'desc');
            $args['search']  = $query['searchText'];
            //查询数据
            $list = daohangPageSelect($args);
            if( is_null($list) ){
                return json(['total'=>0,'data'=>'']);
            }
            return json($list);
		}
        
        $this->assign('query', $query);
        
        return $this->fetch('daohang@page/index');
	}
    
    //定义表单字段
    private function fields($action='create', $data=[])
    {
        $fields = [
            'info_id'          => [
                'type'  => 'hidden',
                'value' => $data['info_id'],
            ],
            'info_type'        => [
                'type'  => 'hidden',
                'value' => 'page',
            ],
            'info_module'      => [
                'type'  => 'hidden',
                'value' => 'daohang',
            ],
            'info_controll'    => [
                'type'  => 'hidden',
                'value' => 'page',
            ],
            'info_user_id'     => [
                'type'  => 'hidden',
                'value' => DcUserCurrentGetId(),
            ],
            'info_html_1'      => [
                'type'  => 'html',
                'value' => '<div class="row"><div class="col-12 col-md-9">',
            ],
            'info_name'        => [
                'type' => 'text',
                'value' => $data['info_name'],
            ],
            'info_slug'        => [
                'type'  => 'text',
                'value' => $data['info_slug'],
            ],
            'info_excerpt'     => [
                'type'  => 'text',
                'value' => $data['info_excerpt']
            ],
            'info_content'     => [
                'type'  => 'editor',
                'value' => $data['info_content'],
                'rows'  => 30,
            ],
            'info_html_2'      => [
                'type'  => 'html',
                'value' => '</div><div class="col-12 col-md-3">',
            ],
            'info_tpl'         => [
                'type'  => 'text',
                'value' => DcEmpty($data['info_tpl'],'index'),
            ],
            'info_title'       => [
                'type'  => 'text',
                'value' => $data['info_title'],
            ],
            'info_keywords'    => [
                'type'  => 'text',
                'value' => $data['info_keywords'],
            ],
            'info_description' => [
                'type'  => 'text',
                'value' => $data['info_description']
            ],
            'info_order'       => [
                'type'  => 'number',
                'value' => intval($data['info_order']),
            ],
            'info_views'       => [
                'type'  => 'number',
                'value' => intval($data['info_views']),
            ],
            'info_hits'        => [
                'type'  => 'number',
                'value' => intval($data['info_hits']),
            ],
            'info_status'      => [
                'type'  => 'select',
                'value' => $data['info_status'],
                'option'=> ['normal'=>lang('normal'),'hidden'=>lang('hidden')],
            ],
            'info_html_3'      => [
              'type'  => 'html',
              'value' => '</div></div>',
            ]
        ];
        //增加相同字段
        foreach($fields as $key=>$value){
            $fields[$key]['class_left']  = 'col-12';
            $fields[$key]['class_right'] = 'col-12';
            $fields[$key]['class_tips']  = '';
        }
        //去除不需要的字段
        if( $action=='edit' ){
            unset($fields['info_user_id']);
        }else{
            unset($fields['info_id']);
        }
        //返回表单字段
        return $fields;
    }
	
}