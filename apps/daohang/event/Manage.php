<?php
namespace app\daohang\event;

use think\Controller;

class Manage extends Controller
{
    public function _initialize()
    {
        // 移除标签
        //$this->request->filter('trim,strip_tags,htmlspecialchars');
        // 继承上级
        //parent::_initialize();
    }
    
    public function create()
    {
        $this->assign('items', $this->fields('create'));
        
        return $this->fetch('daohang@manage/create');
    }
    
    public function save()
    {
        if( !daohangSave(input('post.')) ){
            $this->error(\daicuo\Info::getError());
        }

        $this->success(lang('success'));
    }
    
    public function delete()
    {
        daohangDelete(input('id/a'));
        
        $this->success(lang('success'));
    }
    
    public function edit()
    {
        $info_id = input('id/d',0);
        if(!$info_id){
            $this->error(lang('mustIn'));
        }
        //查询数据
        $data = daohangId($info_id);
        if( is_null($data) ){
            $this->error(lang('empty'));
        }
        //定义变量
        $this->assign('data', $data);
        $this->assign('items', $this->fields('edit', $data));
        return $this->fetch('daohang@manage/edit');
    }
    
    public function update()
    {
        if( !daohangUpdate(input('post.')) ){
            $this->error(\daicuo\Info::getError());
        }
        $this->success(lang('success'));
    }
    
    public function preview()
    {
        $info_id = input('id/d',0);
        if(!$info_id){
            $this->error(lang('mustIn'));
        }
        
        $url = str_replace($this->request->baseFile(), '', daohangUrlWeb(daohangId($info_id)));//去掉后台入口文件
        
        $this->redirect($url,302);
    }
    
    public function index()
    {
        
        $this->request->filter('trim,strip_tags,htmlspecialchars');
         
        $query = $this->request->param();
        
        if($this->request->isAjax()){
            $args = array();
            $args['cache']   = false;
            $args['limit']   = DcEmpty($query['pageSize'], 20);
            $args['page']    = DcEmpty($query['pageNumber'], 1);
            $args['sort']    = DcEmpty($query['sortName'], 'info_id');
            $args['order']   = DcEmpty($query['sortOrder'], 'desc');
            $args['search']  = $query['searchText'];
            //查询数据
            $list = daohangSelect($args);
            if( is_null($list) ){
                return json(['total'=>0,'data'=>'']);
            }
            return json($list);
		}
        
        $this->assign('query', $query);
        
        return $this->fetch('daohang@manage/index');
	}
    
    //定义表单字段
    private function fields($action='create', $data=[])
    {
        $fields = [
            'info_id'          => [
                'type'  => 'hidden',
                'value' => $data['info_id'],
            ],
            'info_module'      => [
                'type'  => 'hidden',
                'value' => 'daohang',
            ],
            'info_controll'    => [
                'type'  => 'hidden',
                'value' => 'web',
            ],
            'info_user_id'     => [
                'type'  => 'hidden',
                'value' => DcUserCurrentGetId(),
            ],
            'info_html_1'      => [
                'type'  => 'html',
                'value' => '<div class="row"><div class="col-12 col-md-9">',
            ],
            'info_name'        => [
                'type' => 'text',
                'value' => $data['info_name'],
            ],
            'info_slug'        => [
                'type'  => 'text',
                'value' => $data['info_slug'],
            ],
            'url_web'          => [
                'type'  => 'text',
                'value' => $data['url_web'],
            ],
            'url_wap'          => [
                'type'  => 'text',
                'value' => $data['url_wap'],
            ],
            'tag_name'         => [
                'type'  =>'tags',
                'value' => implode(',',$data['tag_name']),
                'option'=>daohangTags(10),
                'class_tags'      => 'form-text pt-1',
                'class_tags_list' => 'text-purple mr-2',
            ],
            'image_level'      => [
                'type'  => 'image',
                'value' => $data['image_level'],
            ],
            'image_vertical'   => [
                'type'  => 'image',
                'value' => $data['image_vertical'],
            ],
            /*'image_qrcode'     => [
                'type'  => 'image',
                'value' => $data['image_qrcode'],
            ],
            'image_mini'       => [
                'type'  => 'image',
                'value' => $data['image_mini'],
            ],*/
            'image_ico'        => [
                'type'  => 'image',
                'value' => $data['image_ico'],
            ],
            'info_title'       => [
                'type'  => 'text',
                'value' => $data['info_title'],
            ],
            'info_keywords'    => [
                'type'  => 'text',
                'value' => $data['info_keywords'],
            ],
            'info_description' => [
                'type'  => 'text',
                'value' => $data['info_description']
            ],
            'info_excerpt'     => [
                'type'  => 'text',
                'value' => $data['info_excerpt']
            ],
            'info_content'     => [
                'type'  => 'editor',
                'value' => $data['info_content'],
                'rows'  => 15,
            ],
            'info_html_2'      => [
                'type'  => 'html',
                'value' => '</div><div class="col-12 col-md-3">',
            ],
            'info_tpl'         => [
                'type'  => 'text',
                'value' => DcEmpty($data['info_tpl'],'index'),
            ],
            'info_order'       => [
                'type'  => 'number',
                'value' => intval($data['info_order']),
            ],
            'info_views'       => [
                'type'  => 'number',
                'value' => intval($data['info_views']),
            ],
            'info_hits'        => [
                'type'  => 'number',
                'value' => intval($data['info_hits']),
            ],
            'info_status'      => [
                'type'  => 'select',
                'value' => $data['info_status'],
                'option'=> ['normal'=>lang('normal'),'hidden'=>lang('hidden')],
            ],
            'info_color'       => [
                'type'  => 'select',
                'value' => $data['info_color'],
                'option'=> [
                    'text-dark'      => 'text-dark',
                    'text-danger'    => 'text-danger',
                    'text-success'   => 'text-success',
                    'text-primary'   => 'text-primary',
                    'text-info'      => 'text-info',
                    'text-secondary' => 'text-secondary',
                    'text-muted'     => 'text-muted',
                    'text-light'     => 'text-light',
                ],
            ],
            'category_id[]'        => [
                'type'  => 'checkbox',
                'value' => $data['category_id'],
                'option'=> DcTermCheck(['module'=>'daohang']),
                //'class_right'=>'col-12 pr-0',
                //'class_right_check'=>'col-4 form-check form-check-inline py-1 px-0',
            ],
            'info_html_3'      => [
              'type'  => 'html',
              'value' => '</div></div>',
            ]
        ];
        //增加相同字段
        foreach($fields as $key=>$value){
            $fields[$key]['class_left']  = 'col-12';
            $fields[$key]['class_right'] = 'col-12';
            $fields[$key]['class_tips']  = '';
        }
        //去除不需要的字段
        if( $action=='edit' ){
            unset($fields['info_user_id']);
        }else{
            unset($fields['info_id']);
        }
        //返回表单字段
        return $fields;
    }
	
}