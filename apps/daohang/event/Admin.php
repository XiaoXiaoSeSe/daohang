<?php
namespace app\daohang\event;

use think\Controller;

class Admin extends Controller
{
	
    public function _initialize()
    {
        parent::_initialize();
    }

	public function index()
    {
        $assign = [];
        $assign['optionThemes']  = DcThemeOption('daohang');
        $assign['tipsValue']     = '[:id] [:slug] [:name] ';
        $assign['tipsSearch']    = '[:searchText] ';
        $assign['tipsPage']      = '[:pageNumber] [:pageSize] [:sortName] [:sortNumber]';
        $assign['tipsClass']     = 'col-12 col-md-6 offset-md-2 pt-2 form-text text-muted small';
        $this->assign($assign);
        return $this->fetch('daohang@admin/index');
	}
    
    public function update()
    {
        $status = \daicuo\Op::write(
            input('post.'),
            input('module/s','daohang'), 
            input('controll/s','system'),
            input('action/s','config'),
            input('order/d',0),
            input('autoload/s','yes')
        );
		if( !$status ){
		    $this->error(lang('fail'));
        }
        //处理伪静态路由
        $this->configRoute(input('post.'));
        //返回结果
        $this->success(lang('success'));
	}
    
    public function seo()
    {
        return $this->fetch('daohang@admin/seo');
    }
    
    public function save(){
        $status = \daicuo\Op::write(
            input('post.'),
            input('module/s','daohang'), 
            input('controll/s','system'),
            input('action/s','seo'),
            input('order/d',0),
            input('autoload/s','yes')
        );
		if( !$status ){
		    $this->error(lang('fail'));
        }
        $this->success(lang('success'));
    }
    
    //配置伪静态
    private function configRoute($post)
    {
        //批量删除路由伪静态
        \daicuo\Op::delete_all(['op_name'=>['eq','site_route'],'op_module'=>['eq','daohang'],'op_controll'=>['in','index,category,tag,search,web,page']]);
        //批量添加路由伪静态
        $result = \daicuo\Route::save_all([
            [
                'rule'        => $post['rewrite_index'],
                'address'     => 'daohang/index/index',
                'method'      => '*',
                'op_module'   => 'daohang',
                'op_controll' => 'index',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_category'],
                'address'     => 'daohang/category/index',
                'method'      => '*',
                'op_module'   => 'daohang',
                'op_controll' => 'category',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_tag'],
                'address'     => 'daohang/tag/index',
                'method'      => '*',
                'op_module'   => 'daohang',
                'op_controll' => 'tag',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_search'],
                'address'     => 'daohang/search/index',
                'method'      => '*',
                'op_module'   => 'daohang',
                'op_controll' => 'search',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_web'],
                'address'     => 'daohang/web/index',
                'method'      => '*',
                'op_module'   => 'daohang',
                'op_controll' => 'web',
                'op_action'   => 'index',
            ],
            [
                'rule'        => $post['rewrite_page'],
                'address'     => 'daohang/page/index',
                'method'      => '*',
                'op_module'   => 'daohang',
                'op_controll' => 'page',
                'op_action'   => 'index',
            ],
        ]);
        //清理全局缓存
        DcCache('route_all', null);
    }
	
}