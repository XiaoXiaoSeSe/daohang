<?php
//扩展队列表字段
DcConfigMerge('custom_fields.term_meta', [
    'term_title',
    'term_keywords',
    'term_description',
]);

//扩展内容表字段
DcConfigMerge('custom_fields.info_meta', [
    'info_keywords',
    'info_description',
    'info_color',
    'info_tpl',
    'url_web',
    'url_wap',
    'image_level',
    'image_vertical',
    'image_qrcode',
    'image_mini',
    'image_ico',
]);

//扩展后台菜单
DcConfigMerge('admin_menu.config', [
    [
        'ico'      => 'fa fa-fw fa-link',
        'title'    => '网址设置',
        'module'   => 'daohang',
        'controll' => 'admin',
        'action'   => 'index',
        'order'    => 51,
        'url'      => DcUrlAddon( ['module'=>'daohang','controll'=>'admin','action'=>'index','active'=>'config'] ),
    ],
    [
        'ico'      => 'fa fa-fw fa-heart',
        'title'    => '优化配置',
        'target'   => '_self',
        'module'   => 'daohang',
        'controll' => 'admin',
        'action'   => 'seo',
        'order'    => 52,
        'url'      => DcUrlAddon( ['module'=>'daohang','controll'=>'admin','action'=>'seo','active'=>'config'] )
    ],
    [
        'ico'      => 'fa fa-fw fa-gavel',
        'title'    => '权限配置',
        'target'   => '_self',
        'module'   => 'daohang',
        'controll' => 'auth',
        'action'   => 'index',
        'order'    => 53,
        'url'      => DcUrlAddon( ['module'=>'daohang','controll'=>'auth','action'=>'index','active'=>'config'] )
    ],
]);

//扩展插件菜单
DcConfigMerge('admin_menu.addon', [
    [
        'menu_ico'    => 'fa-link',
        'menu_title'  => '网址',
        'menu_module' => 'daohang',
        'menu_items'  => [
            [
                'ico'      => 'fa-link',
                'title'    => '网址管理',
                'target'   => '_self',
                'controll' => 'manage',
                'action'   => 'index',
                'url'      => DcUrlAddon( ['module'=>'daohang','controll'=>'manage','action'=>'index'] )
            ],
            [
                'ico'      => 'fa-plus',
                'title'    => '添加网址',
                'target'   => '_self',
                'controll' => 'manage',
                'action'   => 'create',
                'url'      => DcUrlAddon( ['module'=>'daohang','controll'=>'manage','action'=>'create'] )
            ],
            [
                'ico'      => 'fa fa-fw fa-file-o',
                'title'    => '页面管理',
                'target'   => '_self',
                'controll' => 'page',
                'action'   => 'index',
                'url'      => DcUrlAddon( ['module'=>'daohang','controll'=>'page','action'=>'index'] )
            ],
            [
                'ico'      => 'fa-leaf',
                'title'    => '网址分类',
                'target'   => '_blank',
                'controll' => 'term',
                'action'   => 'index',
                'url'      => DcUrl('admin/category/index',['op_module'=>'daohang'],''),
            ],
            [
                'ico'      => 'fa-navicon',
                'title'    => '顶部导航',
                'target'   => '_blank',
                'controll' => 'term',
                'action'   => 'index',
                'url'      => DcUrl('admin/nav/index',['op_module'=>'daohang','op_controll'=>'navbar','op_action'=>'header'],''),
            ],
            [
                'ico'      => 'fa-navicon',
                'title'    => '底部导航',
                'target'   => '_blank',
                'controll' => 'term',
                'action'   => 'index',
                'url'      => DcUrl('admin/nav/index',['op_module'=>'daohang','op_controll'=>'navbar','op_action'=>'footer'],''),
            ],
            [
                'ico'      => 'fa-navicon',
                'title'    => '左侧导航',
                'target'   => '_blank',
                'controll' => 'term',
                'action'   => 'index',
                'url'      => DcUrl('admin/nav/index',['op_module'=>'daohang','op_controll'=>'user','op_action'=>'nav'],''),
            ],
        ]
    ]
]);

//插件初始配置会被数据库配置覆盖
return [
    'daohang' => [
        'theme'     => 'default',
        'theme_wap' => 'default',
    ]
];