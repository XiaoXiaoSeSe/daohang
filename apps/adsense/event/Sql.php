<?php
namespace app\adsense\event;

class Sql
{
    /**
    * 安装时触发/通常用于数据库操作或调用接口
    * @return bool 只有返回true时才会往下执行
    */
	  public function install()
    {
        //写入默认广告
        $this->infoSql();
        //返回执行结果
        return true;
	  }
    
    /**
    * 升级时触发/通常用于数据库操作
    * @return bool 只有返回true时才会往下执行
    */
    public function upgrade()
    {
        //更新默认广告
        $this->infoSql();
        //更新插件信息
        \daicuo\Apply::updateStatus('adsense', 'enable');
        //返回执行结果
        return true;
    }
    
    /**
    * 卸载时触发/通常用于数据库操作
    * @return bool 只有返回true时才会往下执行
    */
    public function unInstall()
    {
        //删除插件配置
        \daicuo\Op::delete_module('adsense');
        //删除默认广告
        \daicuo\Info::delete_module('adsense');
        //返回执行结果
        return true;
	}
    
    //安装与升级共用脚本
    private function infoSql()
    {
        //默认广告
        $data = [
            [
                'info_slug'    => 'doubiText',
                'info_name'    => '抖币充值文字广告',
                'info_module'  => 'adsense',
                'info_controll'=> 'index',
                'info_action'  => 'text',
                'web' => [
                    'text' => [
                        'title' => '抖币优惠充值，最低8折',
                        'link'  => '/adsense/chongzhi/doubi/',
                        'class' => 'text-danger',
                        'size'  => '',
                    ]
                ],
                'mobile' => [
                    'text' => [
                        'title' => '抖币优惠充值，最低8折',
                        'link'  => '/adsense/chongzhi/doubi/',
                        'class' => 'text-danger',
                        'size'  => '',
                    ]
                ]
            ],
            [
                'info_slug'    => 'doubi250250',
                'info_name'    => '抖币充值图片广告250*250',
                'info_module'  => 'adsense',
                'info_controll'=> 'index',
                'info_action'  => 'image',
                'web' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/250250.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => '',
                        'width'  => '250px',
                        'height' => '250px',
                    ]
                ],
                'mobile' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/250250.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => '',
                        'width'  => '250px',
                        'height' => '250px',
                    ]
                ]
            ],
            [
                'info_slug'    => 'doubi300250',
                'info_name'    => '抖币充值图片广告300*250',
                'info_module'  => 'adsense',
                'info_controll'=> 'index',
                'info_action'  => 'image',
                'web' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/300250.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => '',
                        'width'  => '300px',
                        'height' => '250px',
                    ]
                ],
                'mobile' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/300250.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => '',
                        'width'  => '300px',
                        'height' => '250px',
                    ]
                ]
            ],
            [
                'info_slug'    => 'doubi300300',
                'info_name'    => '抖币充值图片广告300*300',
                'info_module'  => 'adsense',
                'info_controll'=> 'index',
                'info_action'  => 'image',
                'web' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/300300.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => '',
                        'width'  => '300px',
                        'height' => '300px',
                    ]
                ],
                'mobile' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/300300.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => '',
                        'width'  => '300px',
                        'height' => '300px',
                    ]
                ]
            ],
            [
                'info_slug'    => 'doubi32050',
                'info_name'    => '抖币充值图片广告320*50',
                'info_module'  => 'adsense',
                'info_controll'=> 'index',
                'info_action'  => 'image',
                'web' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/32050.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => '',
                        'width'  => '320px',
                        'height' => '50px',
                    ]
                ],
                'mobile' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/32050.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => '',
                        'width'  => '320px',
                        'height' => '50px',
                    ]
                ]
            ],
            [
                'info_slug'    => 'doubi46890',
                'info_name'    => '抖币充值图片广告468*90',
                'info_module'  => 'adsense',
                'info_controll'=> 'index',
                'info_action'  => 'image',
                'web' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/46890.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => 'img-fluid',
                        'width'  => '',
                        'height' => '',
                    ]
                ],
                'mobile' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/46890.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => 'img-fluid',
                        'width'  => '',
                        'height' => '',
                    ]
                ]
            ],
            [
                'info_slug'    => 'doubi72890',
                'info_name'    => '抖币充值图片广告728*90',
                'info_module'  => 'adsense',
                'info_controll'=> 'index',
                'info_action'  => 'image',
                'web' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/72890.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => 'img-fluid',
                        'width'  => '',
                        'height' => '',
                    ]
                ],
                'mobile' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/72890.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => 'img-fluid',
                        'width'  => '',
                        'height' => '',
                    ]
                ]
            ],
            [
                'info_slug'    => 'doubi97090',
                'info_name'    => '抖币充值图片广告970*90',
                'info_module'  => 'adsense',
                'info_controll'=> 'index',
                'info_action'  => 'image',
                'web' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/97090.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => 'img-fluid',
                        'width'  => '',
                        'height' => '',
                    ]
                ],
                'mobile' => [
                    'image' => [
                        'src'    => 'https://cdn.daicuo.cc/youdu/douyin/97090.png',
                        'link'   => '/adsense/chongzhi/doubi/',
                        'alt'    => '抖币优惠充值，最低8折',
                        'class'  => 'img-fluid',
                        'width'  => '',
                        'height' => '',
                    ]
                ]
            ],
        ];
        
        //别名附加条件
        //config('common.where_slug_unique',['info_module'=>['eq','adsense']]);
        //验证规则
        //config('common.validate_name','adsense/Adsense');
        //验证场景
        //config('common.validate_scene','save');
        foreach($data as $key=>$value){
            $value['info_content']   = serialize(['web'=>$value['web'],'mobile'=>$value['mobile']]);
            unset($data['web']);
            unset($data['mobile']);
            \daicuo\Info::delete(['info_module'=>['eq','adsense'],'info_slug'=>['eq',$value['info_slug']]]);
            \daicuo\Info::save($value);
        }
    }
	
}