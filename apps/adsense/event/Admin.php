<?php
namespace app\adsense\event;

use think\Controller;

class Admin extends Controller
{
	
	public function _initialize()
    {
		parent::_initialize();
	}
    
    public function create()
    {
        return $this->fetch('adsense@admin/create');
	}
    
    public function save()
    {
        if( !$infoId = adsenseSave(input('post.')) ){
            $this->error(config('daicuo.error'));
        }
        $this->success(lang('success'));
	}
    
    public function delete()
    {
        $ids = input('id/a');
		if(!$ids){
			$this->error(lang('mustIn'));
		}
        adsenseDelete($ids);
        $this->success(lang('success'));
    }
    
    public function edit()
    {
        $info_id = input('id/d',0);
		if(!$info_id){
			$this->error(lang('mustIn'));
		}
		//查询数据
        $data = adsenseGetId($info_id, false);
        if( is_null($data) ){
            $this->error(lang('empty'));
        }
		$this->assign('data', $data);
        return $this->fetch('adsense@admin/edit');
    }
    
    public function update()
    {
        if( !$info = adsenseUpdate(input('post.')) ){
            $this->error(config('daicuo.error'));
        }
        $this->success(lang('success'));
	}

	public function index()
    {
        if($this->request->isAjax()){
            $item = adsenseAll([
                'cache'      => false,
                'status'     => 'normal',
                //'searchText' => DcHtml($this->query['searchText']),
                //'pageSize'   => 10,
                //'pageNumber' => $this->page,
            ]);
            //操作管理
            foreach($item as $key=>$value){
                $item[$key]['info_operates'] = adsenseOperate($value);
            }
            return json($item);
        }
        return $this->fetch('adsense@admin/index');
	}
    
    public function lianmeng()
    {
        $json = json_decode( DcCurl('auto',10,'http://hao.daicuo.cc/adsense/lianmeng/') , true);
        
        $this->assign($json);//['alert','item']
        
        return $this->fetch('adsense@admin/lianmeng');
	}
    
    public function token()
    {
        //是否设置框架平台的ID与TOKEN
        if(!config("common.site_id") || !config("common.site_token")){
            $this->error('您还没有填写呆错框架平台的渠道与令牌','http://hao.daicuo.cc/1.4/token/');
        }
        //换取登录后台的密钥
        $array = [];
        $array['id'] = config("common.site_id");
        $array['token'] = config("common.site_token");
        $sercet = DcCurl('auto', 3, 'http://hao.daicuo.cc/adsense/sercet/',$array);
        //跳转充值联盟后台管理
        $this->redirect('https://hao.daicuo.cc/adsense/admin/?sercet='.$sercet, 302);
    }
	
}