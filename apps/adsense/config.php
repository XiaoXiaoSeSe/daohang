<?php
//扩展后台菜单
DcConfigMerge('admin_menu.addon', [
    [
        'menu_ico'    => 'fa-cny',
        'menu_title'  => '广告',
        'menu_module' => 'adsense',
        'menu_items'  => [
            [
                'ico'      => 'fa-gear',
                'title'    => '广告管理',
                'target'   => '_self',
                'controll' => 'admin',
                'action'   => 'index',
                'url'      => DcUrlAddon( ['module'=>'adsense','controll'=>'admin','action'=>'index'] )
            ],
            [
                'ico'      => 'fa-list',
                'title'    => '广告联盟',
                'target'   => '_self',
                'controll' => 'admin',
                'action'   => 'lianmeng',
                'url'      => DcUrlAddon( ['module'=>'adsense','controll'=>'admin','action'=>'lianmeng'] )
            ]
        ]
    ]
]);

//应用配置
return [
    'adsense' => [
        'theme'     => 'default',
        'theme_wap' => 'default',
    ]
];