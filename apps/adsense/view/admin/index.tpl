{extend name="apps/common/view/admin.tpl" /}
<!-- -->
{block name="header_meta"}
<title>广告管理－{:lang('appName')}</title>
{/block}
{block name="header_addon"}
<link rel="stylesheet" type="text/css" href="{$domain}{$path_root}{$path_addon}/view/theme.css?{:DcConfig('common.site_applys.adsense.version')}">
<script type="text/javascript" src="{$domain}{$path_root}{$path_addon}/view/theme.js?{:DcConfig('common.site_applys.adsense.version')}"></script>
{/block}
<!-- -->
{block name="main"}
<h6 class="border-bottom pb-2 text-purple">
  广告管理
</h6>
<!-- -->
<form action="{:DcUrlAddon(['module'=>'adsense','controll'=>'admin','action'=>'delete'])}" method="post" data-toggle="form">
<input type="hidden" name="_method" value="delete">
<div id="toolbar" class="toolbar mb-2">
  <a class="btn btn-sm btn-light border" href="javascript:;" data-toggle="reload">
    <i class="fa fa-refresh fa-fw"></i>
    {:lang('refresh')}
  </a>
  <a class="btn btn-sm btn-outline-purple" href="{:DcUrlAddon(['module'=>'adsense','controll'=>'admin','action'=>'create'])}" data-toggle="create" data-modal-lg="true" data-callback="daicuo.admin.adsense.bakCreate">
    <i class="fa fa-plus fa-fw"></i>
    {:lang('create')}
  </a>
  <button class="btn btn-sm btn-outline-danger" type="submit" data-toggle="delete">
    <i class="fa fa-trash"></i>
    {:lang('delete')}
  </button>
</div>
{:DcBuildTable([
    'data-escape'=>'false',
    'data-toolbar'=>'.toolbar',
    'data-toolbar-align'=>'none float-md-left',
    'data-search-align'=>'none float-md-right',
    'data-toggle'=>'bootstrap-table',
    'data-url' => DcUrlAddon(['module'=>'adsense','controll'=>'admin','action'=>'index']),
    'data-buttons-class'=>'purple',
    'data-icon-size'=>'sm',
    'data-search'=>'true',
    //'data-show-search-button'=>'true',
    
    'data-unique-id'=>'info_id',
    'data-id-field'=>'info_id',
    'data-select-item-name'=>'id[]',
    'data-query-params-type'=>'params',
    //'data-query-params'=>'queryParams',
    'data-sort-name'=>'info_id',
    'data-sort-order'=>'desc',
    
    //'data-pagination'=> 'true',
    //'data-pagination-h-align'=>'left',
    //'data-pagination-detail-h-align'=>'right',
    //'data-pagination-v-align'=>'top',
    //'data-show-extended-pagination'=> 'true',

    //'data-page-number'=> $page,
    //'data-page-size'=> '10',
    //'data-page-list'=>'[10, 25, 50, 100]',
    
    'data-side-pagination'=> 'client',
    //'data-total-field'=>'total',
    //'data-data-field'=>'rows',
    'columns'=>[
        [
            'data-checkbox'=>'true',
        ],
        [
            'data-field'=>'info_id',
            'data-title'=>'id',
            'data-width'=>'50',
            'data-width-unit'=>'px',
            'data-sortable'=>'true',
            'data-sort-name'=>'info_id',
            'data-sort-order'=>'desc',
            'data-class'=>'',
            'data-align'=>'center',
            'data-valign'=>'middle',
            'data-halign'=>'center',
            'data-falign'=>'center',
            'data-visible'=>'true',
            'data-formatter'=>'',
            'data-footer-formatter'=>'',
        ],
         [
            'data-field'=>'info_slug',
            'data-title'=>'广告标识',
            'data-align'=>'left',
            'data-halign'=>'center',
            'data-width'=>'180',
            'data-width-unit'=>'px',
        ],
        [
            'data-field'=>'info_show',
            'data-title'=>'调用代码',
            'data-align'=>'left',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'info_name',
            'data-title'=>'广告描述',
            'data-align'=>'left',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'info_views',
            'data-title'=>'展示次数',
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'60',
            'data-width-unit'=>'px',
            'data-sort-name'=>'info_views',
            'data-sort-order'=>'desc',
        ],
        [
            'data-field'=>'info_hits',
            'data-title'=>'点击次数',
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'60',
            'data-width-unit'=>'px',
            'data-sort-name'=>'info_hits',
            'data-sort-order'=>'desc',
        ],
        [
            'data-field'=>'info_action_text',
            'data-title'=>'广告类型',
            'data-align'=>'center',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'info_status_text',
            'data-title'=>lang('status'),
            'data-align'=>'center',
            'data-halign'=>'center',
        ],
        [
            'data-field'=>'info_operate',
            'data-title'=>lang('operate'),
            'data-align'=>'center',
            'data-halign'=>'center',
            'data-width'=>'100',
            'data-width-unit'=>'px',
            'data-formatter'=>'window.daicuo.admin.adsense.bakOperate',
        ]
    ]
])}
</form>
{/block}
<!-- -->